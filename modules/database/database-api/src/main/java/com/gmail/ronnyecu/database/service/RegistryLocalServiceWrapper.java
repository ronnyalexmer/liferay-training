/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.gmail.ronnyecu.database.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link RegistryLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see RegistryLocalService
 * @generated
 */
@ProviderType
public class RegistryLocalServiceWrapper
	implements RegistryLocalService, ServiceWrapper<RegistryLocalService> {

	public RegistryLocalServiceWrapper(
		RegistryLocalService registryLocalService) {

		_registryLocalService = registryLocalService;
	}

	/**
	 * Adds the registry to the database. Also notifies the appropriate model listeners.
	 *
	 * @param registry the registry
	 * @return the registry that was added
	 */
	@Override
	public com.gmail.ronnyecu.database.model.Registry addRegistry(
		com.gmail.ronnyecu.database.model.Registry registry) {

		return _registryLocalService.addRegistry(registry);
	}

	/**
	 * Creates a new registry with the primary key. Does not add the registry to the database.
	 *
	 * @param registryId the primary key for the new registry
	 * @return the new registry
	 */
	@Override
	public com.gmail.ronnyecu.database.model.Registry createRegistry(
		long registryId) {

		return _registryLocalService.createRegistry(registryId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _registryLocalService.deletePersistedModel(persistedModel);
	}

	/**
	 * Deletes the registry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param registryId the primary key of the registry
	 * @return the registry that was removed
	 * @throws PortalException if a registry with the primary key could not be found
	 */
	@Override
	public com.gmail.ronnyecu.database.model.Registry deleteRegistry(
			long registryId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _registryLocalService.deleteRegistry(registryId);
	}

	/**
	 * Deletes the registry from the database. Also notifies the appropriate model listeners.
	 *
	 * @param registry the registry
	 * @return the registry that was removed
	 */
	@Override
	public com.gmail.ronnyecu.database.model.Registry deleteRegistry(
		com.gmail.ronnyecu.database.model.Registry registry) {

		return _registryLocalService.deleteRegistry(registry);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _registryLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _registryLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.gmail.ronnyecu.database.model.impl.RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _registryLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.gmail.ronnyecu.database.model.impl.RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _registryLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _registryLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _registryLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public com.gmail.ronnyecu.database.model.Registry fetchRegistry(
		long registryId) {

		return _registryLocalService.fetchRegistry(registryId);
	}

	/**
	 * Returns the registry matching the UUID and group.
	 *
	 * @param uuid the registry's UUID
	 * @param groupId the primary key of the group
	 * @return the matching registry, or <code>null</code> if a matching registry could not be found
	 */
	@Override
	public com.gmail.ronnyecu.database.model.Registry
		fetchRegistryByUuidAndGroupId(String uuid, long groupId) {

		return _registryLocalService.fetchRegistryByUuidAndGroupId(
			uuid, groupId);
	}

	@Override
	public java.util.List<com.gmail.ronnyecu.database.model.Registry>
		findRegistryWithoutName(
			long companyId, long groupId, int start, int end) {

		return _registryLocalService.findRegistryWithoutName(
			companyId, groupId, start, end);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _registryLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _registryLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _registryLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _registryLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _registryLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns a range of all the registries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.gmail.ronnyecu.database.model.impl.RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @return the range of registries
	 */
	@Override
	public java.util.List<com.gmail.ronnyecu.database.model.Registry>
		getRegistries(int start, int end) {

		return _registryLocalService.getRegistries(start, end);
	}

	/**
	 * Returns all the registries matching the UUID and company.
	 *
	 * @param uuid the UUID of the registries
	 * @param companyId the primary key of the company
	 * @return the matching registries, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<com.gmail.ronnyecu.database.model.Registry>
		getRegistriesByUuidAndCompanyId(String uuid, long companyId) {

		return _registryLocalService.getRegistriesByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of registries matching the UUID and company.
	 *
	 * @param uuid the UUID of the registries
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching registries, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<com.gmail.ronnyecu.database.model.Registry>
		getRegistriesByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<com.gmail.ronnyecu.database.model.Registry>
					orderByComparator) {

		return _registryLocalService.getRegistriesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of registries.
	 *
	 * @return the number of registries
	 */
	@Override
	public int getRegistriesCount() {
		return _registryLocalService.getRegistriesCount();
	}

	/**
	 * Returns the registry with the primary key.
	 *
	 * @param registryId the primary key of the registry
	 * @return the registry
	 * @throws PortalException if a registry with the primary key could not be found
	 */
	@Override
	public com.gmail.ronnyecu.database.model.Registry getRegistry(
			long registryId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _registryLocalService.getRegistry(registryId);
	}

	/**
	 * Returns the registry matching the UUID and group.
	 *
	 * @param uuid the registry's UUID
	 * @param groupId the primary key of the group
	 * @return the matching registry
	 * @throws PortalException if a matching registry could not be found
	 */
	@Override
	public com.gmail.ronnyecu.database.model.Registry
			getRegistryByUuidAndGroupId(String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _registryLocalService.getRegistryByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Updates the registry in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param registry the registry
	 * @return the registry that was updated
	 */
	@Override
	public com.gmail.ronnyecu.database.model.Registry updateRegistry(
		com.gmail.ronnyecu.database.model.Registry registry) {

		return _registryLocalService.updateRegistry(registry);
	}

	@Override
	public RegistryLocalService getWrappedService() {
		return _registryLocalService;
	}

	@Override
	public void setWrappedService(RegistryLocalService registryLocalService) {
		_registryLocalService = registryLocalService;
	}

	private RegistryLocalService _registryLocalService;

}