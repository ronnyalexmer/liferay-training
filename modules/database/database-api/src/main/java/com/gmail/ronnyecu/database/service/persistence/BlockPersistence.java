/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.gmail.ronnyecu.database.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.gmail.ronnyecu.database.exception.NoSuchBlockException;
import com.gmail.ronnyecu.database.model.Block;

import com.liferay.portal.kernel.service.persistence.BasePersistence;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * The persistence interface for the block service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see BlockUtil
 * @generated
 */
@ProviderType
public interface BlockPersistence extends BasePersistence<Block> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link BlockUtil} to access the block persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */
	@Override
	public Map<Serializable, Block> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys);

	/**
	 * Returns all the blocks where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching blocks
	 */
	public java.util.List<Block> findByUuid(String uuid);

	/**
	 * Returns a range of all the blocks where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @return the range of matching blocks
	 */
	public java.util.List<Block> findByUuid(String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the blocks where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByUuid(String, int, int, OrderByComparator)}
	 * @param uuid the uuid
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching blocks
	 */
	@Deprecated
	public java.util.List<Block> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Block> orderByComparator, boolean useFinderCache);

	/**
	 * Returns an ordered range of all the blocks where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching blocks
	 */
	public java.util.List<Block> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Block> orderByComparator);

	/**
	 * Returns the first block in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching block
	 * @throws NoSuchBlockException if a matching block could not be found
	 */
	public Block findByUuid_First(
			String uuid, OrderByComparator<Block> orderByComparator)
		throws NoSuchBlockException;

	/**
	 * Returns the first block in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching block, or <code>null</code> if a matching block could not be found
	 */
	public Block fetchByUuid_First(
		String uuid, OrderByComparator<Block> orderByComparator);

	/**
	 * Returns the last block in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching block
	 * @throws NoSuchBlockException if a matching block could not be found
	 */
	public Block findByUuid_Last(
			String uuid, OrderByComparator<Block> orderByComparator)
		throws NoSuchBlockException;

	/**
	 * Returns the last block in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching block, or <code>null</code> if a matching block could not be found
	 */
	public Block fetchByUuid_Last(
		String uuid, OrderByComparator<Block> orderByComparator);

	/**
	 * Returns the blocks before and after the current block in the ordered set where uuid = &#63;.
	 *
	 * @param blockId the primary key of the current block
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next block
	 * @throws NoSuchBlockException if a block with the primary key could not be found
	 */
	public Block[] findByUuid_PrevAndNext(
			long blockId, String uuid,
			OrderByComparator<Block> orderByComparator)
		throws NoSuchBlockException;

	/**
	 * Removes all the blocks where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of blocks where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching blocks
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the block where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchBlockException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching block
	 * @throws NoSuchBlockException if a matching block could not be found
	 */
	public Block findByUUID_G(String uuid, long groupId)
		throws NoSuchBlockException;

	/**
	 * Returns the block where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #fetchByUUID_G(String,long)}
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching block, or <code>null</code> if a matching block could not be found
	 */
	@Deprecated
	public Block fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Returns the block where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching block, or <code>null</code> if a matching block could not be found
	 */
	public Block fetchByUUID_G(String uuid, long groupId);

	/**
	 * Removes the block where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the block that was removed
	 */
	public Block removeByUUID_G(String uuid, long groupId)
		throws NoSuchBlockException;

	/**
	 * Returns the number of blocks where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching blocks
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the blocks where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching blocks
	 */
	public java.util.List<Block> findByUuid_C(String uuid, long companyId);

	/**
	 * Returns a range of all the blocks where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @return the range of matching blocks
	 */
	public java.util.List<Block> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the blocks where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByUuid_C(String,long, int, int, OrderByComparator)}
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching blocks
	 */
	@Deprecated
	public java.util.List<Block> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Block> orderByComparator, boolean useFinderCache);

	/**
	 * Returns an ordered range of all the blocks where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching blocks
	 */
	public java.util.List<Block> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Block> orderByComparator);

	/**
	 * Returns the first block in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching block
	 * @throws NoSuchBlockException if a matching block could not be found
	 */
	public Block findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<Block> orderByComparator)
		throws NoSuchBlockException;

	/**
	 * Returns the first block in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching block, or <code>null</code> if a matching block could not be found
	 */
	public Block fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<Block> orderByComparator);

	/**
	 * Returns the last block in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching block
	 * @throws NoSuchBlockException if a matching block could not be found
	 */
	public Block findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<Block> orderByComparator)
		throws NoSuchBlockException;

	/**
	 * Returns the last block in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching block, or <code>null</code> if a matching block could not be found
	 */
	public Block fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<Block> orderByComparator);

	/**
	 * Returns the blocks before and after the current block in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param blockId the primary key of the current block
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next block
	 * @throws NoSuchBlockException if a block with the primary key could not be found
	 */
	public Block[] findByUuid_C_PrevAndNext(
			long blockId, String uuid, long companyId,
			OrderByComparator<Block> orderByComparator)
		throws NoSuchBlockException;

	/**
	 * Removes all the blocks where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of blocks where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching blocks
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns the block where companyId = &#63; and groupId = &#63; and blockCode = &#63; or throws a <code>NoSuchBlockException</code> if it could not be found.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @return the matching block
	 * @throws NoSuchBlockException if a matching block could not be found
	 */
	public Block findByBlockCode(long companyId, long groupId, String blockCode)
		throws NoSuchBlockException;

	/**
	 * Returns the block where companyId = &#63; and groupId = &#63; and blockCode = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #fetchByBlockCode(long,long,String)}
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching block, or <code>null</code> if a matching block could not be found
	 */
	@Deprecated
	public Block fetchByBlockCode(
		long companyId, long groupId, String blockCode, boolean useFinderCache);

	/**
	 * Returns the block where companyId = &#63; and groupId = &#63; and blockCode = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching block, or <code>null</code> if a matching block could not be found
	 */
	public Block fetchByBlockCode(
		long companyId, long groupId, String blockCode);

	/**
	 * Removes the block where companyId = &#63; and groupId = &#63; and blockCode = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @return the block that was removed
	 */
	public Block removeByBlockCode(
			long companyId, long groupId, String blockCode)
		throws NoSuchBlockException;

	/**
	 * Returns the number of blocks where companyId = &#63; and groupId = &#63; and blockCode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @return the number of matching blocks
	 */
	public int countByBlockCode(long companyId, long groupId, String blockCode);

	/**
	 * Returns all the blocks where companyId = &#63; and groupId = &#63; and fileName LIKE &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fileName the file name
	 * @return the matching blocks
	 */
	public java.util.List<Block> findByFileName(
		long companyId, long groupId, String fileName);

	/**
	 * Returns a range of all the blocks where companyId = &#63; and groupId = &#63; and fileName LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fileName the file name
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @return the range of matching blocks
	 */
	public java.util.List<Block> findByFileName(
		long companyId, long groupId, String fileName, int start, int end);

	/**
	 * Returns an ordered range of all the blocks where companyId = &#63; and groupId = &#63; and fileName LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByFileName(long,long,String, int, int, OrderByComparator)}
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fileName the file name
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching blocks
	 */
	@Deprecated
	public java.util.List<Block> findByFileName(
		long companyId, long groupId, String fileName, int start, int end,
		OrderByComparator<Block> orderByComparator, boolean useFinderCache);

	/**
	 * Returns an ordered range of all the blocks where companyId = &#63; and groupId = &#63; and fileName LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fileName the file name
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching blocks
	 */
	public java.util.List<Block> findByFileName(
		long companyId, long groupId, String fileName, int start, int end,
		OrderByComparator<Block> orderByComparator);

	/**
	 * Returns the first block in the ordered set where companyId = &#63; and groupId = &#63; and fileName LIKE &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fileName the file name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching block
	 * @throws NoSuchBlockException if a matching block could not be found
	 */
	public Block findByFileName_First(
			long companyId, long groupId, String fileName,
			OrderByComparator<Block> orderByComparator)
		throws NoSuchBlockException;

	/**
	 * Returns the first block in the ordered set where companyId = &#63; and groupId = &#63; and fileName LIKE &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fileName the file name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching block, or <code>null</code> if a matching block could not be found
	 */
	public Block fetchByFileName_First(
		long companyId, long groupId, String fileName,
		OrderByComparator<Block> orderByComparator);

	/**
	 * Returns the last block in the ordered set where companyId = &#63; and groupId = &#63; and fileName LIKE &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fileName the file name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching block
	 * @throws NoSuchBlockException if a matching block could not be found
	 */
	public Block findByFileName_Last(
			long companyId, long groupId, String fileName,
			OrderByComparator<Block> orderByComparator)
		throws NoSuchBlockException;

	/**
	 * Returns the last block in the ordered set where companyId = &#63; and groupId = &#63; and fileName LIKE &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fileName the file name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching block, or <code>null</code> if a matching block could not be found
	 */
	public Block fetchByFileName_Last(
		long companyId, long groupId, String fileName,
		OrderByComparator<Block> orderByComparator);

	/**
	 * Returns the blocks before and after the current block in the ordered set where companyId = &#63; and groupId = &#63; and fileName LIKE &#63;.
	 *
	 * @param blockId the primary key of the current block
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fileName the file name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next block
	 * @throws NoSuchBlockException if a block with the primary key could not be found
	 */
	public Block[] findByFileName_PrevAndNext(
			long blockId, long companyId, long groupId, String fileName,
			OrderByComparator<Block> orderByComparator)
		throws NoSuchBlockException;

	/**
	 * Removes all the blocks where companyId = &#63; and groupId = &#63; and fileName LIKE &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fileName the file name
	 */
	public void removeByFileName(long companyId, long groupId, String fileName);

	/**
	 * Returns the number of blocks where companyId = &#63; and groupId = &#63; and fileName LIKE &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fileName the file name
	 * @return the number of matching blocks
	 */
	public int countByFileName(long companyId, long groupId, String fileName);

	/**
	 * Returns all the blocks where companyId = &#63; and groupId = &#63; and modifiedDate &lt; &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param modifiedDate the modified date
	 * @return the matching blocks
	 */
	public java.util.List<Block> findByModifiedDate(
		long companyId, long groupId, Date modifiedDate);

	/**
	 * Returns a range of all the blocks where companyId = &#63; and groupId = &#63; and modifiedDate &lt; &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param modifiedDate the modified date
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @return the range of matching blocks
	 */
	public java.util.List<Block> findByModifiedDate(
		long companyId, long groupId, Date modifiedDate, int start, int end);

	/**
	 * Returns an ordered range of all the blocks where companyId = &#63; and groupId = &#63; and modifiedDate &lt; &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByModifiedDate(long,long,Date, int, int, OrderByComparator)}
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param modifiedDate the modified date
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching blocks
	 */
	@Deprecated
	public java.util.List<Block> findByModifiedDate(
		long companyId, long groupId, Date modifiedDate, int start, int end,
		OrderByComparator<Block> orderByComparator, boolean useFinderCache);

	/**
	 * Returns an ordered range of all the blocks where companyId = &#63; and groupId = &#63; and modifiedDate &lt; &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param modifiedDate the modified date
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching blocks
	 */
	public java.util.List<Block> findByModifiedDate(
		long companyId, long groupId, Date modifiedDate, int start, int end,
		OrderByComparator<Block> orderByComparator);

	/**
	 * Returns the first block in the ordered set where companyId = &#63; and groupId = &#63; and modifiedDate &lt; &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param modifiedDate the modified date
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching block
	 * @throws NoSuchBlockException if a matching block could not be found
	 */
	public Block findByModifiedDate_First(
			long companyId, long groupId, Date modifiedDate,
			OrderByComparator<Block> orderByComparator)
		throws NoSuchBlockException;

	/**
	 * Returns the first block in the ordered set where companyId = &#63; and groupId = &#63; and modifiedDate &lt; &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param modifiedDate the modified date
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching block, or <code>null</code> if a matching block could not be found
	 */
	public Block fetchByModifiedDate_First(
		long companyId, long groupId, Date modifiedDate,
		OrderByComparator<Block> orderByComparator);

	/**
	 * Returns the last block in the ordered set where companyId = &#63; and groupId = &#63; and modifiedDate &lt; &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param modifiedDate the modified date
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching block
	 * @throws NoSuchBlockException if a matching block could not be found
	 */
	public Block findByModifiedDate_Last(
			long companyId, long groupId, Date modifiedDate,
			OrderByComparator<Block> orderByComparator)
		throws NoSuchBlockException;

	/**
	 * Returns the last block in the ordered set where companyId = &#63; and groupId = &#63; and modifiedDate &lt; &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param modifiedDate the modified date
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching block, or <code>null</code> if a matching block could not be found
	 */
	public Block fetchByModifiedDate_Last(
		long companyId, long groupId, Date modifiedDate,
		OrderByComparator<Block> orderByComparator);

	/**
	 * Returns the blocks before and after the current block in the ordered set where companyId = &#63; and groupId = &#63; and modifiedDate &lt; &#63;.
	 *
	 * @param blockId the primary key of the current block
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param modifiedDate the modified date
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next block
	 * @throws NoSuchBlockException if a block with the primary key could not be found
	 */
	public Block[] findByModifiedDate_PrevAndNext(
			long blockId, long companyId, long groupId, Date modifiedDate,
			OrderByComparator<Block> orderByComparator)
		throws NoSuchBlockException;

	/**
	 * Removes all the blocks where companyId = &#63; and groupId = &#63; and modifiedDate &lt; &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param modifiedDate the modified date
	 */
	public void removeByModifiedDate(
		long companyId, long groupId, Date modifiedDate);

	/**
	 * Returns the number of blocks where companyId = &#63; and groupId = &#63; and modifiedDate &lt; &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param modifiedDate the modified date
	 * @return the number of matching blocks
	 */
	public int countByModifiedDate(
		long companyId, long groupId, Date modifiedDate);

	/**
	 * Returns all the blocks where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @return the matching blocks
	 */
	public java.util.List<Block> findByWithoutDescription(
		long companyId, long groupId);

	/**
	 * Returns a range of all the blocks where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @return the range of matching blocks
	 */
	public java.util.List<Block> findByWithoutDescription(
		long companyId, long groupId, int start, int end);

	/**
	 * Returns an ordered range of all the blocks where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByWithoutDescription(long,long, int, int, OrderByComparator)}
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching blocks
	 */
	@Deprecated
	public java.util.List<Block> findByWithoutDescription(
		long companyId, long groupId, int start, int end,
		OrderByComparator<Block> orderByComparator, boolean useFinderCache);

	/**
	 * Returns an ordered range of all the blocks where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching blocks
	 */
	public java.util.List<Block> findByWithoutDescription(
		long companyId, long groupId, int start, int end,
		OrderByComparator<Block> orderByComparator);

	/**
	 * Returns the first block in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching block
	 * @throws NoSuchBlockException if a matching block could not be found
	 */
	public Block findByWithoutDescription_First(
			long companyId, long groupId,
			OrderByComparator<Block> orderByComparator)
		throws NoSuchBlockException;

	/**
	 * Returns the first block in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching block, or <code>null</code> if a matching block could not be found
	 */
	public Block fetchByWithoutDescription_First(
		long companyId, long groupId,
		OrderByComparator<Block> orderByComparator);

	/**
	 * Returns the last block in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching block
	 * @throws NoSuchBlockException if a matching block could not be found
	 */
	public Block findByWithoutDescription_Last(
			long companyId, long groupId,
			OrderByComparator<Block> orderByComparator)
		throws NoSuchBlockException;

	/**
	 * Returns the last block in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching block, or <code>null</code> if a matching block could not be found
	 */
	public Block fetchByWithoutDescription_Last(
		long companyId, long groupId,
		OrderByComparator<Block> orderByComparator);

	/**
	 * Returns the blocks before and after the current block in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param blockId the primary key of the current block
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next block
	 * @throws NoSuchBlockException if a block with the primary key could not be found
	 */
	public Block[] findByWithoutDescription_PrevAndNext(
			long blockId, long companyId, long groupId,
			OrderByComparator<Block> orderByComparator)
		throws NoSuchBlockException;

	/**
	 * Removes all the blocks where companyId = &#63; and groupId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 */
	public void removeByWithoutDescription(long companyId, long groupId);

	/**
	 * Returns the number of blocks where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @return the number of matching blocks
	 */
	public int countByWithoutDescription(long companyId, long groupId);

	/**
	 * Caches the block in the entity cache if it is enabled.
	 *
	 * @param block the block
	 */
	public void cacheResult(Block block);

	/**
	 * Caches the blocks in the entity cache if it is enabled.
	 *
	 * @param blocks the blocks
	 */
	public void cacheResult(java.util.List<Block> blocks);

	/**
	 * Creates a new block with the primary key. Does not add the block to the database.
	 *
	 * @param blockId the primary key for the new block
	 * @return the new block
	 */
	public Block create(long blockId);

	/**
	 * Removes the block with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param blockId the primary key of the block
	 * @return the block that was removed
	 * @throws NoSuchBlockException if a block with the primary key could not be found
	 */
	public Block remove(long blockId) throws NoSuchBlockException;

	public Block updateImpl(Block block);

	/**
	 * Returns the block with the primary key or throws a <code>NoSuchBlockException</code> if it could not be found.
	 *
	 * @param blockId the primary key of the block
	 * @return the block
	 * @throws NoSuchBlockException if a block with the primary key could not be found
	 */
	public Block findByPrimaryKey(long blockId) throws NoSuchBlockException;

	/**
	 * Returns the block with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param blockId the primary key of the block
	 * @return the block, or <code>null</code> if a block with the primary key could not be found
	 */
	public Block fetchByPrimaryKey(long blockId);

	/**
	 * Returns all the blocks.
	 *
	 * @return the blocks
	 */
	public java.util.List<Block> findAll();

	/**
	 * Returns a range of all the blocks.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @return the range of blocks
	 */
	public java.util.List<Block> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the blocks.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findAll(int, int, OrderByComparator)}
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of blocks
	 */
	@Deprecated
	public java.util.List<Block> findAll(
		int start, int end, OrderByComparator<Block> orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns an ordered range of all the blocks.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of blocks
	 */
	public java.util.List<Block> findAll(
		int start, int end, OrderByComparator<Block> orderByComparator);

	/**
	 * Removes all the blocks from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of blocks.
	 *
	 * @return the number of blocks
	 */
	public int countAll();

	@Override
	public Set<String> getBadColumnNames();

}