/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.gmail.ronnyecu.database.service;

import aQute.bnd.annotation.ProviderType;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for Block. This utility wraps
 * <code>com.gmail.ronnyecu.database.service.impl.BlockLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see BlockLocalService
 * @generated
 */
@ProviderType
public class BlockLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.gmail.ronnyecu.database.service.impl.BlockLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the block to the database. Also notifies the appropriate model listeners.
	 *
	 * @param block the block
	 * @return the block that was added
	 */
	public static com.gmail.ronnyecu.database.model.Block addBlock(
		com.gmail.ronnyecu.database.model.Block block) {

		return getService().addBlock(block);
	}

	/**
	 * Creates a new block with the primary key. Does not add the block to the database.
	 *
	 * @param blockId the primary key for the new block
	 * @return the new block
	 */
	public static com.gmail.ronnyecu.database.model.Block createBlock(
		long blockId) {

		return getService().createBlock(blockId);
	}

	/**
	 * Deletes the block from the database. Also notifies the appropriate model listeners.
	 *
	 * @param block the block
	 * @return the block that was removed
	 */
	public static com.gmail.ronnyecu.database.model.Block deleteBlock(
		com.gmail.ronnyecu.database.model.Block block) {

		return getService().deleteBlock(block);
	}

	/**
	 * Deletes the block with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param blockId the primary key of the block
	 * @return the block that was removed
	 * @throws PortalException if a block with the primary key could not be found
	 */
	public static com.gmail.ronnyecu.database.model.Block deleteBlock(
			long blockId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deleteBlock(blockId);
	}

	/**
	 * @throws PortalException
	 */
	public static com.liferay.portal.kernel.model.PersistedModel
			deletePersistedModel(
				com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery
		dynamicQuery() {

		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.gmail.ronnyecu.database.model.impl.BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.gmail.ronnyecu.database.model.impl.BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static com.gmail.ronnyecu.database.model.Block fetchBlock(
		long blockId) {

		return getService().fetchBlock(blockId);
	}

	/**
	 * Returns the block matching the UUID and group.
	 *
	 * @param uuid the block's UUID
	 * @param groupId the primary key of the group
	 * @return the matching block, or <code>null</code> if a matching block could not be found
	 */
	public static com.gmail.ronnyecu.database.model.Block
		fetchBlockByUuidAndGroupId(String uuid, long groupId) {

		return getService().fetchBlockByUuidAndGroupId(uuid, groupId);
	}

	public static java.util.List<com.gmail.ronnyecu.database.model.Block>
		findAllBlock() {

		return getService().findAllBlock();
	}

	public static java.util.List<com.gmail.ronnyecu.database.model.Block>
		findByFileName(
			long companyId, long groupId, String words, int start, int end) {

		return getService().findByFileName(
			companyId, groupId, words, start, end);
	}

	public static java.util.List<com.gmail.ronnyecu.database.model.Block>
		findByFileName(
			long companyId, long groupId, String words, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<com.gmail.ronnyecu.database.model.Block> obc) {

		return getService().findByFileName(
			companyId, groupId, words, start, end, obc);
	}

	public static int findByFileNameCount(
		long companyId, long groupId, String words) {

		return getService().findByFileNameCount(companyId, groupId, words);
	}

	public static java.util.List<com.gmail.ronnyecu.database.model.Block>
		findModiedDateLessThan(
			long companyId, long groupId, java.util.Date date, int start,
			int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<com.gmail.ronnyecu.database.model.Block> obc) {

		return getService().findModiedDateLessThan(
			companyId, groupId, date, start, end, obc);
	}

	public static java.util.List<com.gmail.ronnyecu.database.model.Block>
		findWithoutDescription(
			long companyId, long groupId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<com.gmail.ronnyecu.database.model.Block> obc) {

		return getService().findWithoutDescription(
			companyId, groupId, start, end, obc);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	/**
	 * Returns the block with the primary key.
	 *
	 * @param blockId the primary key of the block
	 * @return the block
	 * @throws PortalException if a block with the primary key could not be found
	 */
	public static com.gmail.ronnyecu.database.model.Block getBlock(long blockId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getBlock(blockId);
	}

	/**
	 * Returns the block matching the UUID and group.
	 *
	 * @param uuid the block's UUID
	 * @param groupId the primary key of the group
	 * @return the matching block
	 * @throws PortalException if a matching block could not be found
	 */
	public static com.gmail.ronnyecu.database.model.Block
			getBlockByUuidAndGroupId(String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getBlockByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the blocks.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.gmail.ronnyecu.database.model.impl.BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @return the range of blocks
	 */
	public static java.util.List<com.gmail.ronnyecu.database.model.Block>
		getBlocks(int start, int end) {

		return getService().getBlocks(start, end);
	}

	public static java.util.List<com.gmail.ronnyecu.database.model.Block>
		getBlocks(
			int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<com.gmail.ronnyecu.database.model.Block> obc) {

		return getService().getBlocks(start, end, obc);
	}

	/**
	 * Returns all the blocks matching the UUID and company.
	 *
	 * @param uuid the UUID of the blocks
	 * @param companyId the primary key of the company
	 * @return the matching blocks, or an empty list if no matches were found
	 */
	public static java.util.List<com.gmail.ronnyecu.database.model.Block>
		getBlocksByUuidAndCompanyId(String uuid, long companyId) {

		return getService().getBlocksByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of blocks matching the UUID and company.
	 *
	 * @param uuid the UUID of the blocks
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching blocks, or an empty list if no matches were found
	 */
	public static java.util.List<com.gmail.ronnyecu.database.model.Block>
		getBlocksByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<com.gmail.ronnyecu.database.model.Block> orderByComparator) {

		return getService().getBlocksByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of blocks.
	 *
	 * @return the number of blocks
	 */
	public static int getBlocksCount() {
		return getService().getBlocksCount();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	public static int getModiedDateLessThanCount(
		long companyId, long groupId, java.util.Date date) {

		return getService().getModiedDateLessThanCount(
			companyId, groupId, date);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static com.liferay.portal.kernel.model.PersistedModel
			getPersistedModel(java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	public static int getWithoutDescriptionCount(long companyId, long groupId) {
		return getService().getWithoutDescriptionCount(companyId, groupId);
	}

	/**
	 * Updates the block in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param block the block
	 * @return the block that was updated
	 */
	public static com.gmail.ronnyecu.database.model.Block updateBlock(
		com.gmail.ronnyecu.database.model.Block block) {

		return getService().updateBlock(block);
	}

	public static BlockLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<BlockLocalService, BlockLocalService>
		_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(BlockLocalService.class);

		ServiceTracker<BlockLocalService, BlockLocalService> serviceTracker =
			new ServiceTracker<BlockLocalService, BlockLocalService>(
				bundle.getBundleContext(), BlockLocalService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}