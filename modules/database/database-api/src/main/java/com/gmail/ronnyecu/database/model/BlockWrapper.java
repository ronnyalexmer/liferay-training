/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.gmail.ronnyecu.database.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Block}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Block
 * @generated
 */
@ProviderType
public class BlockWrapper implements Block, ModelWrapper<Block> {

	public BlockWrapper(Block block) {
		_block = block;
	}

	@Override
	public Class<?> getModelClass() {
		return Block.class;
	}

	@Override
	public String getModelClassName() {
		return Block.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("blockId", getBlockId());
		attributes.put("companyId", getCompanyId());
		attributes.put("groupId", getGroupId());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("blockCode", getBlockCode());
		attributes.put("fileName", getFileName());
		attributes.put("description", getDescription());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long blockId = (Long)attributes.get("blockId");

		if (blockId != null) {
			setBlockId(blockId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String blockCode = (String)attributes.get("blockCode");

		if (blockCode != null) {
			setBlockCode(blockCode);
		}

		String fileName = (String)attributes.get("fileName");

		if (fileName != null) {
			setFileName(fileName);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}
	}

	@Override
	public Object clone() {
		return new BlockWrapper((Block)_block.clone());
	}

	@Override
	public int compareTo(Block block) {
		return _block.compareTo(block);
	}

	/**
	 * Returns the block code of this block.
	 *
	 * @return the block code of this block
	 */
	@Override
	public String getBlockCode() {
		return _block.getBlockCode();
	}

	/**
	 * Returns the block ID of this block.
	 *
	 * @return the block ID of this block
	 */
	@Override
	public long getBlockId() {
		return _block.getBlockId();
	}

	/**
	 * Returns the company ID of this block.
	 *
	 * @return the company ID of this block
	 */
	@Override
	public long getCompanyId() {
		return _block.getCompanyId();
	}

	/**
	 * Returns the create date of this block.
	 *
	 * @return the create date of this block
	 */
	@Override
	public Date getCreateDate() {
		return _block.getCreateDate();
	}

	/**
	 * Returns the description of this block.
	 *
	 * @return the description of this block
	 */
	@Override
	public String getDescription() {
		return _block.getDescription();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _block.getExpandoBridge();
	}

	/**
	 * Returns the file name of this block.
	 *
	 * @return the file name of this block
	 */
	@Override
	public String getFileName() {
		return _block.getFileName();
	}

	/**
	 * Returns the group ID of this block.
	 *
	 * @return the group ID of this block
	 */
	@Override
	public long getGroupId() {
		return _block.getGroupId();
	}

	/**
	 * Returns the modified date of this block.
	 *
	 * @return the modified date of this block
	 */
	@Override
	public Date getModifiedDate() {
		return _block.getModifiedDate();
	}

	/**
	 * Returns the primary key of this block.
	 *
	 * @return the primary key of this block
	 */
	@Override
	public long getPrimaryKey() {
		return _block.getPrimaryKey();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _block.getPrimaryKeyObj();
	}

	/**
	 * Returns the uuid of this block.
	 *
	 * @return the uuid of this block
	 */
	@Override
	public String getUuid() {
		return _block.getUuid();
	}

	@Override
	public int hashCode() {
		return _block.hashCode();
	}

	@Override
	public boolean isCachedModel() {
		return _block.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _block.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _block.isNew();
	}

	@Override
	public void persist() {
		_block.persist();
	}

	/**
	 * Sets the block code of this block.
	 *
	 * @param blockCode the block code of this block
	 */
	@Override
	public void setBlockCode(String blockCode) {
		_block.setBlockCode(blockCode);
	}

	/**
	 * Sets the block ID of this block.
	 *
	 * @param blockId the block ID of this block
	 */
	@Override
	public void setBlockId(long blockId) {
		_block.setBlockId(blockId);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_block.setCachedModel(cachedModel);
	}

	/**
	 * Sets the company ID of this block.
	 *
	 * @param companyId the company ID of this block
	 */
	@Override
	public void setCompanyId(long companyId) {
		_block.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this block.
	 *
	 * @param createDate the create date of this block
	 */
	@Override
	public void setCreateDate(Date createDate) {
		_block.setCreateDate(createDate);
	}

	/**
	 * Sets the description of this block.
	 *
	 * @param description the description of this block
	 */
	@Override
	public void setDescription(String description) {
		_block.setDescription(description);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {

		_block.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_block.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_block.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	 * Sets the file name of this block.
	 *
	 * @param fileName the file name of this block
	 */
	@Override
	public void setFileName(String fileName) {
		_block.setFileName(fileName);
	}

	/**
	 * Sets the group ID of this block.
	 *
	 * @param groupId the group ID of this block
	 */
	@Override
	public void setGroupId(long groupId) {
		_block.setGroupId(groupId);
	}

	/**
	 * Sets the modified date of this block.
	 *
	 * @param modifiedDate the modified date of this block
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_block.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_block.setNew(n);
	}

	/**
	 * Sets the primary key of this block.
	 *
	 * @param primaryKey the primary key of this block
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		_block.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_block.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	 * Sets the uuid of this block.
	 *
	 * @param uuid the uuid of this block
	 */
	@Override
	public void setUuid(String uuid) {
		_block.setUuid(uuid);
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Block> toCacheModel() {
		return _block.toCacheModel();
	}

	@Override
	public Block toEscapedModel() {
		return new BlockWrapper(_block.toEscapedModel());
	}

	@Override
	public String toString() {
		return _block.toString();
	}

	@Override
	public Block toUnescapedModel() {
		return new BlockWrapper(_block.toUnescapedModel());
	}

	@Override
	public String toXmlString() {
		return _block.toXmlString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof BlockWrapper)) {
			return false;
		}

		BlockWrapper blockWrapper = (BlockWrapper)obj;

		if (Objects.equals(_block, blockWrapper._block)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _block.getStagedModelType();
	}

	@Override
	public Block getWrappedModel() {
		return _block;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _block.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _block.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_block.resetOriginalValues();
	}

	private final Block _block;

}