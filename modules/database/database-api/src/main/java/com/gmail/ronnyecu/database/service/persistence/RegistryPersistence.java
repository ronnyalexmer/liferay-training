/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.gmail.ronnyecu.database.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.gmail.ronnyecu.database.exception.NoSuchRegistryException;
import com.gmail.ronnyecu.database.model.Registry;

import com.liferay.portal.kernel.service.persistence.BasePersistence;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.Map;
import java.util.Set;

/**
 * The persistence interface for the registry service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see RegistryUtil
 * @generated
 */
@ProviderType
public interface RegistryPersistence extends BasePersistence<Registry> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link RegistryUtil} to access the registry persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */
	@Override
	public Map<Serializable, Registry> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys);

	/**
	 * Returns all the registries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching registries
	 */
	public java.util.List<Registry> findByUuid(String uuid);

	/**
	 * Returns a range of all the registries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @return the range of matching registries
	 */
	public java.util.List<Registry> findByUuid(String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the registries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByUuid(String, int, int, OrderByComparator)}
	 * @param uuid the uuid
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching registries
	 */
	@Deprecated
	public java.util.List<Registry> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Registry> orderByComparator, boolean useFinderCache);

	/**
	 * Returns an ordered range of all the registries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching registries
	 */
	public java.util.List<Registry> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Registry> orderByComparator);

	/**
	 * Returns the first registry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	public Registry findByUuid_First(
			String uuid, OrderByComparator<Registry> orderByComparator)
		throws NoSuchRegistryException;

	/**
	 * Returns the first registry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching registry, or <code>null</code> if a matching registry could not be found
	 */
	public Registry fetchByUuid_First(
		String uuid, OrderByComparator<Registry> orderByComparator);

	/**
	 * Returns the last registry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	public Registry findByUuid_Last(
			String uuid, OrderByComparator<Registry> orderByComparator)
		throws NoSuchRegistryException;

	/**
	 * Returns the last registry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching registry, or <code>null</code> if a matching registry could not be found
	 */
	public Registry fetchByUuid_Last(
		String uuid, OrderByComparator<Registry> orderByComparator);

	/**
	 * Returns the registries before and after the current registry in the ordered set where uuid = &#63;.
	 *
	 * @param registryId the primary key of the current registry
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next registry
	 * @throws NoSuchRegistryException if a registry with the primary key could not be found
	 */
	public Registry[] findByUuid_PrevAndNext(
			long registryId, String uuid,
			OrderByComparator<Registry> orderByComparator)
		throws NoSuchRegistryException;

	/**
	 * Removes all the registries where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of registries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching registries
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the registry where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchRegistryException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	public Registry findByUUID_G(String uuid, long groupId)
		throws NoSuchRegistryException;

	/**
	 * Returns the registry where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #fetchByUUID_G(String,long)}
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching registry, or <code>null</code> if a matching registry could not be found
	 */
	@Deprecated
	public Registry fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Returns the registry where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching registry, or <code>null</code> if a matching registry could not be found
	 */
	public Registry fetchByUUID_G(String uuid, long groupId);

	/**
	 * Removes the registry where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the registry that was removed
	 */
	public Registry removeByUUID_G(String uuid, long groupId)
		throws NoSuchRegistryException;

	/**
	 * Returns the number of registries where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching registries
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the registries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching registries
	 */
	public java.util.List<Registry> findByUuid_C(String uuid, long companyId);

	/**
	 * Returns a range of all the registries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @return the range of matching registries
	 */
	public java.util.List<Registry> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the registries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByUuid_C(String,long, int, int, OrderByComparator)}
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching registries
	 */
	@Deprecated
	public java.util.List<Registry> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Registry> orderByComparator, boolean useFinderCache);

	/**
	 * Returns an ordered range of all the registries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching registries
	 */
	public java.util.List<Registry> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Registry> orderByComparator);

	/**
	 * Returns the first registry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	public Registry findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<Registry> orderByComparator)
		throws NoSuchRegistryException;

	/**
	 * Returns the first registry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching registry, or <code>null</code> if a matching registry could not be found
	 */
	public Registry fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<Registry> orderByComparator);

	/**
	 * Returns the last registry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	public Registry findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<Registry> orderByComparator)
		throws NoSuchRegistryException;

	/**
	 * Returns the last registry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching registry, or <code>null</code> if a matching registry could not be found
	 */
	public Registry fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<Registry> orderByComparator);

	/**
	 * Returns the registries before and after the current registry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param registryId the primary key of the current registry
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next registry
	 * @throws NoSuchRegistryException if a registry with the primary key could not be found
	 */
	public Registry[] findByUuid_C_PrevAndNext(
			long registryId, String uuid, long companyId,
			OrderByComparator<Registry> orderByComparator)
		throws NoSuchRegistryException;

	/**
	 * Removes all the registries where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of registries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching registries
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns the registry where companyId = &#63; and groupId = &#63; and nif = &#63; or throws a <code>NoSuchRegistryException</code> if it could not be found.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param nif the nif
	 * @return the matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	public Registry findByNif(long companyId, long groupId, String nif)
		throws NoSuchRegistryException;

	/**
	 * Returns the registry where companyId = &#63; and groupId = &#63; and nif = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #fetchByNif(long,long,String)}
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param nif the nif
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching registry, or <code>null</code> if a matching registry could not be found
	 */
	@Deprecated
	public Registry fetchByNif(
		long companyId, long groupId, String nif, boolean useFinderCache);

	/**
	 * Returns the registry where companyId = &#63; and groupId = &#63; and nif = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param nif the nif
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching registry, or <code>null</code> if a matching registry could not be found
	 */
	public Registry fetchByNif(long companyId, long groupId, String nif);

	/**
	 * Removes the registry where companyId = &#63; and groupId = &#63; and nif = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param nif the nif
	 * @return the registry that was removed
	 */
	public Registry removeByNif(long companyId, long groupId, String nif)
		throws NoSuchRegistryException;

	/**
	 * Returns the number of registries where companyId = &#63; and groupId = &#63; and nif = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param nif the nif
	 * @return the number of matching registries
	 */
	public int countByNif(long companyId, long groupId, String nif);

	/**
	 * Returns all the registries where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @return the matching registries
	 */
	public java.util.List<Registry> findByWithoutName(
		long companyId, long groupId);

	/**
	 * Returns a range of all the registries where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @return the range of matching registries
	 */
	public java.util.List<Registry> findByWithoutName(
		long companyId, long groupId, int start, int end);

	/**
	 * Returns an ordered range of all the registries where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByWithoutName(long,long, int, int, OrderByComparator)}
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching registries
	 */
	@Deprecated
	public java.util.List<Registry> findByWithoutName(
		long companyId, long groupId, int start, int end,
		OrderByComparator<Registry> orderByComparator, boolean useFinderCache);

	/**
	 * Returns an ordered range of all the registries where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching registries
	 */
	public java.util.List<Registry> findByWithoutName(
		long companyId, long groupId, int start, int end,
		OrderByComparator<Registry> orderByComparator);

	/**
	 * Returns the first registry in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	public Registry findByWithoutName_First(
			long companyId, long groupId,
			OrderByComparator<Registry> orderByComparator)
		throws NoSuchRegistryException;

	/**
	 * Returns the first registry in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching registry, or <code>null</code> if a matching registry could not be found
	 */
	public Registry fetchByWithoutName_First(
		long companyId, long groupId,
		OrderByComparator<Registry> orderByComparator);

	/**
	 * Returns the last registry in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	public Registry findByWithoutName_Last(
			long companyId, long groupId,
			OrderByComparator<Registry> orderByComparator)
		throws NoSuchRegistryException;

	/**
	 * Returns the last registry in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching registry, or <code>null</code> if a matching registry could not be found
	 */
	public Registry fetchByWithoutName_Last(
		long companyId, long groupId,
		OrderByComparator<Registry> orderByComparator);

	/**
	 * Returns the registries before and after the current registry in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param registryId the primary key of the current registry
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next registry
	 * @throws NoSuchRegistryException if a registry with the primary key could not be found
	 */
	public Registry[] findByWithoutName_PrevAndNext(
			long registryId, long companyId, long groupId,
			OrderByComparator<Registry> orderByComparator)
		throws NoSuchRegistryException;

	/**
	 * Removes all the registries where companyId = &#63; and groupId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 */
	public void removeByWithoutName(long companyId, long groupId);

	/**
	 * Returns the number of registries where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @return the number of matching registries
	 */
	public int countByWithoutName(long companyId, long groupId);

	/**
	 * Returns all the registries where companyId = &#63; and groupId = &#63; and blockCode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @return the matching registries
	 */
	public java.util.List<Registry> findByBlockCode(
		long companyId, long groupId, String blockCode);

	/**
	 * Returns a range of all the registries where companyId = &#63; and groupId = &#63; and blockCode = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @return the range of matching registries
	 */
	public java.util.List<Registry> findByBlockCode(
		long companyId, long groupId, String blockCode, int start, int end);

	/**
	 * Returns an ordered range of all the registries where companyId = &#63; and groupId = &#63; and blockCode = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByBlockCode(long,long,String, int, int, OrderByComparator)}
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching registries
	 */
	@Deprecated
	public java.util.List<Registry> findByBlockCode(
		long companyId, long groupId, String blockCode, int start, int end,
		OrderByComparator<Registry> orderByComparator, boolean useFinderCache);

	/**
	 * Returns an ordered range of all the registries where companyId = &#63; and groupId = &#63; and blockCode = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching registries
	 */
	public java.util.List<Registry> findByBlockCode(
		long companyId, long groupId, String blockCode, int start, int end,
		OrderByComparator<Registry> orderByComparator);

	/**
	 * Returns the first registry in the ordered set where companyId = &#63; and groupId = &#63; and blockCode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	public Registry findByBlockCode_First(
			long companyId, long groupId, String blockCode,
			OrderByComparator<Registry> orderByComparator)
		throws NoSuchRegistryException;

	/**
	 * Returns the first registry in the ordered set where companyId = &#63; and groupId = &#63; and blockCode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching registry, or <code>null</code> if a matching registry could not be found
	 */
	public Registry fetchByBlockCode_First(
		long companyId, long groupId, String blockCode,
		OrderByComparator<Registry> orderByComparator);

	/**
	 * Returns the last registry in the ordered set where companyId = &#63; and groupId = &#63; and blockCode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	public Registry findByBlockCode_Last(
			long companyId, long groupId, String blockCode,
			OrderByComparator<Registry> orderByComparator)
		throws NoSuchRegistryException;

	/**
	 * Returns the last registry in the ordered set where companyId = &#63; and groupId = &#63; and blockCode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching registry, or <code>null</code> if a matching registry could not be found
	 */
	public Registry fetchByBlockCode_Last(
		long companyId, long groupId, String blockCode,
		OrderByComparator<Registry> orderByComparator);

	/**
	 * Returns the registries before and after the current registry in the ordered set where companyId = &#63; and groupId = &#63; and blockCode = &#63;.
	 *
	 * @param registryId the primary key of the current registry
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next registry
	 * @throws NoSuchRegistryException if a registry with the primary key could not be found
	 */
	public Registry[] findByBlockCode_PrevAndNext(
			long registryId, long companyId, long groupId, String blockCode,
			OrderByComparator<Registry> orderByComparator)
		throws NoSuchRegistryException;

	/**
	 * Removes all the registries where companyId = &#63; and groupId = &#63; and blockCode = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 */
	public void removeByBlockCode(
		long companyId, long groupId, String blockCode);

	/**
	 * Returns the number of registries where companyId = &#63; and groupId = &#63; and blockCode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @return the number of matching registries
	 */
	public int countByBlockCode(long companyId, long groupId, String blockCode);

	/**
	 * Returns all the registries where companyId = &#63; and groupId = &#63; and fullName = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 * @return the matching registries
	 */
	public java.util.List<Registry> findByName(
		long companyId, long groupId, String fullName);

	/**
	 * Returns a range of all the registries where companyId = &#63; and groupId = &#63; and fullName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @return the range of matching registries
	 */
	public java.util.List<Registry> findByName(
		long companyId, long groupId, String fullName, int start, int end);

	/**
	 * Returns an ordered range of all the registries where companyId = &#63; and groupId = &#63; and fullName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByName(long,long,String, int, int, OrderByComparator)}
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching registries
	 */
	@Deprecated
	public java.util.List<Registry> findByName(
		long companyId, long groupId, String fullName, int start, int end,
		OrderByComparator<Registry> orderByComparator, boolean useFinderCache);

	/**
	 * Returns an ordered range of all the registries where companyId = &#63; and groupId = &#63; and fullName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching registries
	 */
	public java.util.List<Registry> findByName(
		long companyId, long groupId, String fullName, int start, int end,
		OrderByComparator<Registry> orderByComparator);

	/**
	 * Returns the first registry in the ordered set where companyId = &#63; and groupId = &#63; and fullName = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	public Registry findByName_First(
			long companyId, long groupId, String fullName,
			OrderByComparator<Registry> orderByComparator)
		throws NoSuchRegistryException;

	/**
	 * Returns the first registry in the ordered set where companyId = &#63; and groupId = &#63; and fullName = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching registry, or <code>null</code> if a matching registry could not be found
	 */
	public Registry fetchByName_First(
		long companyId, long groupId, String fullName,
		OrderByComparator<Registry> orderByComparator);

	/**
	 * Returns the last registry in the ordered set where companyId = &#63; and groupId = &#63; and fullName = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	public Registry findByName_Last(
			long companyId, long groupId, String fullName,
			OrderByComparator<Registry> orderByComparator)
		throws NoSuchRegistryException;

	/**
	 * Returns the last registry in the ordered set where companyId = &#63; and groupId = &#63; and fullName = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching registry, or <code>null</code> if a matching registry could not be found
	 */
	public Registry fetchByName_Last(
		long companyId, long groupId, String fullName,
		OrderByComparator<Registry> orderByComparator);

	/**
	 * Returns the registries before and after the current registry in the ordered set where companyId = &#63; and groupId = &#63; and fullName = &#63;.
	 *
	 * @param registryId the primary key of the current registry
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next registry
	 * @throws NoSuchRegistryException if a registry with the primary key could not be found
	 */
	public Registry[] findByName_PrevAndNext(
			long registryId, long companyId, long groupId, String fullName,
			OrderByComparator<Registry> orderByComparator)
		throws NoSuchRegistryException;

	/**
	 * Removes all the registries where companyId = &#63; and groupId = &#63; and fullName = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 */
	public void removeByName(long companyId, long groupId, String fullName);

	/**
	 * Returns the number of registries where companyId = &#63; and groupId = &#63; and fullName = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 * @return the number of matching registries
	 */
	public int countByName(long companyId, long groupId, String fullName);

	/**
	 * Caches the registry in the entity cache if it is enabled.
	 *
	 * @param registry the registry
	 */
	public void cacheResult(Registry registry);

	/**
	 * Caches the registries in the entity cache if it is enabled.
	 *
	 * @param registries the registries
	 */
	public void cacheResult(java.util.List<Registry> registries);

	/**
	 * Creates a new registry with the primary key. Does not add the registry to the database.
	 *
	 * @param registryId the primary key for the new registry
	 * @return the new registry
	 */
	public Registry create(long registryId);

	/**
	 * Removes the registry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param registryId the primary key of the registry
	 * @return the registry that was removed
	 * @throws NoSuchRegistryException if a registry with the primary key could not be found
	 */
	public Registry remove(long registryId) throws NoSuchRegistryException;

	public Registry updateImpl(Registry registry);

	/**
	 * Returns the registry with the primary key or throws a <code>NoSuchRegistryException</code> if it could not be found.
	 *
	 * @param registryId the primary key of the registry
	 * @return the registry
	 * @throws NoSuchRegistryException if a registry with the primary key could not be found
	 */
	public Registry findByPrimaryKey(long registryId)
		throws NoSuchRegistryException;

	/**
	 * Returns the registry with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param registryId the primary key of the registry
	 * @return the registry, or <code>null</code> if a registry with the primary key could not be found
	 */
	public Registry fetchByPrimaryKey(long registryId);

	/**
	 * Returns all the registries.
	 *
	 * @return the registries
	 */
	public java.util.List<Registry> findAll();

	/**
	 * Returns a range of all the registries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @return the range of registries
	 */
	public java.util.List<Registry> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the registries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findAll(int, int, OrderByComparator)}
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of registries
	 */
	@Deprecated
	public java.util.List<Registry> findAll(
		int start, int end, OrderByComparator<Registry> orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns an ordered range of all the registries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of registries
	 */
	public java.util.List<Registry> findAll(
		int start, int end, OrderByComparator<Registry> orderByComparator);

	/**
	 * Removes all the registries from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of registries.
	 *
	 * @return the number of registries
	 */
	public int countAll();

	@Override
	public Set<String> getBadColumnNames();

}