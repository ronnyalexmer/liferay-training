/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.gmail.ronnyecu.database.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Registry}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Registry
 * @generated
 */
@ProviderType
public class RegistryWrapper implements Registry, ModelWrapper<Registry> {

	public RegistryWrapper(Registry registry) {
		_registry = registry;
	}

	@Override
	public Class<?> getModelClass() {
		return Registry.class;
	}

	@Override
	public String getModelClassName() {
		return Registry.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("registryId", getRegistryId());
		attributes.put("companyId", getCompanyId());
		attributes.put("groupId", getGroupId());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("nif", getNif());
		attributes.put("fullName", getFullName());
		attributes.put("amount", getAmount());
		attributes.put("token", getToken());
		attributes.put("blockCode", getBlockCode());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long registryId = (Long)attributes.get("registryId");

		if (registryId != null) {
			setRegistryId(registryId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String nif = (String)attributes.get("nif");

		if (nif != null) {
			setNif(nif);
		}

		String fullName = (String)attributes.get("fullName");

		if (fullName != null) {
			setFullName(fullName);
		}

		Double amount = (Double)attributes.get("amount");

		if (amount != null) {
			setAmount(amount);
		}

		String token = (String)attributes.get("token");

		if (token != null) {
			setToken(token);
		}

		String blockCode = (String)attributes.get("blockCode");

		if (blockCode != null) {
			setBlockCode(blockCode);
		}
	}

	@Override
	public Object clone() {
		return new RegistryWrapper((Registry)_registry.clone());
	}

	@Override
	public int compareTo(Registry registry) {
		return _registry.compareTo(registry);
	}

	/**
	 * Returns the amount of this registry.
	 *
	 * @return the amount of this registry
	 */
	@Override
	public double getAmount() {
		return _registry.getAmount();
	}

	/**
	 * Returns the block code of this registry.
	 *
	 * @return the block code of this registry
	 */
	@Override
	public String getBlockCode() {
		return _registry.getBlockCode();
	}

	/**
	 * Returns the company ID of this registry.
	 *
	 * @return the company ID of this registry
	 */
	@Override
	public long getCompanyId() {
		return _registry.getCompanyId();
	}

	/**
	 * Returns the create date of this registry.
	 *
	 * @return the create date of this registry
	 */
	@Override
	public Date getCreateDate() {
		return _registry.getCreateDate();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _registry.getExpandoBridge();
	}

	/**
	 * Returns the full name of this registry.
	 *
	 * @return the full name of this registry
	 */
	@Override
	public String getFullName() {
		return _registry.getFullName();
	}

	/**
	 * Returns the group ID of this registry.
	 *
	 * @return the group ID of this registry
	 */
	@Override
	public long getGroupId() {
		return _registry.getGroupId();
	}

	/**
	 * Returns the modified date of this registry.
	 *
	 * @return the modified date of this registry
	 */
	@Override
	public Date getModifiedDate() {
		return _registry.getModifiedDate();
	}

	/**
	 * Returns the nif of this registry.
	 *
	 * @return the nif of this registry
	 */
	@Override
	public String getNif() {
		return _registry.getNif();
	}

	/**
	 * Returns the primary key of this registry.
	 *
	 * @return the primary key of this registry
	 */
	@Override
	public long getPrimaryKey() {
		return _registry.getPrimaryKey();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _registry.getPrimaryKeyObj();
	}

	/**
	 * Returns the registry ID of this registry.
	 *
	 * @return the registry ID of this registry
	 */
	@Override
	public long getRegistryId() {
		return _registry.getRegistryId();
	}

	/**
	 * Returns the token of this registry.
	 *
	 * @return the token of this registry
	 */
	@Override
	public String getToken() {
		return _registry.getToken();
	}

	/**
	 * Returns the uuid of this registry.
	 *
	 * @return the uuid of this registry
	 */
	@Override
	public String getUuid() {
		return _registry.getUuid();
	}

	@Override
	public int hashCode() {
		return _registry.hashCode();
	}

	@Override
	public boolean isCachedModel() {
		return _registry.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _registry.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _registry.isNew();
	}

	@Override
	public void persist() {
		_registry.persist();
	}

	/**
	 * Sets the amount of this registry.
	 *
	 * @param amount the amount of this registry
	 */
	@Override
	public void setAmount(double amount) {
		_registry.setAmount(amount);
	}

	/**
	 * Sets the block code of this registry.
	 *
	 * @param blockCode the block code of this registry
	 */
	@Override
	public void setBlockCode(String blockCode) {
		_registry.setBlockCode(blockCode);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_registry.setCachedModel(cachedModel);
	}

	/**
	 * Sets the company ID of this registry.
	 *
	 * @param companyId the company ID of this registry
	 */
	@Override
	public void setCompanyId(long companyId) {
		_registry.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this registry.
	 *
	 * @param createDate the create date of this registry
	 */
	@Override
	public void setCreateDate(Date createDate) {
		_registry.setCreateDate(createDate);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {

		_registry.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_registry.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_registry.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	 * Sets the full name of this registry.
	 *
	 * @param fullName the full name of this registry
	 */
	@Override
	public void setFullName(String fullName) {
		_registry.setFullName(fullName);
	}

	/**
	 * Sets the group ID of this registry.
	 *
	 * @param groupId the group ID of this registry
	 */
	@Override
	public void setGroupId(long groupId) {
		_registry.setGroupId(groupId);
	}

	/**
	 * Sets the modified date of this registry.
	 *
	 * @param modifiedDate the modified date of this registry
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_registry.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_registry.setNew(n);
	}

	/**
	 * Sets the nif of this registry.
	 *
	 * @param nif the nif of this registry
	 */
	@Override
	public void setNif(String nif) {
		_registry.setNif(nif);
	}

	/**
	 * Sets the primary key of this registry.
	 *
	 * @param primaryKey the primary key of this registry
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		_registry.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_registry.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	 * Sets the registry ID of this registry.
	 *
	 * @param registryId the registry ID of this registry
	 */
	@Override
	public void setRegistryId(long registryId) {
		_registry.setRegistryId(registryId);
	}

	/**
	 * Sets the token of this registry.
	 *
	 * @param token the token of this registry
	 */
	@Override
	public void setToken(String token) {
		_registry.setToken(token);
	}

	/**
	 * Sets the uuid of this registry.
	 *
	 * @param uuid the uuid of this registry
	 */
	@Override
	public void setUuid(String uuid) {
		_registry.setUuid(uuid);
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Registry> toCacheModel() {
		return _registry.toCacheModel();
	}

	@Override
	public Registry toEscapedModel() {
		return new RegistryWrapper(_registry.toEscapedModel());
	}

	@Override
	public String toString() {
		return _registry.toString();
	}

	@Override
	public Registry toUnescapedModel() {
		return new RegistryWrapper(_registry.toUnescapedModel());
	}

	@Override
	public String toXmlString() {
		return _registry.toXmlString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof RegistryWrapper)) {
			return false;
		}

		RegistryWrapper registryWrapper = (RegistryWrapper)obj;

		if (Objects.equals(_registry, registryWrapper._registry)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _registry.getStagedModelType();
	}

	@Override
	public Registry getWrappedModel() {
		return _registry;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _registry.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _registry.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_registry.resetOriginalValues();
	}

	private final Registry _registry;

}