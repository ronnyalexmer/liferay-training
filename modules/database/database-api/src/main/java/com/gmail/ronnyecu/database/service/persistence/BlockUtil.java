/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.gmail.ronnyecu.database.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.gmail.ronnyecu.database.model.Block;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * The persistence utility for the block service. This utility wraps <code>com.gmail.ronnyecu.database.service.persistence.impl.BlockPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see BlockPersistence
 * @generated
 */
@ProviderType
public class BlockUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Block block) {
		getPersistence().clearCache(block);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, Block> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Block> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Block> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Block> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Block> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Block update(Block block) {
		return getPersistence().update(block);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Block update(Block block, ServiceContext serviceContext) {
		return getPersistence().update(block, serviceContext);
	}

	/**
	 * Returns all the blocks where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching blocks
	 */
	public static List<Block> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the blocks where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @return the range of matching blocks
	 */
	public static List<Block> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the blocks where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByUuid(String, int, int, OrderByComparator)}
	 * @param uuid the uuid
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching blocks
	 */
	@Deprecated
	public static List<Block> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Block> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns an ordered range of all the blocks where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching blocks
	 */
	public static List<Block> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Block> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns the first block in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching block
	 * @throws NoSuchBlockException if a matching block could not be found
	 */
	public static Block findByUuid_First(
			String uuid, OrderByComparator<Block> orderByComparator)
		throws com.gmail.ronnyecu.database.exception.NoSuchBlockException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first block in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching block, or <code>null</code> if a matching block could not be found
	 */
	public static Block fetchByUuid_First(
		String uuid, OrderByComparator<Block> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last block in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching block
	 * @throws NoSuchBlockException if a matching block could not be found
	 */
	public static Block findByUuid_Last(
			String uuid, OrderByComparator<Block> orderByComparator)
		throws com.gmail.ronnyecu.database.exception.NoSuchBlockException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last block in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching block, or <code>null</code> if a matching block could not be found
	 */
	public static Block fetchByUuid_Last(
		String uuid, OrderByComparator<Block> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the blocks before and after the current block in the ordered set where uuid = &#63;.
	 *
	 * @param blockId the primary key of the current block
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next block
	 * @throws NoSuchBlockException if a block with the primary key could not be found
	 */
	public static Block[] findByUuid_PrevAndNext(
			long blockId, String uuid,
			OrderByComparator<Block> orderByComparator)
		throws com.gmail.ronnyecu.database.exception.NoSuchBlockException {

		return getPersistence().findByUuid_PrevAndNext(
			blockId, uuid, orderByComparator);
	}

	/**
	 * Removes all the blocks where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of blocks where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching blocks
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the block where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchBlockException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching block
	 * @throws NoSuchBlockException if a matching block could not be found
	 */
	public static Block findByUUID_G(String uuid, long groupId)
		throws com.gmail.ronnyecu.database.exception.NoSuchBlockException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the block where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #fetchByUUID_G(String,long)}
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching block, or <code>null</code> if a matching block could not be found
	 */
	@Deprecated
	public static Block fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Returns the block where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching block, or <code>null</code> if a matching block could not be found
	 */
	public static Block fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Removes the block where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the block that was removed
	 */
	public static Block removeByUUID_G(String uuid, long groupId)
		throws com.gmail.ronnyecu.database.exception.NoSuchBlockException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of blocks where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching blocks
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the blocks where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching blocks
	 */
	public static List<Block> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the blocks where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @return the range of matching blocks
	 */
	public static List<Block> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the blocks where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByUuid_C(String,long, int, int, OrderByComparator)}
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching blocks
	 */
	@Deprecated
	public static List<Block> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Block> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns an ordered range of all the blocks where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching blocks
	 */
	public static List<Block> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Block> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the first block in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching block
	 * @throws NoSuchBlockException if a matching block could not be found
	 */
	public static Block findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<Block> orderByComparator)
		throws com.gmail.ronnyecu.database.exception.NoSuchBlockException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first block in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching block, or <code>null</code> if a matching block could not be found
	 */
	public static Block fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<Block> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last block in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching block
	 * @throws NoSuchBlockException if a matching block could not be found
	 */
	public static Block findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<Block> orderByComparator)
		throws com.gmail.ronnyecu.database.exception.NoSuchBlockException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last block in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching block, or <code>null</code> if a matching block could not be found
	 */
	public static Block fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<Block> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the blocks before and after the current block in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param blockId the primary key of the current block
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next block
	 * @throws NoSuchBlockException if a block with the primary key could not be found
	 */
	public static Block[] findByUuid_C_PrevAndNext(
			long blockId, String uuid, long companyId,
			OrderByComparator<Block> orderByComparator)
		throws com.gmail.ronnyecu.database.exception.NoSuchBlockException {

		return getPersistence().findByUuid_C_PrevAndNext(
			blockId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the blocks where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of blocks where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching blocks
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the block where companyId = &#63; and groupId = &#63; and blockCode = &#63; or throws a <code>NoSuchBlockException</code> if it could not be found.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @return the matching block
	 * @throws NoSuchBlockException if a matching block could not be found
	 */
	public static Block findByBlockCode(
			long companyId, long groupId, String blockCode)
		throws com.gmail.ronnyecu.database.exception.NoSuchBlockException {

		return getPersistence().findByBlockCode(companyId, groupId, blockCode);
	}

	/**
	 * Returns the block where companyId = &#63; and groupId = &#63; and blockCode = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #fetchByBlockCode(long,long,String)}
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching block, or <code>null</code> if a matching block could not be found
	 */
	@Deprecated
	public static Block fetchByBlockCode(
		long companyId, long groupId, String blockCode,
		boolean useFinderCache) {

		return getPersistence().fetchByBlockCode(
			companyId, groupId, blockCode, useFinderCache);
	}

	/**
	 * Returns the block where companyId = &#63; and groupId = &#63; and blockCode = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching block, or <code>null</code> if a matching block could not be found
	 */
	public static Block fetchByBlockCode(
		long companyId, long groupId, String blockCode) {

		return getPersistence().fetchByBlockCode(companyId, groupId, blockCode);
	}

	/**
	 * Removes the block where companyId = &#63; and groupId = &#63; and blockCode = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @return the block that was removed
	 */
	public static Block removeByBlockCode(
			long companyId, long groupId, String blockCode)
		throws com.gmail.ronnyecu.database.exception.NoSuchBlockException {

		return getPersistence().removeByBlockCode(
			companyId, groupId, blockCode);
	}

	/**
	 * Returns the number of blocks where companyId = &#63; and groupId = &#63; and blockCode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @return the number of matching blocks
	 */
	public static int countByBlockCode(
		long companyId, long groupId, String blockCode) {

		return getPersistence().countByBlockCode(companyId, groupId, blockCode);
	}

	/**
	 * Returns all the blocks where companyId = &#63; and groupId = &#63; and fileName LIKE &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fileName the file name
	 * @return the matching blocks
	 */
	public static List<Block> findByFileName(
		long companyId, long groupId, String fileName) {

		return getPersistence().findByFileName(companyId, groupId, fileName);
	}

	/**
	 * Returns a range of all the blocks where companyId = &#63; and groupId = &#63; and fileName LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fileName the file name
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @return the range of matching blocks
	 */
	public static List<Block> findByFileName(
		long companyId, long groupId, String fileName, int start, int end) {

		return getPersistence().findByFileName(
			companyId, groupId, fileName, start, end);
	}

	/**
	 * Returns an ordered range of all the blocks where companyId = &#63; and groupId = &#63; and fileName LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByFileName(long,long,String, int, int, OrderByComparator)}
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fileName the file name
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching blocks
	 */
	@Deprecated
	public static List<Block> findByFileName(
		long companyId, long groupId, String fileName, int start, int end,
		OrderByComparator<Block> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByFileName(
			companyId, groupId, fileName, start, end, orderByComparator,
			useFinderCache);
	}

	/**
	 * Returns an ordered range of all the blocks where companyId = &#63; and groupId = &#63; and fileName LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fileName the file name
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching blocks
	 */
	public static List<Block> findByFileName(
		long companyId, long groupId, String fileName, int start, int end,
		OrderByComparator<Block> orderByComparator) {

		return getPersistence().findByFileName(
			companyId, groupId, fileName, start, end, orderByComparator);
	}

	/**
	 * Returns the first block in the ordered set where companyId = &#63; and groupId = &#63; and fileName LIKE &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fileName the file name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching block
	 * @throws NoSuchBlockException if a matching block could not be found
	 */
	public static Block findByFileName_First(
			long companyId, long groupId, String fileName,
			OrderByComparator<Block> orderByComparator)
		throws com.gmail.ronnyecu.database.exception.NoSuchBlockException {

		return getPersistence().findByFileName_First(
			companyId, groupId, fileName, orderByComparator);
	}

	/**
	 * Returns the first block in the ordered set where companyId = &#63; and groupId = &#63; and fileName LIKE &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fileName the file name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching block, or <code>null</code> if a matching block could not be found
	 */
	public static Block fetchByFileName_First(
		long companyId, long groupId, String fileName,
		OrderByComparator<Block> orderByComparator) {

		return getPersistence().fetchByFileName_First(
			companyId, groupId, fileName, orderByComparator);
	}

	/**
	 * Returns the last block in the ordered set where companyId = &#63; and groupId = &#63; and fileName LIKE &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fileName the file name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching block
	 * @throws NoSuchBlockException if a matching block could not be found
	 */
	public static Block findByFileName_Last(
			long companyId, long groupId, String fileName,
			OrderByComparator<Block> orderByComparator)
		throws com.gmail.ronnyecu.database.exception.NoSuchBlockException {

		return getPersistence().findByFileName_Last(
			companyId, groupId, fileName, orderByComparator);
	}

	/**
	 * Returns the last block in the ordered set where companyId = &#63; and groupId = &#63; and fileName LIKE &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fileName the file name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching block, or <code>null</code> if a matching block could not be found
	 */
	public static Block fetchByFileName_Last(
		long companyId, long groupId, String fileName,
		OrderByComparator<Block> orderByComparator) {

		return getPersistence().fetchByFileName_Last(
			companyId, groupId, fileName, orderByComparator);
	}

	/**
	 * Returns the blocks before and after the current block in the ordered set where companyId = &#63; and groupId = &#63; and fileName LIKE &#63;.
	 *
	 * @param blockId the primary key of the current block
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fileName the file name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next block
	 * @throws NoSuchBlockException if a block with the primary key could not be found
	 */
	public static Block[] findByFileName_PrevAndNext(
			long blockId, long companyId, long groupId, String fileName,
			OrderByComparator<Block> orderByComparator)
		throws com.gmail.ronnyecu.database.exception.NoSuchBlockException {

		return getPersistence().findByFileName_PrevAndNext(
			blockId, companyId, groupId, fileName, orderByComparator);
	}

	/**
	 * Removes all the blocks where companyId = &#63; and groupId = &#63; and fileName LIKE &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fileName the file name
	 */
	public static void removeByFileName(
		long companyId, long groupId, String fileName) {

		getPersistence().removeByFileName(companyId, groupId, fileName);
	}

	/**
	 * Returns the number of blocks where companyId = &#63; and groupId = &#63; and fileName LIKE &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fileName the file name
	 * @return the number of matching blocks
	 */
	public static int countByFileName(
		long companyId, long groupId, String fileName) {

		return getPersistence().countByFileName(companyId, groupId, fileName);
	}

	/**
	 * Returns all the blocks where companyId = &#63; and groupId = &#63; and modifiedDate &lt; &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param modifiedDate the modified date
	 * @return the matching blocks
	 */
	public static List<Block> findByModifiedDate(
		long companyId, long groupId, Date modifiedDate) {

		return getPersistence().findByModifiedDate(
			companyId, groupId, modifiedDate);
	}

	/**
	 * Returns a range of all the blocks where companyId = &#63; and groupId = &#63; and modifiedDate &lt; &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param modifiedDate the modified date
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @return the range of matching blocks
	 */
	public static List<Block> findByModifiedDate(
		long companyId, long groupId, Date modifiedDate, int start, int end) {

		return getPersistence().findByModifiedDate(
			companyId, groupId, modifiedDate, start, end);
	}

	/**
	 * Returns an ordered range of all the blocks where companyId = &#63; and groupId = &#63; and modifiedDate &lt; &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByModifiedDate(long,long,Date, int, int, OrderByComparator)}
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param modifiedDate the modified date
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching blocks
	 */
	@Deprecated
	public static List<Block> findByModifiedDate(
		long companyId, long groupId, Date modifiedDate, int start, int end,
		OrderByComparator<Block> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByModifiedDate(
			companyId, groupId, modifiedDate, start, end, orderByComparator,
			useFinderCache);
	}

	/**
	 * Returns an ordered range of all the blocks where companyId = &#63; and groupId = &#63; and modifiedDate &lt; &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param modifiedDate the modified date
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching blocks
	 */
	public static List<Block> findByModifiedDate(
		long companyId, long groupId, Date modifiedDate, int start, int end,
		OrderByComparator<Block> orderByComparator) {

		return getPersistence().findByModifiedDate(
			companyId, groupId, modifiedDate, start, end, orderByComparator);
	}

	/**
	 * Returns the first block in the ordered set where companyId = &#63; and groupId = &#63; and modifiedDate &lt; &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param modifiedDate the modified date
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching block
	 * @throws NoSuchBlockException if a matching block could not be found
	 */
	public static Block findByModifiedDate_First(
			long companyId, long groupId, Date modifiedDate,
			OrderByComparator<Block> orderByComparator)
		throws com.gmail.ronnyecu.database.exception.NoSuchBlockException {

		return getPersistence().findByModifiedDate_First(
			companyId, groupId, modifiedDate, orderByComparator);
	}

	/**
	 * Returns the first block in the ordered set where companyId = &#63; and groupId = &#63; and modifiedDate &lt; &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param modifiedDate the modified date
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching block, or <code>null</code> if a matching block could not be found
	 */
	public static Block fetchByModifiedDate_First(
		long companyId, long groupId, Date modifiedDate,
		OrderByComparator<Block> orderByComparator) {

		return getPersistence().fetchByModifiedDate_First(
			companyId, groupId, modifiedDate, orderByComparator);
	}

	/**
	 * Returns the last block in the ordered set where companyId = &#63; and groupId = &#63; and modifiedDate &lt; &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param modifiedDate the modified date
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching block
	 * @throws NoSuchBlockException if a matching block could not be found
	 */
	public static Block findByModifiedDate_Last(
			long companyId, long groupId, Date modifiedDate,
			OrderByComparator<Block> orderByComparator)
		throws com.gmail.ronnyecu.database.exception.NoSuchBlockException {

		return getPersistence().findByModifiedDate_Last(
			companyId, groupId, modifiedDate, orderByComparator);
	}

	/**
	 * Returns the last block in the ordered set where companyId = &#63; and groupId = &#63; and modifiedDate &lt; &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param modifiedDate the modified date
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching block, or <code>null</code> if a matching block could not be found
	 */
	public static Block fetchByModifiedDate_Last(
		long companyId, long groupId, Date modifiedDate,
		OrderByComparator<Block> orderByComparator) {

		return getPersistence().fetchByModifiedDate_Last(
			companyId, groupId, modifiedDate, orderByComparator);
	}

	/**
	 * Returns the blocks before and after the current block in the ordered set where companyId = &#63; and groupId = &#63; and modifiedDate &lt; &#63;.
	 *
	 * @param blockId the primary key of the current block
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param modifiedDate the modified date
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next block
	 * @throws NoSuchBlockException if a block with the primary key could not be found
	 */
	public static Block[] findByModifiedDate_PrevAndNext(
			long blockId, long companyId, long groupId, Date modifiedDate,
			OrderByComparator<Block> orderByComparator)
		throws com.gmail.ronnyecu.database.exception.NoSuchBlockException {

		return getPersistence().findByModifiedDate_PrevAndNext(
			blockId, companyId, groupId, modifiedDate, orderByComparator);
	}

	/**
	 * Removes all the blocks where companyId = &#63; and groupId = &#63; and modifiedDate &lt; &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param modifiedDate the modified date
	 */
	public static void removeByModifiedDate(
		long companyId, long groupId, Date modifiedDate) {

		getPersistence().removeByModifiedDate(companyId, groupId, modifiedDate);
	}

	/**
	 * Returns the number of blocks where companyId = &#63; and groupId = &#63; and modifiedDate &lt; &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param modifiedDate the modified date
	 * @return the number of matching blocks
	 */
	public static int countByModifiedDate(
		long companyId, long groupId, Date modifiedDate) {

		return getPersistence().countByModifiedDate(
			companyId, groupId, modifiedDate);
	}

	/**
	 * Returns all the blocks where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @return the matching blocks
	 */
	public static List<Block> findByWithoutDescription(
		long companyId, long groupId) {

		return getPersistence().findByWithoutDescription(companyId, groupId);
	}

	/**
	 * Returns a range of all the blocks where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @return the range of matching blocks
	 */
	public static List<Block> findByWithoutDescription(
		long companyId, long groupId, int start, int end) {

		return getPersistence().findByWithoutDescription(
			companyId, groupId, start, end);
	}

	/**
	 * Returns an ordered range of all the blocks where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByWithoutDescription(long,long, int, int, OrderByComparator)}
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching blocks
	 */
	@Deprecated
	public static List<Block> findByWithoutDescription(
		long companyId, long groupId, int start, int end,
		OrderByComparator<Block> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByWithoutDescription(
			companyId, groupId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns an ordered range of all the blocks where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching blocks
	 */
	public static List<Block> findByWithoutDescription(
		long companyId, long groupId, int start, int end,
		OrderByComparator<Block> orderByComparator) {

		return getPersistence().findByWithoutDescription(
			companyId, groupId, start, end, orderByComparator);
	}

	/**
	 * Returns the first block in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching block
	 * @throws NoSuchBlockException if a matching block could not be found
	 */
	public static Block findByWithoutDescription_First(
			long companyId, long groupId,
			OrderByComparator<Block> orderByComparator)
		throws com.gmail.ronnyecu.database.exception.NoSuchBlockException {

		return getPersistence().findByWithoutDescription_First(
			companyId, groupId, orderByComparator);
	}

	/**
	 * Returns the first block in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching block, or <code>null</code> if a matching block could not be found
	 */
	public static Block fetchByWithoutDescription_First(
		long companyId, long groupId,
		OrderByComparator<Block> orderByComparator) {

		return getPersistence().fetchByWithoutDescription_First(
			companyId, groupId, orderByComparator);
	}

	/**
	 * Returns the last block in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching block
	 * @throws NoSuchBlockException if a matching block could not be found
	 */
	public static Block findByWithoutDescription_Last(
			long companyId, long groupId,
			OrderByComparator<Block> orderByComparator)
		throws com.gmail.ronnyecu.database.exception.NoSuchBlockException {

		return getPersistence().findByWithoutDescription_Last(
			companyId, groupId, orderByComparator);
	}

	/**
	 * Returns the last block in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching block, or <code>null</code> if a matching block could not be found
	 */
	public static Block fetchByWithoutDescription_Last(
		long companyId, long groupId,
		OrderByComparator<Block> orderByComparator) {

		return getPersistence().fetchByWithoutDescription_Last(
			companyId, groupId, orderByComparator);
	}

	/**
	 * Returns the blocks before and after the current block in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param blockId the primary key of the current block
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next block
	 * @throws NoSuchBlockException if a block with the primary key could not be found
	 */
	public static Block[] findByWithoutDescription_PrevAndNext(
			long blockId, long companyId, long groupId,
			OrderByComparator<Block> orderByComparator)
		throws com.gmail.ronnyecu.database.exception.NoSuchBlockException {

		return getPersistence().findByWithoutDescription_PrevAndNext(
			blockId, companyId, groupId, orderByComparator);
	}

	/**
	 * Removes all the blocks where companyId = &#63; and groupId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 */
	public static void removeByWithoutDescription(
		long companyId, long groupId) {

		getPersistence().removeByWithoutDescription(companyId, groupId);
	}

	/**
	 * Returns the number of blocks where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @return the number of matching blocks
	 */
	public static int countByWithoutDescription(long companyId, long groupId) {
		return getPersistence().countByWithoutDescription(companyId, groupId);
	}

	/**
	 * Caches the block in the entity cache if it is enabled.
	 *
	 * @param block the block
	 */
	public static void cacheResult(Block block) {
		getPersistence().cacheResult(block);
	}

	/**
	 * Caches the blocks in the entity cache if it is enabled.
	 *
	 * @param blocks the blocks
	 */
	public static void cacheResult(List<Block> blocks) {
		getPersistence().cacheResult(blocks);
	}

	/**
	 * Creates a new block with the primary key. Does not add the block to the database.
	 *
	 * @param blockId the primary key for the new block
	 * @return the new block
	 */
	public static Block create(long blockId) {
		return getPersistence().create(blockId);
	}

	/**
	 * Removes the block with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param blockId the primary key of the block
	 * @return the block that was removed
	 * @throws NoSuchBlockException if a block with the primary key could not be found
	 */
	public static Block remove(long blockId)
		throws com.gmail.ronnyecu.database.exception.NoSuchBlockException {

		return getPersistence().remove(blockId);
	}

	public static Block updateImpl(Block block) {
		return getPersistence().updateImpl(block);
	}

	/**
	 * Returns the block with the primary key or throws a <code>NoSuchBlockException</code> if it could not be found.
	 *
	 * @param blockId the primary key of the block
	 * @return the block
	 * @throws NoSuchBlockException if a block with the primary key could not be found
	 */
	public static Block findByPrimaryKey(long blockId)
		throws com.gmail.ronnyecu.database.exception.NoSuchBlockException {

		return getPersistence().findByPrimaryKey(blockId);
	}

	/**
	 * Returns the block with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param blockId the primary key of the block
	 * @return the block, or <code>null</code> if a block with the primary key could not be found
	 */
	public static Block fetchByPrimaryKey(long blockId) {
		return getPersistence().fetchByPrimaryKey(blockId);
	}

	/**
	 * Returns all the blocks.
	 *
	 * @return the blocks
	 */
	public static List<Block> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the blocks.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @return the range of blocks
	 */
	public static List<Block> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the blocks.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findAll(int, int, OrderByComparator)}
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of blocks
	 */
	@Deprecated
	public static List<Block> findAll(
		int start, int end, OrderByComparator<Block> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns an ordered range of all the blocks.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of blocks
	 */
	public static List<Block> findAll(
		int start, int end, OrderByComparator<Block> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Removes all the blocks from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of blocks.
	 *
	 * @return the number of blocks
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static Set<String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static BlockPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<BlockPersistence, BlockPersistence>
		_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(BlockPersistence.class);

		ServiceTracker<BlockPersistence, BlockPersistence> serviceTracker =
			new ServiceTracker<BlockPersistence, BlockPersistence>(
				bundle.getBundleContext(), BlockPersistence.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}