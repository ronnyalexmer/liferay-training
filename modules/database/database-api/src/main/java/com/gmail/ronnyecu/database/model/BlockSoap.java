/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.gmail.ronnyecu.database.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class BlockSoap implements Serializable {

	public static BlockSoap toSoapModel(Block model) {
		BlockSoap soapModel = new BlockSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setBlockId(model.getBlockId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setBlockCode(model.getBlockCode());
		soapModel.setFileName(model.getFileName());
		soapModel.setDescription(model.getDescription());

		return soapModel;
	}

	public static BlockSoap[] toSoapModels(Block[] models) {
		BlockSoap[] soapModels = new BlockSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static BlockSoap[][] toSoapModels(Block[][] models) {
		BlockSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new BlockSoap[models.length][models[0].length];
		}
		else {
			soapModels = new BlockSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static BlockSoap[] toSoapModels(List<Block> models) {
		List<BlockSoap> soapModels = new ArrayList<BlockSoap>(models.size());

		for (Block model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new BlockSoap[soapModels.size()]);
	}

	public BlockSoap() {
	}

	public long getPrimaryKey() {
		return _blockId;
	}

	public void setPrimaryKey(long pk) {
		setBlockId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getBlockId() {
		return _blockId;
	}

	public void setBlockId(long blockId) {
		_blockId = blockId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getBlockCode() {
		return _blockCode;
	}

	public void setBlockCode(String blockCode) {
		_blockCode = blockCode;
	}

	public String getFileName() {
		return _fileName;
	}

	public void setFileName(String fileName) {
		_fileName = fileName;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	private String _uuid;
	private long _blockId;
	private long _companyId;
	private long _groupId;
	private Date _createDate;
	private Date _modifiedDate;
	private String _blockCode;
	private String _fileName;
	private String _description;

}