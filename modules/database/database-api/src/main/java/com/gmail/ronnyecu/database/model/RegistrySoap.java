/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.gmail.ronnyecu.database.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class RegistrySoap implements Serializable {

	public static RegistrySoap toSoapModel(Registry model) {
		RegistrySoap soapModel = new RegistrySoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setRegistryId(model.getRegistryId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setNif(model.getNif());
		soapModel.setFullName(model.getFullName());
		soapModel.setAmount(model.getAmount());
		soapModel.setToken(model.getToken());
		soapModel.setBlockCode(model.getBlockCode());

		return soapModel;
	}

	public static RegistrySoap[] toSoapModels(Registry[] models) {
		RegistrySoap[] soapModels = new RegistrySoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static RegistrySoap[][] toSoapModels(Registry[][] models) {
		RegistrySoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new RegistrySoap[models.length][models[0].length];
		}
		else {
			soapModels = new RegistrySoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static RegistrySoap[] toSoapModels(List<Registry> models) {
		List<RegistrySoap> soapModels = new ArrayList<RegistrySoap>(
			models.size());

		for (Registry model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new RegistrySoap[soapModels.size()]);
	}

	public RegistrySoap() {
	}

	public long getPrimaryKey() {
		return _registryId;
	}

	public void setPrimaryKey(long pk) {
		setRegistryId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getRegistryId() {
		return _registryId;
	}

	public void setRegistryId(long registryId) {
		_registryId = registryId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getNif() {
		return _nif;
	}

	public void setNif(String nif) {
		_nif = nif;
	}

	public String getFullName() {
		return _fullName;
	}

	public void setFullName(String fullName) {
		_fullName = fullName;
	}

	public double getAmount() {
		return _amount;
	}

	public void setAmount(double amount) {
		_amount = amount;
	}

	public String getToken() {
		return _token;
	}

	public void setToken(String token) {
		_token = token;
	}

	public String getBlockCode() {
		return _blockCode;
	}

	public void setBlockCode(String blockCode) {
		_blockCode = blockCode;
	}

	private String _uuid;
	private long _registryId;
	private long _companyId;
	private long _groupId;
	private Date _createDate;
	private Date _modifiedDate;
	private String _nif;
	private String _fullName;
	private double _amount;
	private String _token;
	private String _blockCode;

}