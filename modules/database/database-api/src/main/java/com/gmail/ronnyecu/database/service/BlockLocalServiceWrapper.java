/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.gmail.ronnyecu.database.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link BlockLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see BlockLocalService
 * @generated
 */
@ProviderType
public class BlockLocalServiceWrapper
	implements BlockLocalService, ServiceWrapper<BlockLocalService> {

	public BlockLocalServiceWrapper(BlockLocalService blockLocalService) {
		_blockLocalService = blockLocalService;
	}

	/**
	 * Adds the block to the database. Also notifies the appropriate model listeners.
	 *
	 * @param block the block
	 * @return the block that was added
	 */
	@Override
	public com.gmail.ronnyecu.database.model.Block addBlock(
		com.gmail.ronnyecu.database.model.Block block) {

		return _blockLocalService.addBlock(block);
	}

	/**
	 * Creates a new block with the primary key. Does not add the block to the database.
	 *
	 * @param blockId the primary key for the new block
	 * @return the new block
	 */
	@Override
	public com.gmail.ronnyecu.database.model.Block createBlock(long blockId) {
		return _blockLocalService.createBlock(blockId);
	}

	/**
	 * Deletes the block from the database. Also notifies the appropriate model listeners.
	 *
	 * @param block the block
	 * @return the block that was removed
	 */
	@Override
	public com.gmail.ronnyecu.database.model.Block deleteBlock(
		com.gmail.ronnyecu.database.model.Block block) {

		return _blockLocalService.deleteBlock(block);
	}

	/**
	 * Deletes the block with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param blockId the primary key of the block
	 * @return the block that was removed
	 * @throws PortalException if a block with the primary key could not be found
	 */
	@Override
	public com.gmail.ronnyecu.database.model.Block deleteBlock(long blockId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _blockLocalService.deleteBlock(blockId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _blockLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _blockLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _blockLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.gmail.ronnyecu.database.model.impl.BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _blockLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.gmail.ronnyecu.database.model.impl.BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _blockLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _blockLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _blockLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public com.gmail.ronnyecu.database.model.Block fetchBlock(long blockId) {
		return _blockLocalService.fetchBlock(blockId);
	}

	/**
	 * Returns the block matching the UUID and group.
	 *
	 * @param uuid the block's UUID
	 * @param groupId the primary key of the group
	 * @return the matching block, or <code>null</code> if a matching block could not be found
	 */
	@Override
	public com.gmail.ronnyecu.database.model.Block fetchBlockByUuidAndGroupId(
		String uuid, long groupId) {

		return _blockLocalService.fetchBlockByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public java.util.List<com.gmail.ronnyecu.database.model.Block>
		findAllBlock() {

		return _blockLocalService.findAllBlock();
	}

	@Override
	public java.util.List<com.gmail.ronnyecu.database.model.Block>
		findByFileName(
			long companyId, long groupId, String words, int start, int end) {

		return _blockLocalService.findByFileName(
			companyId, groupId, words, start, end);
	}

	@Override
	public java.util.List<com.gmail.ronnyecu.database.model.Block>
		findByFileName(
			long companyId, long groupId, String words, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<com.gmail.ronnyecu.database.model.Block> obc) {

		return _blockLocalService.findByFileName(
			companyId, groupId, words, start, end, obc);
	}

	@Override
	public int findByFileNameCount(long companyId, long groupId, String words) {
		return _blockLocalService.findByFileNameCount(
			companyId, groupId, words);
	}

	@Override
	public java.util.List<com.gmail.ronnyecu.database.model.Block>
		findModiedDateLessThan(
			long companyId, long groupId, java.util.Date date, int start,
			int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<com.gmail.ronnyecu.database.model.Block> obc) {

		return _blockLocalService.findModiedDateLessThan(
			companyId, groupId, date, start, end, obc);
	}

	@Override
	public java.util.List<com.gmail.ronnyecu.database.model.Block>
		findWithoutDescription(
			long companyId, long groupId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<com.gmail.ronnyecu.database.model.Block> obc) {

		return _blockLocalService.findWithoutDescription(
			companyId, groupId, start, end, obc);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _blockLocalService.getActionableDynamicQuery();
	}

	/**
	 * Returns the block with the primary key.
	 *
	 * @param blockId the primary key of the block
	 * @return the block
	 * @throws PortalException if a block with the primary key could not be found
	 */
	@Override
	public com.gmail.ronnyecu.database.model.Block getBlock(long blockId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _blockLocalService.getBlock(blockId);
	}

	/**
	 * Returns the block matching the UUID and group.
	 *
	 * @param uuid the block's UUID
	 * @param groupId the primary key of the group
	 * @return the matching block
	 * @throws PortalException if a matching block could not be found
	 */
	@Override
	public com.gmail.ronnyecu.database.model.Block getBlockByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _blockLocalService.getBlockByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the blocks.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.gmail.ronnyecu.database.model.impl.BlockModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @return the range of blocks
	 */
	@Override
	public java.util.List<com.gmail.ronnyecu.database.model.Block> getBlocks(
		int start, int end) {

		return _blockLocalService.getBlocks(start, end);
	}

	@Override
	public java.util.List<com.gmail.ronnyecu.database.model.Block> getBlocks(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<com.gmail.ronnyecu.database.model.Block> obc) {

		return _blockLocalService.getBlocks(start, end, obc);
	}

	/**
	 * Returns all the blocks matching the UUID and company.
	 *
	 * @param uuid the UUID of the blocks
	 * @param companyId the primary key of the company
	 * @return the matching blocks, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<com.gmail.ronnyecu.database.model.Block>
		getBlocksByUuidAndCompanyId(String uuid, long companyId) {

		return _blockLocalService.getBlocksByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of blocks matching the UUID and company.
	 *
	 * @param uuid the UUID of the blocks
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of blocks
	 * @param end the upper bound of the range of blocks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching blocks, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<com.gmail.ronnyecu.database.model.Block>
		getBlocksByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<com.gmail.ronnyecu.database.model.Block> orderByComparator) {

		return _blockLocalService.getBlocksByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of blocks.
	 *
	 * @return the number of blocks
	 */
	@Override
	public int getBlocksCount() {
		return _blockLocalService.getBlocksCount();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _blockLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _blockLocalService.getIndexableActionableDynamicQuery();
	}

	@Override
	public int getModiedDateLessThanCount(
		long companyId, long groupId, java.util.Date date) {

		return _blockLocalService.getModiedDateLessThanCount(
			companyId, groupId, date);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _blockLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _blockLocalService.getPersistedModel(primaryKeyObj);
	}

	@Override
	public int getWithoutDescriptionCount(long companyId, long groupId) {
		return _blockLocalService.getWithoutDescriptionCount(
			companyId, groupId);
	}

	/**
	 * Updates the block in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param block the block
	 * @return the block that was updated
	 */
	@Override
	public com.gmail.ronnyecu.database.model.Block updateBlock(
		com.gmail.ronnyecu.database.model.Block block) {

		return _blockLocalService.updateBlock(block);
	}

	@Override
	public BlockLocalService getWrappedService() {
		return _blockLocalService;
	}

	@Override
	public void setWrappedService(BlockLocalService blockLocalService) {
		_blockLocalService = blockLocalService;
	}

	private BlockLocalService _blockLocalService;

}