/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.gmail.ronnyecu.database.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.gmail.ronnyecu.database.model.Registry;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * The persistence utility for the registry service. This utility wraps <code>com.gmail.ronnyecu.database.service.persistence.impl.RegistryPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see RegistryPersistence
 * @generated
 */
@ProviderType
public class RegistryUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Registry registry) {
		getPersistence().clearCache(registry);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, Registry> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Registry> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Registry> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Registry> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Registry> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Registry update(Registry registry) {
		return getPersistence().update(registry);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Registry update(
		Registry registry, ServiceContext serviceContext) {

		return getPersistence().update(registry, serviceContext);
	}

	/**
	 * Returns all the registries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching registries
	 */
	public static List<Registry> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the registries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @return the range of matching registries
	 */
	public static List<Registry> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the registries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByUuid(String, int, int, OrderByComparator)}
	 * @param uuid the uuid
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching registries
	 */
	@Deprecated
	public static List<Registry> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Registry> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns an ordered range of all the registries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching registries
	 */
	public static List<Registry> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Registry> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns the first registry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	public static Registry findByUuid_First(
			String uuid, OrderByComparator<Registry> orderByComparator)
		throws com.gmail.ronnyecu.database.exception.NoSuchRegistryException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first registry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching registry, or <code>null</code> if a matching registry could not be found
	 */
	public static Registry fetchByUuid_First(
		String uuid, OrderByComparator<Registry> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last registry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	public static Registry findByUuid_Last(
			String uuid, OrderByComparator<Registry> orderByComparator)
		throws com.gmail.ronnyecu.database.exception.NoSuchRegistryException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last registry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching registry, or <code>null</code> if a matching registry could not be found
	 */
	public static Registry fetchByUuid_Last(
		String uuid, OrderByComparator<Registry> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the registries before and after the current registry in the ordered set where uuid = &#63;.
	 *
	 * @param registryId the primary key of the current registry
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next registry
	 * @throws NoSuchRegistryException if a registry with the primary key could not be found
	 */
	public static Registry[] findByUuid_PrevAndNext(
			long registryId, String uuid,
			OrderByComparator<Registry> orderByComparator)
		throws com.gmail.ronnyecu.database.exception.NoSuchRegistryException {

		return getPersistence().findByUuid_PrevAndNext(
			registryId, uuid, orderByComparator);
	}

	/**
	 * Removes all the registries where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of registries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching registries
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the registry where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchRegistryException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	public static Registry findByUUID_G(String uuid, long groupId)
		throws com.gmail.ronnyecu.database.exception.NoSuchRegistryException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the registry where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #fetchByUUID_G(String,long)}
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching registry, or <code>null</code> if a matching registry could not be found
	 */
	@Deprecated
	public static Registry fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Returns the registry where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching registry, or <code>null</code> if a matching registry could not be found
	 */
	public static Registry fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Removes the registry where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the registry that was removed
	 */
	public static Registry removeByUUID_G(String uuid, long groupId)
		throws com.gmail.ronnyecu.database.exception.NoSuchRegistryException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of registries where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching registries
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the registries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching registries
	 */
	public static List<Registry> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the registries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @return the range of matching registries
	 */
	public static List<Registry> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the registries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByUuid_C(String,long, int, int, OrderByComparator)}
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching registries
	 */
	@Deprecated
	public static List<Registry> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Registry> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns an ordered range of all the registries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching registries
	 */
	public static List<Registry> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Registry> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the first registry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	public static Registry findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<Registry> orderByComparator)
		throws com.gmail.ronnyecu.database.exception.NoSuchRegistryException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first registry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching registry, or <code>null</code> if a matching registry could not be found
	 */
	public static Registry fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<Registry> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last registry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	public static Registry findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<Registry> orderByComparator)
		throws com.gmail.ronnyecu.database.exception.NoSuchRegistryException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last registry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching registry, or <code>null</code> if a matching registry could not be found
	 */
	public static Registry fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<Registry> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the registries before and after the current registry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param registryId the primary key of the current registry
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next registry
	 * @throws NoSuchRegistryException if a registry with the primary key could not be found
	 */
	public static Registry[] findByUuid_C_PrevAndNext(
			long registryId, String uuid, long companyId,
			OrderByComparator<Registry> orderByComparator)
		throws com.gmail.ronnyecu.database.exception.NoSuchRegistryException {

		return getPersistence().findByUuid_C_PrevAndNext(
			registryId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the registries where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of registries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching registries
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the registry where companyId = &#63; and groupId = &#63; and nif = &#63; or throws a <code>NoSuchRegistryException</code> if it could not be found.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param nif the nif
	 * @return the matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	public static Registry findByNif(long companyId, long groupId, String nif)
		throws com.gmail.ronnyecu.database.exception.NoSuchRegistryException {

		return getPersistence().findByNif(companyId, groupId, nif);
	}

	/**
	 * Returns the registry where companyId = &#63; and groupId = &#63; and nif = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #fetchByNif(long,long,String)}
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param nif the nif
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching registry, or <code>null</code> if a matching registry could not be found
	 */
	@Deprecated
	public static Registry fetchByNif(
		long companyId, long groupId, String nif, boolean useFinderCache) {

		return getPersistence().fetchByNif(
			companyId, groupId, nif, useFinderCache);
	}

	/**
	 * Returns the registry where companyId = &#63; and groupId = &#63; and nif = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param nif the nif
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching registry, or <code>null</code> if a matching registry could not be found
	 */
	public static Registry fetchByNif(
		long companyId, long groupId, String nif) {

		return getPersistence().fetchByNif(companyId, groupId, nif);
	}

	/**
	 * Removes the registry where companyId = &#63; and groupId = &#63; and nif = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param nif the nif
	 * @return the registry that was removed
	 */
	public static Registry removeByNif(long companyId, long groupId, String nif)
		throws com.gmail.ronnyecu.database.exception.NoSuchRegistryException {

		return getPersistence().removeByNif(companyId, groupId, nif);
	}

	/**
	 * Returns the number of registries where companyId = &#63; and groupId = &#63; and nif = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param nif the nif
	 * @return the number of matching registries
	 */
	public static int countByNif(long companyId, long groupId, String nif) {
		return getPersistence().countByNif(companyId, groupId, nif);
	}

	/**
	 * Returns all the registries where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @return the matching registries
	 */
	public static List<Registry> findByWithoutName(
		long companyId, long groupId) {

		return getPersistence().findByWithoutName(companyId, groupId);
	}

	/**
	 * Returns a range of all the registries where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @return the range of matching registries
	 */
	public static List<Registry> findByWithoutName(
		long companyId, long groupId, int start, int end) {

		return getPersistence().findByWithoutName(
			companyId, groupId, start, end);
	}

	/**
	 * Returns an ordered range of all the registries where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByWithoutName(long,long, int, int, OrderByComparator)}
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching registries
	 */
	@Deprecated
	public static List<Registry> findByWithoutName(
		long companyId, long groupId, int start, int end,
		OrderByComparator<Registry> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByWithoutName(
			companyId, groupId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns an ordered range of all the registries where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching registries
	 */
	public static List<Registry> findByWithoutName(
		long companyId, long groupId, int start, int end,
		OrderByComparator<Registry> orderByComparator) {

		return getPersistence().findByWithoutName(
			companyId, groupId, start, end, orderByComparator);
	}

	/**
	 * Returns the first registry in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	public static Registry findByWithoutName_First(
			long companyId, long groupId,
			OrderByComparator<Registry> orderByComparator)
		throws com.gmail.ronnyecu.database.exception.NoSuchRegistryException {

		return getPersistence().findByWithoutName_First(
			companyId, groupId, orderByComparator);
	}

	/**
	 * Returns the first registry in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching registry, or <code>null</code> if a matching registry could not be found
	 */
	public static Registry fetchByWithoutName_First(
		long companyId, long groupId,
		OrderByComparator<Registry> orderByComparator) {

		return getPersistence().fetchByWithoutName_First(
			companyId, groupId, orderByComparator);
	}

	/**
	 * Returns the last registry in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	public static Registry findByWithoutName_Last(
			long companyId, long groupId,
			OrderByComparator<Registry> orderByComparator)
		throws com.gmail.ronnyecu.database.exception.NoSuchRegistryException {

		return getPersistence().findByWithoutName_Last(
			companyId, groupId, orderByComparator);
	}

	/**
	 * Returns the last registry in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching registry, or <code>null</code> if a matching registry could not be found
	 */
	public static Registry fetchByWithoutName_Last(
		long companyId, long groupId,
		OrderByComparator<Registry> orderByComparator) {

		return getPersistence().fetchByWithoutName_Last(
			companyId, groupId, orderByComparator);
	}

	/**
	 * Returns the registries before and after the current registry in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param registryId the primary key of the current registry
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next registry
	 * @throws NoSuchRegistryException if a registry with the primary key could not be found
	 */
	public static Registry[] findByWithoutName_PrevAndNext(
			long registryId, long companyId, long groupId,
			OrderByComparator<Registry> orderByComparator)
		throws com.gmail.ronnyecu.database.exception.NoSuchRegistryException {

		return getPersistence().findByWithoutName_PrevAndNext(
			registryId, companyId, groupId, orderByComparator);
	}

	/**
	 * Removes all the registries where companyId = &#63; and groupId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 */
	public static void removeByWithoutName(long companyId, long groupId) {
		getPersistence().removeByWithoutName(companyId, groupId);
	}

	/**
	 * Returns the number of registries where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @return the number of matching registries
	 */
	public static int countByWithoutName(long companyId, long groupId) {
		return getPersistence().countByWithoutName(companyId, groupId);
	}

	/**
	 * Returns all the registries where companyId = &#63; and groupId = &#63; and blockCode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @return the matching registries
	 */
	public static List<Registry> findByBlockCode(
		long companyId, long groupId, String blockCode) {

		return getPersistence().findByBlockCode(companyId, groupId, blockCode);
	}

	/**
	 * Returns a range of all the registries where companyId = &#63; and groupId = &#63; and blockCode = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @return the range of matching registries
	 */
	public static List<Registry> findByBlockCode(
		long companyId, long groupId, String blockCode, int start, int end) {

		return getPersistence().findByBlockCode(
			companyId, groupId, blockCode, start, end);
	}

	/**
	 * Returns an ordered range of all the registries where companyId = &#63; and groupId = &#63; and blockCode = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByBlockCode(long,long,String, int, int, OrderByComparator)}
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching registries
	 */
	@Deprecated
	public static List<Registry> findByBlockCode(
		long companyId, long groupId, String blockCode, int start, int end,
		OrderByComparator<Registry> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByBlockCode(
			companyId, groupId, blockCode, start, end, orderByComparator,
			useFinderCache);
	}

	/**
	 * Returns an ordered range of all the registries where companyId = &#63; and groupId = &#63; and blockCode = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching registries
	 */
	public static List<Registry> findByBlockCode(
		long companyId, long groupId, String blockCode, int start, int end,
		OrderByComparator<Registry> orderByComparator) {

		return getPersistence().findByBlockCode(
			companyId, groupId, blockCode, start, end, orderByComparator);
	}

	/**
	 * Returns the first registry in the ordered set where companyId = &#63; and groupId = &#63; and blockCode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	public static Registry findByBlockCode_First(
			long companyId, long groupId, String blockCode,
			OrderByComparator<Registry> orderByComparator)
		throws com.gmail.ronnyecu.database.exception.NoSuchRegistryException {

		return getPersistence().findByBlockCode_First(
			companyId, groupId, blockCode, orderByComparator);
	}

	/**
	 * Returns the first registry in the ordered set where companyId = &#63; and groupId = &#63; and blockCode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching registry, or <code>null</code> if a matching registry could not be found
	 */
	public static Registry fetchByBlockCode_First(
		long companyId, long groupId, String blockCode,
		OrderByComparator<Registry> orderByComparator) {

		return getPersistence().fetchByBlockCode_First(
			companyId, groupId, blockCode, orderByComparator);
	}

	/**
	 * Returns the last registry in the ordered set where companyId = &#63; and groupId = &#63; and blockCode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	public static Registry findByBlockCode_Last(
			long companyId, long groupId, String blockCode,
			OrderByComparator<Registry> orderByComparator)
		throws com.gmail.ronnyecu.database.exception.NoSuchRegistryException {

		return getPersistence().findByBlockCode_Last(
			companyId, groupId, blockCode, orderByComparator);
	}

	/**
	 * Returns the last registry in the ordered set where companyId = &#63; and groupId = &#63; and blockCode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching registry, or <code>null</code> if a matching registry could not be found
	 */
	public static Registry fetchByBlockCode_Last(
		long companyId, long groupId, String blockCode,
		OrderByComparator<Registry> orderByComparator) {

		return getPersistence().fetchByBlockCode_Last(
			companyId, groupId, blockCode, orderByComparator);
	}

	/**
	 * Returns the registries before and after the current registry in the ordered set where companyId = &#63; and groupId = &#63; and blockCode = &#63;.
	 *
	 * @param registryId the primary key of the current registry
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next registry
	 * @throws NoSuchRegistryException if a registry with the primary key could not be found
	 */
	public static Registry[] findByBlockCode_PrevAndNext(
			long registryId, long companyId, long groupId, String blockCode,
			OrderByComparator<Registry> orderByComparator)
		throws com.gmail.ronnyecu.database.exception.NoSuchRegistryException {

		return getPersistence().findByBlockCode_PrevAndNext(
			registryId, companyId, groupId, blockCode, orderByComparator);
	}

	/**
	 * Removes all the registries where companyId = &#63; and groupId = &#63; and blockCode = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 */
	public static void removeByBlockCode(
		long companyId, long groupId, String blockCode) {

		getPersistence().removeByBlockCode(companyId, groupId, blockCode);
	}

	/**
	 * Returns the number of registries where companyId = &#63; and groupId = &#63; and blockCode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @return the number of matching registries
	 */
	public static int countByBlockCode(
		long companyId, long groupId, String blockCode) {

		return getPersistence().countByBlockCode(companyId, groupId, blockCode);
	}

	/**
	 * Returns all the registries where companyId = &#63; and groupId = &#63; and fullName = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 * @return the matching registries
	 */
	public static List<Registry> findByName(
		long companyId, long groupId, String fullName) {

		return getPersistence().findByName(companyId, groupId, fullName);
	}

	/**
	 * Returns a range of all the registries where companyId = &#63; and groupId = &#63; and fullName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @return the range of matching registries
	 */
	public static List<Registry> findByName(
		long companyId, long groupId, String fullName, int start, int end) {

		return getPersistence().findByName(
			companyId, groupId, fullName, start, end);
	}

	/**
	 * Returns an ordered range of all the registries where companyId = &#63; and groupId = &#63; and fullName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByName(long,long,String, int, int, OrderByComparator)}
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching registries
	 */
	@Deprecated
	public static List<Registry> findByName(
		long companyId, long groupId, String fullName, int start, int end,
		OrderByComparator<Registry> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByName(
			companyId, groupId, fullName, start, end, orderByComparator,
			useFinderCache);
	}

	/**
	 * Returns an ordered range of all the registries where companyId = &#63; and groupId = &#63; and fullName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching registries
	 */
	public static List<Registry> findByName(
		long companyId, long groupId, String fullName, int start, int end,
		OrderByComparator<Registry> orderByComparator) {

		return getPersistence().findByName(
			companyId, groupId, fullName, start, end, orderByComparator);
	}

	/**
	 * Returns the first registry in the ordered set where companyId = &#63; and groupId = &#63; and fullName = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	public static Registry findByName_First(
			long companyId, long groupId, String fullName,
			OrderByComparator<Registry> orderByComparator)
		throws com.gmail.ronnyecu.database.exception.NoSuchRegistryException {

		return getPersistence().findByName_First(
			companyId, groupId, fullName, orderByComparator);
	}

	/**
	 * Returns the first registry in the ordered set where companyId = &#63; and groupId = &#63; and fullName = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching registry, or <code>null</code> if a matching registry could not be found
	 */
	public static Registry fetchByName_First(
		long companyId, long groupId, String fullName,
		OrderByComparator<Registry> orderByComparator) {

		return getPersistence().fetchByName_First(
			companyId, groupId, fullName, orderByComparator);
	}

	/**
	 * Returns the last registry in the ordered set where companyId = &#63; and groupId = &#63; and fullName = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	public static Registry findByName_Last(
			long companyId, long groupId, String fullName,
			OrderByComparator<Registry> orderByComparator)
		throws com.gmail.ronnyecu.database.exception.NoSuchRegistryException {

		return getPersistence().findByName_Last(
			companyId, groupId, fullName, orderByComparator);
	}

	/**
	 * Returns the last registry in the ordered set where companyId = &#63; and groupId = &#63; and fullName = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching registry, or <code>null</code> if a matching registry could not be found
	 */
	public static Registry fetchByName_Last(
		long companyId, long groupId, String fullName,
		OrderByComparator<Registry> orderByComparator) {

		return getPersistence().fetchByName_Last(
			companyId, groupId, fullName, orderByComparator);
	}

	/**
	 * Returns the registries before and after the current registry in the ordered set where companyId = &#63; and groupId = &#63; and fullName = &#63;.
	 *
	 * @param registryId the primary key of the current registry
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next registry
	 * @throws NoSuchRegistryException if a registry with the primary key could not be found
	 */
	public static Registry[] findByName_PrevAndNext(
			long registryId, long companyId, long groupId, String fullName,
			OrderByComparator<Registry> orderByComparator)
		throws com.gmail.ronnyecu.database.exception.NoSuchRegistryException {

		return getPersistence().findByName_PrevAndNext(
			registryId, companyId, groupId, fullName, orderByComparator);
	}

	/**
	 * Removes all the registries where companyId = &#63; and groupId = &#63; and fullName = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 */
	public static void removeByName(
		long companyId, long groupId, String fullName) {

		getPersistence().removeByName(companyId, groupId, fullName);
	}

	/**
	 * Returns the number of registries where companyId = &#63; and groupId = &#63; and fullName = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 * @return the number of matching registries
	 */
	public static int countByName(
		long companyId, long groupId, String fullName) {

		return getPersistence().countByName(companyId, groupId, fullName);
	}

	/**
	 * Caches the registry in the entity cache if it is enabled.
	 *
	 * @param registry the registry
	 */
	public static void cacheResult(Registry registry) {
		getPersistence().cacheResult(registry);
	}

	/**
	 * Caches the registries in the entity cache if it is enabled.
	 *
	 * @param registries the registries
	 */
	public static void cacheResult(List<Registry> registries) {
		getPersistence().cacheResult(registries);
	}

	/**
	 * Creates a new registry with the primary key. Does not add the registry to the database.
	 *
	 * @param registryId the primary key for the new registry
	 * @return the new registry
	 */
	public static Registry create(long registryId) {
		return getPersistence().create(registryId);
	}

	/**
	 * Removes the registry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param registryId the primary key of the registry
	 * @return the registry that was removed
	 * @throws NoSuchRegistryException if a registry with the primary key could not be found
	 */
	public static Registry remove(long registryId)
		throws com.gmail.ronnyecu.database.exception.NoSuchRegistryException {

		return getPersistence().remove(registryId);
	}

	public static Registry updateImpl(Registry registry) {
		return getPersistence().updateImpl(registry);
	}

	/**
	 * Returns the registry with the primary key or throws a <code>NoSuchRegistryException</code> if it could not be found.
	 *
	 * @param registryId the primary key of the registry
	 * @return the registry
	 * @throws NoSuchRegistryException if a registry with the primary key could not be found
	 */
	public static Registry findByPrimaryKey(long registryId)
		throws com.gmail.ronnyecu.database.exception.NoSuchRegistryException {

		return getPersistence().findByPrimaryKey(registryId);
	}

	/**
	 * Returns the registry with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param registryId the primary key of the registry
	 * @return the registry, or <code>null</code> if a registry with the primary key could not be found
	 */
	public static Registry fetchByPrimaryKey(long registryId) {
		return getPersistence().fetchByPrimaryKey(registryId);
	}

	/**
	 * Returns all the registries.
	 *
	 * @return the registries
	 */
	public static List<Registry> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the registries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @return the range of registries
	 */
	public static List<Registry> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the registries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findAll(int, int, OrderByComparator)}
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of registries
	 */
	@Deprecated
	public static List<Registry> findAll(
		int start, int end, OrderByComparator<Registry> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns an ordered range of all the registries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of registries
	 */
	public static List<Registry> findAll(
		int start, int end, OrderByComparator<Registry> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Removes all the registries from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of registries.
	 *
	 * @return the number of registries
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static Set<String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static RegistryPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<RegistryPersistence, RegistryPersistence>
		_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(RegistryPersistence.class);

		ServiceTracker<RegistryPersistence, RegistryPersistence>
			serviceTracker =
				new ServiceTracker<RegistryPersistence, RegistryPersistence>(
					bundle.getBundleContext(), RegistryPersistence.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}