/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.gmail.ronnyecu.database.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.gmail.ronnyecu.database.model.Registry;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Registry in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class RegistryCacheModel
	implements CacheModel<Registry>, Externalizable {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof RegistryCacheModel)) {
			return false;
		}

		RegistryCacheModel registryCacheModel = (RegistryCacheModel)obj;

		if (registryId == registryCacheModel.registryId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, registryId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", registryId=");
		sb.append(registryId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", nif=");
		sb.append(nif);
		sb.append(", fullName=");
		sb.append(fullName);
		sb.append(", amount=");
		sb.append(amount);
		sb.append(", token=");
		sb.append(token);
		sb.append(", blockCode=");
		sb.append(blockCode);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Registry toEntityModel() {
		RegistryImpl registryImpl = new RegistryImpl();

		if (uuid == null) {
			registryImpl.setUuid("");
		}
		else {
			registryImpl.setUuid(uuid);
		}

		registryImpl.setRegistryId(registryId);
		registryImpl.setCompanyId(companyId);
		registryImpl.setGroupId(groupId);

		if (createDate == Long.MIN_VALUE) {
			registryImpl.setCreateDate(null);
		}
		else {
			registryImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			registryImpl.setModifiedDate(null);
		}
		else {
			registryImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (nif == null) {
			registryImpl.setNif("");
		}
		else {
			registryImpl.setNif(nif);
		}

		if (fullName == null) {
			registryImpl.setFullName("");
		}
		else {
			registryImpl.setFullName(fullName);
		}

		registryImpl.setAmount(amount);

		if (token == null) {
			registryImpl.setToken("");
		}
		else {
			registryImpl.setToken(token);
		}

		if (blockCode == null) {
			registryImpl.setBlockCode("");
		}
		else {
			registryImpl.setBlockCode(blockCode);
		}

		registryImpl.resetOriginalValues();

		return registryImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		registryId = objectInput.readLong();

		companyId = objectInput.readLong();

		groupId = objectInput.readLong();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		nif = objectInput.readUTF();
		fullName = objectInput.readUTF();

		amount = objectInput.readDouble();
		token = objectInput.readUTF();
		blockCode = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(registryId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(groupId);
		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (nif == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(nif);
		}

		if (fullName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(fullName);
		}

		objectOutput.writeDouble(amount);

		if (token == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(token);
		}

		if (blockCode == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(blockCode);
		}
	}

	public String uuid;
	public long registryId;
	public long companyId;
	public long groupId;
	public long createDate;
	public long modifiedDate;
	public String nif;
	public String fullName;
	public double amount;
	public String token;
	public String blockCode;

}