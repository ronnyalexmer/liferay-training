/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.gmail.ronnyecu.database.service.impl;

import com.gmail.ronnyecu.database.model.Block;
import com.gmail.ronnyecu.database.service.base.BlockLocalServiceBaseImpl;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.util.Date;
import java.util.List;

/**
 * The implementation of the block local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>com.gmail.ronnyecu.database.service.BlockLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see BlockLocalServiceBaseImpl
 */
public class BlockLocalServiceImpl extends BlockLocalServiceBaseImpl {

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. Use <code>com.gmail.ronnyecu.database.service.BlockLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>com.gmail.ronnyecu.database.service.BlockLocalServiceUtil</code>.
     */
    public List<Block> findAllBlock() {
        return getBlockPersistence().findAll();
    }

    public List<Block> getBlocks(int start, int end, OrderByComparator<Block> obc) {
        return getBlockPersistence().findAll(start, end, obc);
    }


    public List<Block> findByFileName(long companyId, long groupId, String words, int start, int end) {
        return getBlockPersistence().findByFileName(companyId, groupId, StringPool.PERCENT + words + StringPool.PERCENT, start, end);
    }

    public List<Block> findByFileName(long companyId, long groupId, String words, int start, int end, OrderByComparator<Block> obc) {
        return getBlockPersistence().findByFileName(companyId, groupId, StringPool.PERCENT + words + StringPool.PERCENT, start, end, obc);
    }

    public List<Block> findWithoutDescription(long companyId, long groupId, int start, int end, OrderByComparator<Block> obc) {
        return getBlockPersistence().findByWithoutDescription(companyId, groupId, start, end, obc);
    }

    public List<Block> findModiedDateLessThan(long companyId, long groupId, Date date, int start, int end, OrderByComparator<Block> obc) {
        return getBlockPersistence().findByModifiedDate(companyId, groupId, date, start, end, obc);
    }

    public int getModiedDateLessThanCount(long companyId, long groupId, Date date) {
        return getBlockPersistence().countByModifiedDate(companyId, groupId, date);
    }

    public int getWithoutDescriptionCount(long companyId, long groupId) {
        return getBlockPersistence().countByWithoutDescription(companyId, groupId);
    }

    public int findByFileNameCount(long companyId, long groupId, String words) {
        return getBlockPersistence().findByFileName(companyId, groupId, StringPool.PERCENT + words + StringPool.PERCENT).size();
    }
}