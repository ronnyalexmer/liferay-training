/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.gmail.ronnyecu.database.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.gmail.ronnyecu.database.exception.NoSuchRegistryException;
import com.gmail.ronnyecu.database.model.Registry;
import com.gmail.ronnyecu.database.model.impl.RegistryImpl;
import com.gmail.ronnyecu.database.model.impl.RegistryModelImpl;
import com.gmail.ronnyecu.database.service.persistence.RegistryPersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the registry service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class RegistryPersistenceImpl
	extends BasePersistenceImpl<Registry> implements RegistryPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>RegistryUtil</code> to access the registry persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		RegistryImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the registries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching registries
	 */
	@Override
	public List<Registry> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the registries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @return the range of matching registries
	 */
	@Override
	public List<Registry> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the registries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByUuid(String, int, int, OrderByComparator)}
	 * @param uuid the uuid
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching registries
	 */
	@Deprecated
	@Override
	public List<Registry> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Registry> orderByComparator, boolean useFinderCache) {

		return findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the registries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching registries
	 */
	@Override
	public List<Registry> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Registry> orderByComparator) {

		uuid = Objects.toString(uuid, "");

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindByUuid;
			finderArgs = new Object[] {uuid};
		}
		else {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<Registry> list = (List<Registry>)finderCache.getResult(
			finderPath, finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Registry registry : list) {
				if (!uuid.equals(registry.getUuid())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_REGISTRY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else if (pagination) {
				query.append(RegistryModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<Registry>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Registry>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first registry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	@Override
	public Registry findByUuid_First(
			String uuid, OrderByComparator<Registry> orderByComparator)
		throws NoSuchRegistryException {

		Registry registry = fetchByUuid_First(uuid, orderByComparator);

		if (registry != null) {
			return registry;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append("}");

		throw new NoSuchRegistryException(msg.toString());
	}

	/**
	 * Returns the first registry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching registry, or <code>null</code> if a matching registry could not be found
	 */
	@Override
	public Registry fetchByUuid_First(
		String uuid, OrderByComparator<Registry> orderByComparator) {

		List<Registry> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last registry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	@Override
	public Registry findByUuid_Last(
			String uuid, OrderByComparator<Registry> orderByComparator)
		throws NoSuchRegistryException {

		Registry registry = fetchByUuid_Last(uuid, orderByComparator);

		if (registry != null) {
			return registry;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append("}");

		throw new NoSuchRegistryException(msg.toString());
	}

	/**
	 * Returns the last registry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching registry, or <code>null</code> if a matching registry could not be found
	 */
	@Override
	public Registry fetchByUuid_Last(
		String uuid, OrderByComparator<Registry> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<Registry> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the registries before and after the current registry in the ordered set where uuid = &#63;.
	 *
	 * @param registryId the primary key of the current registry
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next registry
	 * @throws NoSuchRegistryException if a registry with the primary key could not be found
	 */
	@Override
	public Registry[] findByUuid_PrevAndNext(
			long registryId, String uuid,
			OrderByComparator<Registry> orderByComparator)
		throws NoSuchRegistryException {

		uuid = Objects.toString(uuid, "");

		Registry registry = findByPrimaryKey(registryId);

		Session session = null;

		try {
			session = openSession();

			Registry[] array = new RegistryImpl[3];

			array[0] = getByUuid_PrevAndNext(
				session, registry, uuid, orderByComparator, true);

			array[1] = registry;

			array[2] = getByUuid_PrevAndNext(
				session, registry, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Registry getByUuid_PrevAndNext(
		Session session, Registry registry, String uuid,
		OrderByComparator<Registry> orderByComparator, boolean previous) {

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_REGISTRY_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(RegistryModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(registry)) {

				qPos.add(orderByConditionValue);
			}
		}

		List<Registry> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the registries where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (Registry registry :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(registry);
		}
	}

	/**
	 * Returns the number of registries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching registries
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_REGISTRY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 =
		"registry.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(registry.uuid IS NULL OR registry.uuid = '')";

	private FinderPath _finderPathFetchByUUID_G;
	private FinderPath _finderPathCountByUUID_G;

	/**
	 * Returns the registry where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchRegistryException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	@Override
	public Registry findByUUID_G(String uuid, long groupId)
		throws NoSuchRegistryException {

		Registry registry = fetchByUUID_G(uuid, groupId);

		if (registry == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("uuid=");
			msg.append(uuid);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchRegistryException(msg.toString());
		}

		return registry;
	}

	/**
	 * Returns the registry where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #fetchByUUID_G(String,long)}
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching registry, or <code>null</code> if a matching registry could not be found
	 */
	@Deprecated
	@Override
	public Registry fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the registry where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching registry, or <code>null</code> if a matching registry could not be found
	 */
	@Override
	public Registry fetchByUUID_G(String uuid, long groupId) {
		uuid = Objects.toString(uuid, "");

		Object[] finderArgs = new Object[] {uuid, groupId};

		Object result = finderCache.getResult(
			_finderPathFetchByUUID_G, finderArgs, this);

		if (result instanceof Registry) {
			Registry registry = (Registry)result;

			if (!Objects.equals(uuid, registry.getUuid()) ||
				(groupId != registry.getGroupId())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_REGISTRY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				List<Registry> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(
						_finderPathFetchByUUID_G, finderArgs, list);
				}
				else {
					Registry registry = list.get(0);

					result = registry;

					cacheResult(registry);
				}
			}
			catch (Exception e) {
				finderCache.removeResult(_finderPathFetchByUUID_G, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Registry)result;
		}
	}

	/**
	 * Removes the registry where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the registry that was removed
	 */
	@Override
	public Registry removeByUUID_G(String uuid, long groupId)
		throws NoSuchRegistryException {

		Registry registry = findByUUID_G(uuid, groupId);

		return remove(registry);
	}

	/**
	 * Returns the number of registries where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching registries
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUUID_G;

		Object[] finderArgs = new Object[] {uuid, groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_REGISTRY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_2 =
		"registry.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_G_UUID_3 =
		"(registry.uuid IS NULL OR registry.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 =
		"registry.groupId = ?";

	private FinderPath _finderPathWithPaginationFindByUuid_C;
	private FinderPath _finderPathWithoutPaginationFindByUuid_C;
	private FinderPath _finderPathCountByUuid_C;

	/**
	 * Returns all the registries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching registries
	 */
	@Override
	public List<Registry> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(
			uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the registries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @return the range of matching registries
	 */
	@Override
	public List<Registry> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the registries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByUuid_C(String,long, int, int, OrderByComparator)}
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching registries
	 */
	@Deprecated
	@Override
	public List<Registry> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Registry> orderByComparator, boolean useFinderCache) {

		return findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the registries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching registries
	 */
	@Override
	public List<Registry> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Registry> orderByComparator) {

		uuid = Objects.toString(uuid, "");

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindByUuid_C;
			finderArgs = new Object[] {uuid, companyId};
		}
		else {
			finderPath = _finderPathWithPaginationFindByUuid_C;
			finderArgs = new Object[] {
				uuid, companyId, start, end, orderByComparator
			};
		}

		List<Registry> list = (List<Registry>)finderCache.getResult(
			finderPath, finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Registry registry : list) {
				if (!uuid.equals(registry.getUuid()) ||
					(companyId != registry.getCompanyId())) {

					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_REGISTRY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else if (pagination) {
				query.append(RegistryModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				if (!pagination) {
					list = (List<Registry>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Registry>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first registry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	@Override
	public Registry findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<Registry> orderByComparator)
		throws NoSuchRegistryException {

		Registry registry = fetchByUuid_C_First(
			uuid, companyId, orderByComparator);

		if (registry != null) {
			return registry;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append("}");

		throw new NoSuchRegistryException(msg.toString());
	}

	/**
	 * Returns the first registry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching registry, or <code>null</code> if a matching registry could not be found
	 */
	@Override
	public Registry fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<Registry> orderByComparator) {

		List<Registry> list = findByUuid_C(
			uuid, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last registry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	@Override
	public Registry findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<Registry> orderByComparator)
		throws NoSuchRegistryException {

		Registry registry = fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);

		if (registry != null) {
			return registry;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append("}");

		throw new NoSuchRegistryException(msg.toString());
	}

	/**
	 * Returns the last registry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching registry, or <code>null</code> if a matching registry could not be found
	 */
	@Override
	public Registry fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<Registry> orderByComparator) {

		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<Registry> list = findByUuid_C(
			uuid, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the registries before and after the current registry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param registryId the primary key of the current registry
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next registry
	 * @throws NoSuchRegistryException if a registry with the primary key could not be found
	 */
	@Override
	public Registry[] findByUuid_C_PrevAndNext(
			long registryId, String uuid, long companyId,
			OrderByComparator<Registry> orderByComparator)
		throws NoSuchRegistryException {

		uuid = Objects.toString(uuid, "");

		Registry registry = findByPrimaryKey(registryId);

		Session session = null;

		try {
			session = openSession();

			Registry[] array = new RegistryImpl[3];

			array[0] = getByUuid_C_PrevAndNext(
				session, registry, uuid, companyId, orderByComparator, true);

			array[1] = registry;

			array[2] = getByUuid_C_PrevAndNext(
				session, registry, uuid, companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Registry getByUuid_C_PrevAndNext(
		Session session, Registry registry, String uuid, long companyId,
		OrderByComparator<Registry> orderByComparator, boolean previous) {

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_REGISTRY_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(RegistryModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		qPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(registry)) {

				qPos.add(orderByConditionValue);
			}
		}

		List<Registry> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the registries where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (Registry registry :
				findByUuid_C(
					uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(registry);
		}
	}

	/**
	 * Returns the number of registries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching registries
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid_C;

		Object[] finderArgs = new Object[] {uuid, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_REGISTRY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_2 =
		"registry.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_C_UUID_3 =
		"(registry.uuid IS NULL OR registry.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 =
		"registry.companyId = ?";

	private FinderPath _finderPathFetchByNif;
	private FinderPath _finderPathCountByNif;

	/**
	 * Returns the registry where companyId = &#63; and groupId = &#63; and nif = &#63; or throws a <code>NoSuchRegistryException</code> if it could not be found.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param nif the nif
	 * @return the matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	@Override
	public Registry findByNif(long companyId, long groupId, String nif)
		throws NoSuchRegistryException {

		Registry registry = fetchByNif(companyId, groupId, nif);

		if (registry == null) {
			StringBundler msg = new StringBundler(8);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("companyId=");
			msg.append(companyId);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append(", nif=");
			msg.append(nif);

			msg.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchRegistryException(msg.toString());
		}

		return registry;
	}

	/**
	 * Returns the registry where companyId = &#63; and groupId = &#63; and nif = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #fetchByNif(long,long,String)}
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param nif the nif
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching registry, or <code>null</code> if a matching registry could not be found
	 */
	@Deprecated
	@Override
	public Registry fetchByNif(
		long companyId, long groupId, String nif, boolean useFinderCache) {

		return fetchByNif(companyId, groupId, nif);
	}

	/**
	 * Returns the registry where companyId = &#63; and groupId = &#63; and nif = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param nif the nif
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching registry, or <code>null</code> if a matching registry could not be found
	 */
	@Override
	public Registry fetchByNif(long companyId, long groupId, String nif) {
		nif = Objects.toString(nif, "");

		Object[] finderArgs = new Object[] {companyId, groupId, nif};

		Object result = finderCache.getResult(
			_finderPathFetchByNif, finderArgs, this);

		if (result instanceof Registry) {
			Registry registry = (Registry)result;

			if ((companyId != registry.getCompanyId()) ||
				(groupId != registry.getGroupId()) ||
				!Objects.equals(nif, registry.getNif())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(5);

			query.append(_SQL_SELECT_REGISTRY_WHERE);

			query.append(_FINDER_COLUMN_NIF_COMPANYID_2);

			query.append(_FINDER_COLUMN_NIF_GROUPID_2);

			boolean bindNif = false;

			if (nif.isEmpty()) {
				query.append(_FINDER_COLUMN_NIF_NIF_3);
			}
			else {
				bindNif = true;

				query.append(_FINDER_COLUMN_NIF_NIF_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(groupId);

				if (bindNif) {
					qPos.add(nif);
				}

				List<Registry> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(
						_finderPathFetchByNif, finderArgs, list);
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							_log.warn(
								"RegistryPersistenceImpl.fetchByNif(long, long, String, boolean) with parameters (" +
									StringUtil.merge(finderArgs) +
										") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					Registry registry = list.get(0);

					result = registry;

					cacheResult(registry);
				}
			}
			catch (Exception e) {
				finderCache.removeResult(_finderPathFetchByNif, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Registry)result;
		}
	}

	/**
	 * Removes the registry where companyId = &#63; and groupId = &#63; and nif = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param nif the nif
	 * @return the registry that was removed
	 */
	@Override
	public Registry removeByNif(long companyId, long groupId, String nif)
		throws NoSuchRegistryException {

		Registry registry = findByNif(companyId, groupId, nif);

		return remove(registry);
	}

	/**
	 * Returns the number of registries where companyId = &#63; and groupId = &#63; and nif = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param nif the nif
	 * @return the number of matching registries
	 */
	@Override
	public int countByNif(long companyId, long groupId, String nif) {
		nif = Objects.toString(nif, "");

		FinderPath finderPath = _finderPathCountByNif;

		Object[] finderArgs = new Object[] {companyId, groupId, nif};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_REGISTRY_WHERE);

			query.append(_FINDER_COLUMN_NIF_COMPANYID_2);

			query.append(_FINDER_COLUMN_NIF_GROUPID_2);

			boolean bindNif = false;

			if (nif.isEmpty()) {
				query.append(_FINDER_COLUMN_NIF_NIF_3);
			}
			else {
				bindNif = true;

				query.append(_FINDER_COLUMN_NIF_NIF_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(groupId);

				if (bindNif) {
					qPos.add(nif);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_NIF_COMPANYID_2 =
		"registry.companyId = ? AND ";

	private static final String _FINDER_COLUMN_NIF_GROUPID_2 =
		"registry.groupId = ? AND ";

	private static final String _FINDER_COLUMN_NIF_NIF_2 = "registry.nif = ?";

	private static final String _FINDER_COLUMN_NIF_NIF_3 =
		"(registry.nif IS NULL OR registry.nif = '')";

	private FinderPath _finderPathWithPaginationFindByWithoutName;
	private FinderPath _finderPathWithoutPaginationFindByWithoutName;
	private FinderPath _finderPathCountByWithoutName;

	/**
	 * Returns all the registries where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @return the matching registries
	 */
	@Override
	public List<Registry> findByWithoutName(long companyId, long groupId) {
		return findByWithoutName(
			companyId, groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the registries where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @return the range of matching registries
	 */
	@Override
	public List<Registry> findByWithoutName(
		long companyId, long groupId, int start, int end) {

		return findByWithoutName(companyId, groupId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the registries where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByWithoutName(long,long, int, int, OrderByComparator)}
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching registries
	 */
	@Deprecated
	@Override
	public List<Registry> findByWithoutName(
		long companyId, long groupId, int start, int end,
		OrderByComparator<Registry> orderByComparator, boolean useFinderCache) {

		return findByWithoutName(
			companyId, groupId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the registries where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching registries
	 */
	@Override
	public List<Registry> findByWithoutName(
		long companyId, long groupId, int start, int end,
		OrderByComparator<Registry> orderByComparator) {

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindByWithoutName;
			finderArgs = new Object[] {companyId, groupId};
		}
		else {
			finderPath = _finderPathWithPaginationFindByWithoutName;
			finderArgs = new Object[] {
				companyId, groupId, start, end, orderByComparator
			};
		}

		List<Registry> list = (List<Registry>)finderCache.getResult(
			finderPath, finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Registry registry : list) {
				if ((companyId != registry.getCompanyId()) ||
					(groupId != registry.getGroupId())) {

					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_REGISTRY_WHERE);

			query.append(_FINDER_COLUMN_WITHOUTNAME_COMPANYID_2);

			query.append(_FINDER_COLUMN_WITHOUTNAME_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else if (pagination) {
				query.append(RegistryModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(groupId);

				if (!pagination) {
					list = (List<Registry>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Registry>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first registry in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	@Override
	public Registry findByWithoutName_First(
			long companyId, long groupId,
			OrderByComparator<Registry> orderByComparator)
		throws NoSuchRegistryException {

		Registry registry = fetchByWithoutName_First(
			companyId, groupId, orderByComparator);

		if (registry != null) {
			return registry;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", groupId=");
		msg.append(groupId);

		msg.append("}");

		throw new NoSuchRegistryException(msg.toString());
	}

	/**
	 * Returns the first registry in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching registry, or <code>null</code> if a matching registry could not be found
	 */
	@Override
	public Registry fetchByWithoutName_First(
		long companyId, long groupId,
		OrderByComparator<Registry> orderByComparator) {

		List<Registry> list = findByWithoutName(
			companyId, groupId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last registry in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	@Override
	public Registry findByWithoutName_Last(
			long companyId, long groupId,
			OrderByComparator<Registry> orderByComparator)
		throws NoSuchRegistryException {

		Registry registry = fetchByWithoutName_Last(
			companyId, groupId, orderByComparator);

		if (registry != null) {
			return registry;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", groupId=");
		msg.append(groupId);

		msg.append("}");

		throw new NoSuchRegistryException(msg.toString());
	}

	/**
	 * Returns the last registry in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching registry, or <code>null</code> if a matching registry could not be found
	 */
	@Override
	public Registry fetchByWithoutName_Last(
		long companyId, long groupId,
		OrderByComparator<Registry> orderByComparator) {

		int count = countByWithoutName(companyId, groupId);

		if (count == 0) {
			return null;
		}

		List<Registry> list = findByWithoutName(
			companyId, groupId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the registries before and after the current registry in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param registryId the primary key of the current registry
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next registry
	 * @throws NoSuchRegistryException if a registry with the primary key could not be found
	 */
	@Override
	public Registry[] findByWithoutName_PrevAndNext(
			long registryId, long companyId, long groupId,
			OrderByComparator<Registry> orderByComparator)
		throws NoSuchRegistryException {

		Registry registry = findByPrimaryKey(registryId);

		Session session = null;

		try {
			session = openSession();

			Registry[] array = new RegistryImpl[3];

			array[0] = getByWithoutName_PrevAndNext(
				session, registry, companyId, groupId, orderByComparator, true);

			array[1] = registry;

			array[2] = getByWithoutName_PrevAndNext(
				session, registry, companyId, groupId, orderByComparator,
				false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Registry getByWithoutName_PrevAndNext(
		Session session, Registry registry, long companyId, long groupId,
		OrderByComparator<Registry> orderByComparator, boolean previous) {

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_REGISTRY_WHERE);

		query.append(_FINDER_COLUMN_WITHOUTNAME_COMPANYID_2);

		query.append(_FINDER_COLUMN_WITHOUTNAME_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(RegistryModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		qPos.add(groupId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(registry)) {

				qPos.add(orderByConditionValue);
			}
		}

		List<Registry> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the registries where companyId = &#63; and groupId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 */
	@Override
	public void removeByWithoutName(long companyId, long groupId) {
		for (Registry registry :
				findByWithoutName(
					companyId, groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(registry);
		}
	}

	/**
	 * Returns the number of registries where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @return the number of matching registries
	 */
	@Override
	public int countByWithoutName(long companyId, long groupId) {
		FinderPath finderPath = _finderPathCountByWithoutName;

		Object[] finderArgs = new Object[] {companyId, groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_REGISTRY_WHERE);

			query.append(_FINDER_COLUMN_WITHOUTNAME_COMPANYID_2);

			query.append(_FINDER_COLUMN_WITHOUTNAME_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_WITHOUTNAME_COMPANYID_2 =
		"registry.companyId = ? AND ";

	private static final String _FINDER_COLUMN_WITHOUTNAME_GROUPID_2 =
		"registry.groupId = ? AND registry.fullName is null";

	private FinderPath _finderPathWithPaginationFindByBlockCode;
	private FinderPath _finderPathWithoutPaginationFindByBlockCode;
	private FinderPath _finderPathCountByBlockCode;

	/**
	 * Returns all the registries where companyId = &#63; and groupId = &#63; and blockCode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @return the matching registries
	 */
	@Override
	public List<Registry> findByBlockCode(
		long companyId, long groupId, String blockCode) {

		return findByBlockCode(
			companyId, groupId, blockCode, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the registries where companyId = &#63; and groupId = &#63; and blockCode = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @return the range of matching registries
	 */
	@Override
	public List<Registry> findByBlockCode(
		long companyId, long groupId, String blockCode, int start, int end) {

		return findByBlockCode(companyId, groupId, blockCode, start, end, null);
	}

	/**
	 * Returns an ordered range of all the registries where companyId = &#63; and groupId = &#63; and blockCode = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByBlockCode(long,long,String, int, int, OrderByComparator)}
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching registries
	 */
	@Deprecated
	@Override
	public List<Registry> findByBlockCode(
		long companyId, long groupId, String blockCode, int start, int end,
		OrderByComparator<Registry> orderByComparator, boolean useFinderCache) {

		return findByBlockCode(
			companyId, groupId, blockCode, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the registries where companyId = &#63; and groupId = &#63; and blockCode = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching registries
	 */
	@Override
	public List<Registry> findByBlockCode(
		long companyId, long groupId, String blockCode, int start, int end,
		OrderByComparator<Registry> orderByComparator) {

		blockCode = Objects.toString(blockCode, "");

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindByBlockCode;
			finderArgs = new Object[] {companyId, groupId, blockCode};
		}
		else {
			finderPath = _finderPathWithPaginationFindByBlockCode;
			finderArgs = new Object[] {
				companyId, groupId, blockCode, start, end, orderByComparator
			};
		}

		List<Registry> list = (List<Registry>)finderCache.getResult(
			finderPath, finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Registry registry : list) {
				if ((companyId != registry.getCompanyId()) ||
					(groupId != registry.getGroupId()) ||
					!blockCode.equals(registry.getBlockCode())) {

					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					5 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(5);
			}

			query.append(_SQL_SELECT_REGISTRY_WHERE);

			query.append(_FINDER_COLUMN_BLOCKCODE_COMPANYID_2);

			query.append(_FINDER_COLUMN_BLOCKCODE_GROUPID_2);

			boolean bindBlockCode = false;

			if (blockCode.isEmpty()) {
				query.append(_FINDER_COLUMN_BLOCKCODE_BLOCKCODE_3);
			}
			else {
				bindBlockCode = true;

				query.append(_FINDER_COLUMN_BLOCKCODE_BLOCKCODE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else if (pagination) {
				query.append(RegistryModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(groupId);

				if (bindBlockCode) {
					qPos.add(blockCode);
				}

				if (!pagination) {
					list = (List<Registry>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Registry>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first registry in the ordered set where companyId = &#63; and groupId = &#63; and blockCode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	@Override
	public Registry findByBlockCode_First(
			long companyId, long groupId, String blockCode,
			OrderByComparator<Registry> orderByComparator)
		throws NoSuchRegistryException {

		Registry registry = fetchByBlockCode_First(
			companyId, groupId, blockCode, orderByComparator);

		if (registry != null) {
			return registry;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", groupId=");
		msg.append(groupId);

		msg.append(", blockCode=");
		msg.append(blockCode);

		msg.append("}");

		throw new NoSuchRegistryException(msg.toString());
	}

	/**
	 * Returns the first registry in the ordered set where companyId = &#63; and groupId = &#63; and blockCode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching registry, or <code>null</code> if a matching registry could not be found
	 */
	@Override
	public Registry fetchByBlockCode_First(
		long companyId, long groupId, String blockCode,
		OrderByComparator<Registry> orderByComparator) {

		List<Registry> list = findByBlockCode(
			companyId, groupId, blockCode, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last registry in the ordered set where companyId = &#63; and groupId = &#63; and blockCode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	@Override
	public Registry findByBlockCode_Last(
			long companyId, long groupId, String blockCode,
			OrderByComparator<Registry> orderByComparator)
		throws NoSuchRegistryException {

		Registry registry = fetchByBlockCode_Last(
			companyId, groupId, blockCode, orderByComparator);

		if (registry != null) {
			return registry;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", groupId=");
		msg.append(groupId);

		msg.append(", blockCode=");
		msg.append(blockCode);

		msg.append("}");

		throw new NoSuchRegistryException(msg.toString());
	}

	/**
	 * Returns the last registry in the ordered set where companyId = &#63; and groupId = &#63; and blockCode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching registry, or <code>null</code> if a matching registry could not be found
	 */
	@Override
	public Registry fetchByBlockCode_Last(
		long companyId, long groupId, String blockCode,
		OrderByComparator<Registry> orderByComparator) {

		int count = countByBlockCode(companyId, groupId, blockCode);

		if (count == 0) {
			return null;
		}

		List<Registry> list = findByBlockCode(
			companyId, groupId, blockCode, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the registries before and after the current registry in the ordered set where companyId = &#63; and groupId = &#63; and blockCode = &#63;.
	 *
	 * @param registryId the primary key of the current registry
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next registry
	 * @throws NoSuchRegistryException if a registry with the primary key could not be found
	 */
	@Override
	public Registry[] findByBlockCode_PrevAndNext(
			long registryId, long companyId, long groupId, String blockCode,
			OrderByComparator<Registry> orderByComparator)
		throws NoSuchRegistryException {

		blockCode = Objects.toString(blockCode, "");

		Registry registry = findByPrimaryKey(registryId);

		Session session = null;

		try {
			session = openSession();

			Registry[] array = new RegistryImpl[3];

			array[0] = getByBlockCode_PrevAndNext(
				session, registry, companyId, groupId, blockCode,
				orderByComparator, true);

			array[1] = registry;

			array[2] = getByBlockCode_PrevAndNext(
				session, registry, companyId, groupId, blockCode,
				orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Registry getByBlockCode_PrevAndNext(
		Session session, Registry registry, long companyId, long groupId,
		String blockCode, OrderByComparator<Registry> orderByComparator,
		boolean previous) {

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(
				6 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(5);
		}

		query.append(_SQL_SELECT_REGISTRY_WHERE);

		query.append(_FINDER_COLUMN_BLOCKCODE_COMPANYID_2);

		query.append(_FINDER_COLUMN_BLOCKCODE_GROUPID_2);

		boolean bindBlockCode = false;

		if (blockCode.isEmpty()) {
			query.append(_FINDER_COLUMN_BLOCKCODE_BLOCKCODE_3);
		}
		else {
			bindBlockCode = true;

			query.append(_FINDER_COLUMN_BLOCKCODE_BLOCKCODE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(RegistryModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		qPos.add(groupId);

		if (bindBlockCode) {
			qPos.add(blockCode);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(registry)) {

				qPos.add(orderByConditionValue);
			}
		}

		List<Registry> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the registries where companyId = &#63; and groupId = &#63; and blockCode = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 */
	@Override
	public void removeByBlockCode(
		long companyId, long groupId, String blockCode) {

		for (Registry registry :
				findByBlockCode(
					companyId, groupId, blockCode, QueryUtil.ALL_POS,
					QueryUtil.ALL_POS, null)) {

			remove(registry);
		}
	}

	/**
	 * Returns the number of registries where companyId = &#63; and groupId = &#63; and blockCode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param blockCode the block code
	 * @return the number of matching registries
	 */
	@Override
	public int countByBlockCode(
		long companyId, long groupId, String blockCode) {

		blockCode = Objects.toString(blockCode, "");

		FinderPath finderPath = _finderPathCountByBlockCode;

		Object[] finderArgs = new Object[] {companyId, groupId, blockCode};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_REGISTRY_WHERE);

			query.append(_FINDER_COLUMN_BLOCKCODE_COMPANYID_2);

			query.append(_FINDER_COLUMN_BLOCKCODE_GROUPID_2);

			boolean bindBlockCode = false;

			if (blockCode.isEmpty()) {
				query.append(_FINDER_COLUMN_BLOCKCODE_BLOCKCODE_3);
			}
			else {
				bindBlockCode = true;

				query.append(_FINDER_COLUMN_BLOCKCODE_BLOCKCODE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(groupId);

				if (bindBlockCode) {
					qPos.add(blockCode);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_BLOCKCODE_COMPANYID_2 =
		"registry.companyId = ? AND ";

	private static final String _FINDER_COLUMN_BLOCKCODE_GROUPID_2 =
		"registry.groupId = ? AND ";

	private static final String _FINDER_COLUMN_BLOCKCODE_BLOCKCODE_2 =
		"registry.blockCode = ?";

	private static final String _FINDER_COLUMN_BLOCKCODE_BLOCKCODE_3 =
		"(registry.blockCode IS NULL OR registry.blockCode = '')";

	private FinderPath _finderPathWithPaginationFindByName;
	private FinderPath _finderPathWithoutPaginationFindByName;
	private FinderPath _finderPathCountByName;

	/**
	 * Returns all the registries where companyId = &#63; and groupId = &#63; and fullName = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 * @return the matching registries
	 */
	@Override
	public List<Registry> findByName(
		long companyId, long groupId, String fullName) {

		return findByName(
			companyId, groupId, fullName, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the registries where companyId = &#63; and groupId = &#63; and fullName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @return the range of matching registries
	 */
	@Override
	public List<Registry> findByName(
		long companyId, long groupId, String fullName, int start, int end) {

		return findByName(companyId, groupId, fullName, start, end, null);
	}

	/**
	 * Returns an ordered range of all the registries where companyId = &#63; and groupId = &#63; and fullName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByName(long,long,String, int, int, OrderByComparator)}
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching registries
	 */
	@Deprecated
	@Override
	public List<Registry> findByName(
		long companyId, long groupId, String fullName, int start, int end,
		OrderByComparator<Registry> orderByComparator, boolean useFinderCache) {

		return findByName(
			companyId, groupId, fullName, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the registries where companyId = &#63; and groupId = &#63; and fullName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching registries
	 */
	@Override
	public List<Registry> findByName(
		long companyId, long groupId, String fullName, int start, int end,
		OrderByComparator<Registry> orderByComparator) {

		fullName = Objects.toString(fullName, "");

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindByName;
			finderArgs = new Object[] {companyId, groupId, fullName};
		}
		else {
			finderPath = _finderPathWithPaginationFindByName;
			finderArgs = new Object[] {
				companyId, groupId, fullName, start, end, orderByComparator
			};
		}

		List<Registry> list = (List<Registry>)finderCache.getResult(
			finderPath, finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Registry registry : list) {
				if ((companyId != registry.getCompanyId()) ||
					(groupId != registry.getGroupId()) ||
					!fullName.equals(registry.getFullName())) {

					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					5 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(5);
			}

			query.append(_SQL_SELECT_REGISTRY_WHERE);

			query.append(_FINDER_COLUMN_NAME_COMPANYID_2);

			query.append(_FINDER_COLUMN_NAME_GROUPID_2);

			boolean bindFullName = false;

			if (fullName.isEmpty()) {
				query.append(_FINDER_COLUMN_NAME_FULLNAME_3);
			}
			else {
				bindFullName = true;

				query.append(_FINDER_COLUMN_NAME_FULLNAME_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else if (pagination) {
				query.append(RegistryModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(groupId);

				if (bindFullName) {
					qPos.add(fullName);
				}

				if (!pagination) {
					list = (List<Registry>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Registry>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first registry in the ordered set where companyId = &#63; and groupId = &#63; and fullName = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	@Override
	public Registry findByName_First(
			long companyId, long groupId, String fullName,
			OrderByComparator<Registry> orderByComparator)
		throws NoSuchRegistryException {

		Registry registry = fetchByName_First(
			companyId, groupId, fullName, orderByComparator);

		if (registry != null) {
			return registry;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", groupId=");
		msg.append(groupId);

		msg.append(", fullName=");
		msg.append(fullName);

		msg.append("}");

		throw new NoSuchRegistryException(msg.toString());
	}

	/**
	 * Returns the first registry in the ordered set where companyId = &#63; and groupId = &#63; and fullName = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching registry, or <code>null</code> if a matching registry could not be found
	 */
	@Override
	public Registry fetchByName_First(
		long companyId, long groupId, String fullName,
		OrderByComparator<Registry> orderByComparator) {

		List<Registry> list = findByName(
			companyId, groupId, fullName, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last registry in the ordered set where companyId = &#63; and groupId = &#63; and fullName = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching registry
	 * @throws NoSuchRegistryException if a matching registry could not be found
	 */
	@Override
	public Registry findByName_Last(
			long companyId, long groupId, String fullName,
			OrderByComparator<Registry> orderByComparator)
		throws NoSuchRegistryException {

		Registry registry = fetchByName_Last(
			companyId, groupId, fullName, orderByComparator);

		if (registry != null) {
			return registry;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", groupId=");
		msg.append(groupId);

		msg.append(", fullName=");
		msg.append(fullName);

		msg.append("}");

		throw new NoSuchRegistryException(msg.toString());
	}

	/**
	 * Returns the last registry in the ordered set where companyId = &#63; and groupId = &#63; and fullName = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching registry, or <code>null</code> if a matching registry could not be found
	 */
	@Override
	public Registry fetchByName_Last(
		long companyId, long groupId, String fullName,
		OrderByComparator<Registry> orderByComparator) {

		int count = countByName(companyId, groupId, fullName);

		if (count == 0) {
			return null;
		}

		List<Registry> list = findByName(
			companyId, groupId, fullName, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the registries before and after the current registry in the ordered set where companyId = &#63; and groupId = &#63; and fullName = &#63;.
	 *
	 * @param registryId the primary key of the current registry
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next registry
	 * @throws NoSuchRegistryException if a registry with the primary key could not be found
	 */
	@Override
	public Registry[] findByName_PrevAndNext(
			long registryId, long companyId, long groupId, String fullName,
			OrderByComparator<Registry> orderByComparator)
		throws NoSuchRegistryException {

		fullName = Objects.toString(fullName, "");

		Registry registry = findByPrimaryKey(registryId);

		Session session = null;

		try {
			session = openSession();

			Registry[] array = new RegistryImpl[3];

			array[0] = getByName_PrevAndNext(
				session, registry, companyId, groupId, fullName,
				orderByComparator, true);

			array[1] = registry;

			array[2] = getByName_PrevAndNext(
				session, registry, companyId, groupId, fullName,
				orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Registry getByName_PrevAndNext(
		Session session, Registry registry, long companyId, long groupId,
		String fullName, OrderByComparator<Registry> orderByComparator,
		boolean previous) {

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(
				6 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(5);
		}

		query.append(_SQL_SELECT_REGISTRY_WHERE);

		query.append(_FINDER_COLUMN_NAME_COMPANYID_2);

		query.append(_FINDER_COLUMN_NAME_GROUPID_2);

		boolean bindFullName = false;

		if (fullName.isEmpty()) {
			query.append(_FINDER_COLUMN_NAME_FULLNAME_3);
		}
		else {
			bindFullName = true;

			query.append(_FINDER_COLUMN_NAME_FULLNAME_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(RegistryModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		qPos.add(groupId);

		if (bindFullName) {
			qPos.add(fullName);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(registry)) {

				qPos.add(orderByConditionValue);
			}
		}

		List<Registry> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the registries where companyId = &#63; and groupId = &#63; and fullName = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 */
	@Override
	public void removeByName(long companyId, long groupId, String fullName) {
		for (Registry registry :
				findByName(
					companyId, groupId, fullName, QueryUtil.ALL_POS,
					QueryUtil.ALL_POS, null)) {

			remove(registry);
		}
	}

	/**
	 * Returns the number of registries where companyId = &#63; and groupId = &#63; and fullName = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param fullName the full name
	 * @return the number of matching registries
	 */
	@Override
	public int countByName(long companyId, long groupId, String fullName) {
		fullName = Objects.toString(fullName, "");

		FinderPath finderPath = _finderPathCountByName;

		Object[] finderArgs = new Object[] {companyId, groupId, fullName};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_REGISTRY_WHERE);

			query.append(_FINDER_COLUMN_NAME_COMPANYID_2);

			query.append(_FINDER_COLUMN_NAME_GROUPID_2);

			boolean bindFullName = false;

			if (fullName.isEmpty()) {
				query.append(_FINDER_COLUMN_NAME_FULLNAME_3);
			}
			else {
				bindFullName = true;

				query.append(_FINDER_COLUMN_NAME_FULLNAME_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(groupId);

				if (bindFullName) {
					qPos.add(fullName);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_NAME_COMPANYID_2 =
		"registry.companyId = ? AND ";

	private static final String _FINDER_COLUMN_NAME_GROUPID_2 =
		"registry.groupId = ? AND ";

	private static final String _FINDER_COLUMN_NAME_FULLNAME_2 =
		"registry.fullName = ?";

	private static final String _FINDER_COLUMN_NAME_FULLNAME_3 =
		"(registry.fullName IS NULL OR registry.fullName = '')";

	public RegistryPersistenceImpl() {
		setModelClass(Registry.class);

		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");

		try {
			Field field = BasePersistenceImpl.class.getDeclaredField(
				"_dbColumnNames");

			field.setAccessible(true);

			field.set(this, dbColumnNames);
		}
		catch (Exception e) {
			if (_log.isDebugEnabled()) {
				_log.debug(e, e);
			}
		}
	}

	/**
	 * Caches the registry in the entity cache if it is enabled.
	 *
	 * @param registry the registry
	 */
	@Override
	public void cacheResult(Registry registry) {
		entityCache.putResult(
			RegistryModelImpl.ENTITY_CACHE_ENABLED, RegistryImpl.class,
			registry.getPrimaryKey(), registry);

		finderCache.putResult(
			_finderPathFetchByUUID_G,
			new Object[] {registry.getUuid(), registry.getGroupId()}, registry);

		finderCache.putResult(
			_finderPathFetchByNif,
			new Object[] {
				registry.getCompanyId(), registry.getGroupId(),
				registry.getNif()
			},
			registry);

		registry.resetOriginalValues();
	}

	/**
	 * Caches the registries in the entity cache if it is enabled.
	 *
	 * @param registries the registries
	 */
	@Override
	public void cacheResult(List<Registry> registries) {
		for (Registry registry : registries) {
			if (entityCache.getResult(
					RegistryModelImpl.ENTITY_CACHE_ENABLED, RegistryImpl.class,
					registry.getPrimaryKey()) == null) {

				cacheResult(registry);
			}
			else {
				registry.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all registries.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(RegistryImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the registry.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Registry registry) {
		entityCache.removeResult(
			RegistryModelImpl.ENTITY_CACHE_ENABLED, RegistryImpl.class,
			registry.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((RegistryModelImpl)registry, true);
	}

	@Override
	public void clearCache(List<Registry> registries) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Registry registry : registries) {
			entityCache.removeResult(
				RegistryModelImpl.ENTITY_CACHE_ENABLED, RegistryImpl.class,
				registry.getPrimaryKey());

			clearUniqueFindersCache((RegistryModelImpl)registry, true);
		}
	}

	protected void cacheUniqueFindersCache(
		RegistryModelImpl registryModelImpl) {

		Object[] args = new Object[] {
			registryModelImpl.getUuid(), registryModelImpl.getGroupId()
		};

		finderCache.putResult(
			_finderPathCountByUUID_G, args, Long.valueOf(1), false);
		finderCache.putResult(
			_finderPathFetchByUUID_G, args, registryModelImpl, false);

		args = new Object[] {
			registryModelImpl.getCompanyId(), registryModelImpl.getGroupId(),
			registryModelImpl.getNif()
		};

		finderCache.putResult(
			_finderPathCountByNif, args, Long.valueOf(1), false);
		finderCache.putResult(
			_finderPathFetchByNif, args, registryModelImpl, false);
	}

	protected void clearUniqueFindersCache(
		RegistryModelImpl registryModelImpl, boolean clearCurrent) {

		if (clearCurrent) {
			Object[] args = new Object[] {
				registryModelImpl.getUuid(), registryModelImpl.getGroupId()
			};

			finderCache.removeResult(_finderPathCountByUUID_G, args);
			finderCache.removeResult(_finderPathFetchByUUID_G, args);
		}

		if ((registryModelImpl.getColumnBitmask() &
			 _finderPathFetchByUUID_G.getColumnBitmask()) != 0) {

			Object[] args = new Object[] {
				registryModelImpl.getOriginalUuid(),
				registryModelImpl.getOriginalGroupId()
			};

			finderCache.removeResult(_finderPathCountByUUID_G, args);
			finderCache.removeResult(_finderPathFetchByUUID_G, args);
		}

		if (clearCurrent) {
			Object[] args = new Object[] {
				registryModelImpl.getCompanyId(),
				registryModelImpl.getGroupId(), registryModelImpl.getNif()
			};

			finderCache.removeResult(_finderPathCountByNif, args);
			finderCache.removeResult(_finderPathFetchByNif, args);
		}

		if ((registryModelImpl.getColumnBitmask() &
			 _finderPathFetchByNif.getColumnBitmask()) != 0) {

			Object[] args = new Object[] {
				registryModelImpl.getOriginalCompanyId(),
				registryModelImpl.getOriginalGroupId(),
				registryModelImpl.getOriginalNif()
			};

			finderCache.removeResult(_finderPathCountByNif, args);
			finderCache.removeResult(_finderPathFetchByNif, args);
		}
	}

	/**
	 * Creates a new registry with the primary key. Does not add the registry to the database.
	 *
	 * @param registryId the primary key for the new registry
	 * @return the new registry
	 */
	@Override
	public Registry create(long registryId) {
		Registry registry = new RegistryImpl();

		registry.setNew(true);
		registry.setPrimaryKey(registryId);

		String uuid = PortalUUIDUtil.generate();

		registry.setUuid(uuid);

		registry.setCompanyId(CompanyThreadLocal.getCompanyId());

		return registry;
	}

	/**
	 * Removes the registry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param registryId the primary key of the registry
	 * @return the registry that was removed
	 * @throws NoSuchRegistryException if a registry with the primary key could not be found
	 */
	@Override
	public Registry remove(long registryId) throws NoSuchRegistryException {
		return remove((Serializable)registryId);
	}

	/**
	 * Removes the registry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the registry
	 * @return the registry that was removed
	 * @throws NoSuchRegistryException if a registry with the primary key could not be found
	 */
	@Override
	public Registry remove(Serializable primaryKey)
		throws NoSuchRegistryException {

		Session session = null;

		try {
			session = openSession();

			Registry registry = (Registry)session.get(
				RegistryImpl.class, primaryKey);

			if (registry == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchRegistryException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(registry);
		}
		catch (NoSuchRegistryException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Registry removeImpl(Registry registry) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(registry)) {
				registry = (Registry)session.get(
					RegistryImpl.class, registry.getPrimaryKeyObj());
			}

			if (registry != null) {
				session.delete(registry);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (registry != null) {
			clearCache(registry);
		}

		return registry;
	}

	@Override
	public Registry updateImpl(Registry registry) {
		boolean isNew = registry.isNew();

		if (!(registry instanceof RegistryModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(registry.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(registry);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in registry proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom Registry implementation " +
					registry.getClass());
		}

		RegistryModelImpl registryModelImpl = (RegistryModelImpl)registry;

		if (Validator.isNull(registry.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			registry.setUuid(uuid);
		}

		ServiceContext serviceContext =
			ServiceContextThreadLocal.getServiceContext();

		Date now = new Date();

		if (isNew && (registry.getCreateDate() == null)) {
			if (serviceContext == null) {
				registry.setCreateDate(now);
			}
			else {
				registry.setCreateDate(serviceContext.getCreateDate(now));
			}
		}

		if (!registryModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				registry.setModifiedDate(now);
			}
			else {
				registry.setModifiedDate(serviceContext.getModifiedDate(now));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (registry.isNew()) {
				session.save(registry);

				registry.setNew(false);
			}
			else {
				registry = (Registry)session.merge(registry);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!RegistryModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else if (isNew) {
			Object[] args = new Object[] {registryModelImpl.getUuid()};

			finderCache.removeResult(_finderPathCountByUuid, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByUuid, args);

			args = new Object[] {
				registryModelImpl.getUuid(), registryModelImpl.getCompanyId()
			};

			finderCache.removeResult(_finderPathCountByUuid_C, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByUuid_C, args);

			args = new Object[] {
				registryModelImpl.getCompanyId(), registryModelImpl.getGroupId()
			};

			finderCache.removeResult(_finderPathCountByWithoutName, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByWithoutName, args);

			args = new Object[] {
				registryModelImpl.getCompanyId(),
				registryModelImpl.getGroupId(), registryModelImpl.getBlockCode()
			};

			finderCache.removeResult(_finderPathCountByBlockCode, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByBlockCode, args);

			args = new Object[] {
				registryModelImpl.getCompanyId(),
				registryModelImpl.getGroupId(), registryModelImpl.getFullName()
			};

			finderCache.removeResult(_finderPathCountByName, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByName, args);

			finderCache.removeResult(_finderPathCountAll, FINDER_ARGS_EMPTY);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindAll, FINDER_ARGS_EMPTY);
		}
		else {
			if ((registryModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByUuid.getColumnBitmask()) !=
					 0) {

				Object[] args = new Object[] {
					registryModelImpl.getOriginalUuid()
				};

				finderCache.removeResult(_finderPathCountByUuid, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUuid, args);

				args = new Object[] {registryModelImpl.getUuid()};

				finderCache.removeResult(_finderPathCountByUuid, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUuid, args);
			}

			if ((registryModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByUuid_C.getColumnBitmask()) !=
					 0) {

				Object[] args = new Object[] {
					registryModelImpl.getOriginalUuid(),
					registryModelImpl.getOriginalCompanyId()
				};

				finderCache.removeResult(_finderPathCountByUuid_C, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUuid_C, args);

				args = new Object[] {
					registryModelImpl.getUuid(),
					registryModelImpl.getCompanyId()
				};

				finderCache.removeResult(_finderPathCountByUuid_C, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUuid_C, args);
			}

			if ((registryModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByWithoutName.
					 getColumnBitmask()) != 0) {

				Object[] args = new Object[] {
					registryModelImpl.getOriginalCompanyId(),
					registryModelImpl.getOriginalGroupId()
				};

				finderCache.removeResult(_finderPathCountByWithoutName, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByWithoutName, args);

				args = new Object[] {
					registryModelImpl.getCompanyId(),
					registryModelImpl.getGroupId()
				};

				finderCache.removeResult(_finderPathCountByWithoutName, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByWithoutName, args);
			}

			if ((registryModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByBlockCode.
					 getColumnBitmask()) != 0) {

				Object[] args = new Object[] {
					registryModelImpl.getOriginalCompanyId(),
					registryModelImpl.getOriginalGroupId(),
					registryModelImpl.getOriginalBlockCode()
				};

				finderCache.removeResult(_finderPathCountByBlockCode, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByBlockCode, args);

				args = new Object[] {
					registryModelImpl.getCompanyId(),
					registryModelImpl.getGroupId(),
					registryModelImpl.getBlockCode()
				};

				finderCache.removeResult(_finderPathCountByBlockCode, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByBlockCode, args);
			}

			if ((registryModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByName.getColumnBitmask()) !=
					 0) {

				Object[] args = new Object[] {
					registryModelImpl.getOriginalCompanyId(),
					registryModelImpl.getOriginalGroupId(),
					registryModelImpl.getOriginalFullName()
				};

				finderCache.removeResult(_finderPathCountByName, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByName, args);

				args = new Object[] {
					registryModelImpl.getCompanyId(),
					registryModelImpl.getGroupId(),
					registryModelImpl.getFullName()
				};

				finderCache.removeResult(_finderPathCountByName, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByName, args);
			}
		}

		entityCache.putResult(
			RegistryModelImpl.ENTITY_CACHE_ENABLED, RegistryImpl.class,
			registry.getPrimaryKey(), registry, false);

		clearUniqueFindersCache(registryModelImpl, false);
		cacheUniqueFindersCache(registryModelImpl);

		registry.resetOriginalValues();

		return registry;
	}

	/**
	 * Returns the registry with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the registry
	 * @return the registry
	 * @throws NoSuchRegistryException if a registry with the primary key could not be found
	 */
	@Override
	public Registry findByPrimaryKey(Serializable primaryKey)
		throws NoSuchRegistryException {

		Registry registry = fetchByPrimaryKey(primaryKey);

		if (registry == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchRegistryException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return registry;
	}

	/**
	 * Returns the registry with the primary key or throws a <code>NoSuchRegistryException</code> if it could not be found.
	 *
	 * @param registryId the primary key of the registry
	 * @return the registry
	 * @throws NoSuchRegistryException if a registry with the primary key could not be found
	 */
	@Override
	public Registry findByPrimaryKey(long registryId)
		throws NoSuchRegistryException {

		return findByPrimaryKey((Serializable)registryId);
	}

	/**
	 * Returns the registry with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the registry
	 * @return the registry, or <code>null</code> if a registry with the primary key could not be found
	 */
	@Override
	public Registry fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(
			RegistryModelImpl.ENTITY_CACHE_ENABLED, RegistryImpl.class,
			primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Registry registry = (Registry)serializable;

		if (registry == null) {
			Session session = null;

			try {
				session = openSession();

				registry = (Registry)session.get(
					RegistryImpl.class, primaryKey);

				if (registry != null) {
					cacheResult(registry);
				}
				else {
					entityCache.putResult(
						RegistryModelImpl.ENTITY_CACHE_ENABLED,
						RegistryImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(
					RegistryModelImpl.ENTITY_CACHE_ENABLED, RegistryImpl.class,
					primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return registry;
	}

	/**
	 * Returns the registry with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param registryId the primary key of the registry
	 * @return the registry, or <code>null</code> if a registry with the primary key could not be found
	 */
	@Override
	public Registry fetchByPrimaryKey(long registryId) {
		return fetchByPrimaryKey((Serializable)registryId);
	}

	@Override
	public Map<Serializable, Registry> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Registry> map = new HashMap<Serializable, Registry>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Registry registry = fetchByPrimaryKey(primaryKey);

			if (registry != null) {
				map.put(primaryKey, registry);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(
				RegistryModelImpl.ENTITY_CACHE_ENABLED, RegistryImpl.class,
				primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Registry)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler(
			uncachedPrimaryKeys.size() * 2 + 1);

		query.append(_SQL_SELECT_REGISTRY_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(",");
		}

		query.setIndex(query.index() - 1);

		query.append(")");

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Registry registry : (List<Registry>)q.list()) {
				map.put(registry.getPrimaryKeyObj(), registry);

				cacheResult(registry);

				uncachedPrimaryKeys.remove(registry.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(
					RegistryModelImpl.ENTITY_CACHE_ENABLED, RegistryImpl.class,
					primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the registries.
	 *
	 * @return the registries
	 */
	@Override
	public List<Registry> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the registries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @return the range of registries
	 */
	@Override
	public List<Registry> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the registries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findAll(int, int, OrderByComparator)}
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of registries
	 */
	@Deprecated
	@Override
	public List<Registry> findAll(
		int start, int end, OrderByComparator<Registry> orderByComparator,
		boolean useFinderCache) {

		return findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the registries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>RegistryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of registries
	 * @param end the upper bound of the range of registries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of registries
	 */
	@Override
	public List<Registry> findAll(
		int start, int end, OrderByComparator<Registry> orderByComparator) {

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindAll;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<Registry> list = (List<Registry>)finderCache.getResult(
			finderPath, finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_REGISTRY);

				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_REGISTRY;

				if (pagination) {
					sql = sql.concat(RegistryModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Registry>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Registry>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the registries from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Registry registry : findAll()) {
			remove(registry);
		}
	}

	/**
	 * Returns the number of registries.
	 *
	 * @return the number of registries
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_REGISTRY);

				count = (Long)q.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				finderCache.removeResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return RegistryModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the registry persistence.
	 */
	public void afterPropertiesSet() {
		_finderPathWithPaginationFindAll = new FinderPath(
			RegistryModelImpl.ENTITY_CACHE_ENABLED,
			RegistryModelImpl.FINDER_CACHE_ENABLED, RegistryImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			RegistryModelImpl.ENTITY_CACHE_ENABLED,
			RegistryModelImpl.FINDER_CACHE_ENABLED, RegistryImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll",
			new String[0]);

		_finderPathCountAll = new FinderPath(
			RegistryModelImpl.ENTITY_CACHE_ENABLED,
			RegistryModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0]);

		_finderPathWithPaginationFindByUuid = new FinderPath(
			RegistryModelImpl.ENTITY_CACHE_ENABLED,
			RegistryModelImpl.FINDER_CACHE_ENABLED, RegistryImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByUuid = new FinderPath(
			RegistryModelImpl.ENTITY_CACHE_ENABLED,
			RegistryModelImpl.FINDER_CACHE_ENABLED, RegistryImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()},
			RegistryModelImpl.UUID_COLUMN_BITMASK |
			RegistryModelImpl.MODIFIEDDATE_COLUMN_BITMASK);

		_finderPathCountByUuid = new FinderPath(
			RegistryModelImpl.ENTITY_CACHE_ENABLED,
			RegistryModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()});

		_finderPathFetchByUUID_G = new FinderPath(
			RegistryModelImpl.ENTITY_CACHE_ENABLED,
			RegistryModelImpl.FINDER_CACHE_ENABLED, RegistryImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			RegistryModelImpl.UUID_COLUMN_BITMASK |
			RegistryModelImpl.GROUPID_COLUMN_BITMASK);

		_finderPathCountByUUID_G = new FinderPath(
			RegistryModelImpl.ENTITY_CACHE_ENABLED,
			RegistryModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()});

		_finderPathWithPaginationFindByUuid_C = new FinderPath(
			RegistryModelImpl.ENTITY_CACHE_ENABLED,
			RegistryModelImpl.FINDER_CACHE_ENABLED, RegistryImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByUuid_C = new FinderPath(
			RegistryModelImpl.ENTITY_CACHE_ENABLED,
			RegistryModelImpl.FINDER_CACHE_ENABLED, RegistryImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			RegistryModelImpl.UUID_COLUMN_BITMASK |
			RegistryModelImpl.COMPANYID_COLUMN_BITMASK |
			RegistryModelImpl.MODIFIEDDATE_COLUMN_BITMASK);

		_finderPathCountByUuid_C = new FinderPath(
			RegistryModelImpl.ENTITY_CACHE_ENABLED,
			RegistryModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()});

		_finderPathFetchByNif = new FinderPath(
			RegistryModelImpl.ENTITY_CACHE_ENABLED,
			RegistryModelImpl.FINDER_CACHE_ENABLED, RegistryImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByNif",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				String.class.getName()
			},
			RegistryModelImpl.COMPANYID_COLUMN_BITMASK |
			RegistryModelImpl.GROUPID_COLUMN_BITMASK |
			RegistryModelImpl.NIF_COLUMN_BITMASK);

		_finderPathCountByNif = new FinderPath(
			RegistryModelImpl.ENTITY_CACHE_ENABLED,
			RegistryModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByNif",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				String.class.getName()
			});

		_finderPathWithPaginationFindByWithoutName = new FinderPath(
			RegistryModelImpl.ENTITY_CACHE_ENABLED,
			RegistryModelImpl.FINDER_CACHE_ENABLED, RegistryImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByWithoutName",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByWithoutName = new FinderPath(
			RegistryModelImpl.ENTITY_CACHE_ENABLED,
			RegistryModelImpl.FINDER_CACHE_ENABLED, RegistryImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByWithoutName",
			new String[] {Long.class.getName(), Long.class.getName()},
			RegistryModelImpl.COMPANYID_COLUMN_BITMASK |
			RegistryModelImpl.GROUPID_COLUMN_BITMASK |
			RegistryModelImpl.MODIFIEDDATE_COLUMN_BITMASK);

		_finderPathCountByWithoutName = new FinderPath(
			RegistryModelImpl.ENTITY_CACHE_ENABLED,
			RegistryModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByWithoutName",
			new String[] {Long.class.getName(), Long.class.getName()});

		_finderPathWithPaginationFindByBlockCode = new FinderPath(
			RegistryModelImpl.ENTITY_CACHE_ENABLED,
			RegistryModelImpl.FINDER_CACHE_ENABLED, RegistryImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByBlockCode",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByBlockCode = new FinderPath(
			RegistryModelImpl.ENTITY_CACHE_ENABLED,
			RegistryModelImpl.FINDER_CACHE_ENABLED, RegistryImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByBlockCode",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				String.class.getName()
			},
			RegistryModelImpl.COMPANYID_COLUMN_BITMASK |
			RegistryModelImpl.GROUPID_COLUMN_BITMASK |
			RegistryModelImpl.BLOCKCODE_COLUMN_BITMASK |
			RegistryModelImpl.MODIFIEDDATE_COLUMN_BITMASK);

		_finderPathCountByBlockCode = new FinderPath(
			RegistryModelImpl.ENTITY_CACHE_ENABLED,
			RegistryModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByBlockCode",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				String.class.getName()
			});

		_finderPathWithPaginationFindByName = new FinderPath(
			RegistryModelImpl.ENTITY_CACHE_ENABLED,
			RegistryModelImpl.FINDER_CACHE_ENABLED, RegistryImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByName",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByName = new FinderPath(
			RegistryModelImpl.ENTITY_CACHE_ENABLED,
			RegistryModelImpl.FINDER_CACHE_ENABLED, RegistryImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByName",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				String.class.getName()
			},
			RegistryModelImpl.COMPANYID_COLUMN_BITMASK |
			RegistryModelImpl.GROUPID_COLUMN_BITMASK |
			RegistryModelImpl.FULLNAME_COLUMN_BITMASK |
			RegistryModelImpl.MODIFIEDDATE_COLUMN_BITMASK);

		_finderPathCountByName = new FinderPath(
			RegistryModelImpl.ENTITY_CACHE_ENABLED,
			RegistryModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByName",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				String.class.getName()
			});
	}

	public void destroy() {
		entityCache.removeCache(RegistryImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;

	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_REGISTRY =
		"SELECT registry FROM Registry registry";

	private static final String _SQL_SELECT_REGISTRY_WHERE_PKS_IN =
		"SELECT registry FROM Registry registry WHERE registryId IN (";

	private static final String _SQL_SELECT_REGISTRY_WHERE =
		"SELECT registry FROM Registry registry WHERE ";

	private static final String _SQL_COUNT_REGISTRY =
		"SELECT COUNT(registry) FROM Registry registry";

	private static final String _SQL_COUNT_REGISTRY_WHERE =
		"SELECT COUNT(registry) FROM Registry registry WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "registry.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No Registry exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No Registry exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		RegistryPersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid"});

}