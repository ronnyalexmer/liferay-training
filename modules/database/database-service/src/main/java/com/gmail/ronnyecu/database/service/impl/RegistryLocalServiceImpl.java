/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.gmail.ronnyecu.database.service.impl;

import com.gmail.ronnyecu.database.model.Registry;
import com.gmail.ronnyecu.database.service.base.RegistryLocalServiceBaseImpl;

import java.util.List;

/**
 * The implementation of the registry local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>com.gmail.ronnyecu.database.service.RegistryLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see RegistryLocalServiceBaseImpl
 */
public class RegistryLocalServiceImpl extends RegistryLocalServiceBaseImpl {

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. Use <code>com.gmail.ronnyecu.database.service.RegistryLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>com.gmail.ronnyecu.database.service.RegistryLocalServiceUtil</code>.
     */

    public List<Registry> findRegistryWithoutName(long companyId, long groupId, int start, int end) {
        return getRegistryPersistence().findByWithoutName(companyId, groupId, start, end);
    }
}