/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.gmail.ronnyecu.database.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.gmail.ronnyecu.database.model.Block;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Block in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class BlockCacheModel implements CacheModel<Block>, Externalizable {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof BlockCacheModel)) {
			return false;
		}

		BlockCacheModel blockCacheModel = (BlockCacheModel)obj;

		if (blockId == blockCacheModel.blockId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, blockId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", blockId=");
		sb.append(blockId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", blockCode=");
		sb.append(blockCode);
		sb.append(", fileName=");
		sb.append(fileName);
		sb.append(", description=");
		sb.append(description);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Block toEntityModel() {
		BlockImpl blockImpl = new BlockImpl();

		if (uuid == null) {
			blockImpl.setUuid("");
		}
		else {
			blockImpl.setUuid(uuid);
		}

		blockImpl.setBlockId(blockId);
		blockImpl.setCompanyId(companyId);
		blockImpl.setGroupId(groupId);

		if (createDate == Long.MIN_VALUE) {
			blockImpl.setCreateDate(null);
		}
		else {
			blockImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			blockImpl.setModifiedDate(null);
		}
		else {
			blockImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (blockCode == null) {
			blockImpl.setBlockCode("");
		}
		else {
			blockImpl.setBlockCode(blockCode);
		}

		if (fileName == null) {
			blockImpl.setFileName("");
		}
		else {
			blockImpl.setFileName(fileName);
		}

		if (description == null) {
			blockImpl.setDescription("");
		}
		else {
			blockImpl.setDescription(description);
		}

		blockImpl.resetOriginalValues();

		return blockImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		blockId = objectInput.readLong();

		companyId = objectInput.readLong();

		groupId = objectInput.readLong();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		blockCode = objectInput.readUTF();
		fileName = objectInput.readUTF();
		description = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(blockId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(groupId);
		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (blockCode == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(blockCode);
		}

		if (fileName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(fileName);
		}

		if (description == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(description);
		}
	}

	public String uuid;
	public long blockId;
	public long companyId;
	public long groupId;
	public long createDate;
	public long modifiedDate;
	public String blockCode;
	public String fileName;
	public String description;

}