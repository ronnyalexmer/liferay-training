create index IX_56C830EE on FOO_Block (companyId, groupId, blockCode[$COLUMN_LENGTH:75$]);
create index IX_7C901F37 on FOO_Block (companyId, groupId, fileName[$COLUMN_LENGTH:75$]);
create index IX_8C0EF827 on FOO_Block (companyId, groupId, modifiedDate);
create index IX_BEC5F5DB on FOO_Block (groupId, modifiedDate);
create index IX_E7AE81CE on FOO_Block (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_36E923D0 on FOO_Block (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_A918E4EC on FOO_Registry (companyId, groupId, blockCode[$COLUMN_LENGTH:75$]);
create index IX_ECFAC64C on FOO_Registry (companyId, groupId, fullName[$COLUMN_LENGTH:75$]);
create index IX_9074169D on FOO_Registry (companyId, groupId, nif[$COLUMN_LENGTH:75$]);
create index IX_B1932910 on FOO_Registry (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_5CF2B392 on FOO_Registry (uuid_[$COLUMN_LENGTH:75$], groupId);