create table FOO_Block (
	uuid_ VARCHAR(75) null,
	blockId LONG not null primary key,
	companyId LONG,
	groupId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	blockCode VARCHAR(75) null,
	fileName VARCHAR(75) null,
	description VARCHAR(75) null
);

create table FOO_Registry (
	uuid_ VARCHAR(75) null,
	registryId LONG not null primary key,
	companyId LONG,
	groupId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	nif VARCHAR(75) null,
	fullName VARCHAR(75) null,
	amount DOUBLE,
	token VARCHAR(75) null,
	blockCode VARCHAR(75) null
);