package com.gmail.rmedina.panel.block.portlet.actions;

import com.gmail.rmedina.panel.block.constants.BlockPortletKeys;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import org.osgi.service.component.annotations.Component;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + BlockPortletKeys.BLOCK_PANEL,
                "mvc.command.name=/block/view"
        },
        service = MVCRenderCommand.class
)
public class ViewBlockEntitiesMVCRenderCommand implements MVCRenderCommand{
    @Override
    public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
        return "/view.jsp";
    }
}
