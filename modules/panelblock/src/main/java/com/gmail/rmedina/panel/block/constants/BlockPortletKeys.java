package com.gmail.rmedina.panel.block.constants;

/**
 * @author Administrador
 */
public class BlockPortletKeys {

    public static final String BLOCK_PANEL =
            "com_liferay_docs_PanelBlock";

    public static final String URL_ADD_BLOCK = "/block/add";
    public static final String URL_SEARCH_BLOCK = "/block/search";

    public static final String BLOCK_SEARCH_CONTAINER_ID = "blockSearchContainterId";
    public static final String BLOCK_MANAGEMENT_TOOLBAR_ID = "blockManagementToolbar";
    public static final String BLOCK_INFO_PANEL_ID = "blockInfoPanelId";
    public static final String BLOCK_SEARCH_FORM_NAME = "blockSearchFormName";
    public static final int BLOCK_DELTA = 10;
}