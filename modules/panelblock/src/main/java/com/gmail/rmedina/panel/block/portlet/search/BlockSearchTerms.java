package com.gmail.rmedina.panel.block.portlet.search;

import com.liferay.portal.kernel.dao.search.DAOParamUtil;

import javax.portlet.PortletRequest;

public class BlockSearchTerms extends BlockDisplayTerms {
    public BlockSearchTerms(PortletRequest portletRequest) {
        super(portletRequest);

        keywords = DAOParamUtil.getString(portletRequest, KEYWORDS);
        blockName = DAOParamUtil.getString(portletRequest, BLOCK_NAME);
        modifiedDate = DAOParamUtil.getString(portletRequest, MODIFIED_DATE);
    }

}