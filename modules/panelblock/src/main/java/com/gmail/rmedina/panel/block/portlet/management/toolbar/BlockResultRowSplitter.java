package com.gmail.rmedina.panel.block.portlet.management.toolbar;

import com.gmail.ronnyecu.database.model.Block;
import com.liferay.portal.kernel.dao.search.ResultRow;
import com.liferay.portal.kernel.dao.search.ResultRowSplitter;
import com.liferay.portal.kernel.dao.search.ResultRowSplitterEntry;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.security.permission.ResourceActionsUtil;

import java.util.*;

public class BlockResultRowSplitter implements ResultRowSplitter {
    private final Locale _locale;

    public BlockResultRowSplitter(Locale locale) {
        _locale = locale;
    }

    @Override
    public List<ResultRowSplitterEntry> split(List<ResultRow> resultRows) {
        Map<String, List<ResultRow>> rowMap = new HashMap<>();

        for (ResultRow resultRow : resultRows) {
            Block block = (Block) resultRow.getObject();

            List<ResultRow> list = rowMap.computeIfAbsent(block.getModelClassName(), className -> new ArrayList<>());
            list.add(resultRow);
        }

        List<ResultRowSplitterEntry> resultRowSplitterEntries = new ArrayList<>(
                rowMap.size());
        String header = LanguageUtil.get(_locale, "block.name");

        for (Map.Entry<String, List<ResultRow>> entry : rowMap.entrySet()) {
            resultRowSplitterEntries.add(
                    new ResultRowSplitterEntry(header, entry.getValue()));
        }

        return resultRowSplitterEntries;
    }
}
