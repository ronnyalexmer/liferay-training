package com.gmail.rmedina.panel.block.portlet.actions;

import com.gmail.rmedina.panel.block.constants.BlockPortletKeys;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.servlet.HttpHeaders;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.portal.kernel.util.ParamUtil;
import org.osgi.service.component.annotations.Component;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import java.io.IOException;
import java.io.OutputStream;

@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + BlockPortletKeys.BLOCK_PANEL,
                "mvc.command.name=/download/file"
        },
        service = MVCResourceCommand.class
)
public class DownloadMVCA implements MVCResourceCommand {
    @Override
    public boolean serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws PortletException {
        String field = ParamUtil.getString(resourceRequest, "patata");
        System.out.println(field);
        resourceResponse.setContentType(ContentTypes.TEXT_XML_UTF8);
        resourceResponse.addProperty(HttpHeaders.CACHE_CONTROL, "max-age=1, must-revalidate");
        resourceResponse.setProperty(HttpHeaders.CONTENT_DISPOSITION, String.format("%s; filename=%s", HttpHeaders.CONTENT_DISPOSITION_ATTACHMENT, field + ".xml"));
        try {
            OutputStream out = resourceResponse.getPortletOutputStream();
            out.write(xml_short.getBytes());
            out.flush();
            out.close();
        } catch (IOException e) {
            System.out.println(e);
        }
        return false;
    }

    public static final String xml_short = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><response><message>OK</message></response>";
}
