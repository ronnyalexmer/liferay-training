package com.gmail.rmedina.panel.block.portlet.management.toolbar;

import com.liferay.portal.kernel.dao.search.EmptyOnClickRowChecker;

import javax.portlet.PortletResponse;

public class BlockRowChecker extends EmptyOnClickRowChecker {
    public BlockRowChecker(PortletResponse portletResponse) {
        super(portletResponse);
    }
}
