package com.gmail.rmedina.panel.block.application.list;

import com.liferay.application.list.constants.PanelCategoryKeys;
import com.gmail.rmedina.panel.block.constants.BlockPortletKeys;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author Administrador
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=100",
		"panel.category.key=" + PanelCategoryKeys.SITE_ADMINISTRATION_CONTENT
	},
	service = PanelApp.class
)
public class BlockPanel extends BasePanelApp {

	@Override
	public String getPortletId() {
		return BlockPortletKeys.BLOCK_PANEL;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + BlockPortletKeys.BLOCK_PANEL + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}