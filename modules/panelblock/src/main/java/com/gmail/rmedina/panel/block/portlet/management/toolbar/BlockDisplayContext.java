package com.gmail.rmedina.panel.block.portlet.management.toolbar;

import com.gmail.rmedina.panel.block.constants.BlockPortletKeys;
import com.gmail.rmedina.panel.block.portlet.search.BlockSearch;
import com.gmail.rmedina.panel.block.portlet.search.BlockSearchTerms;
import com.gmail.ronnyecu.database.model.Block;
import com.gmail.ronnyecu.database.service.BlockLocalServiceUtil;
import com.liferay.frontend.taglib.clay.servlet.taglib.display.context.BaseManagementToolbarDisplayContext;
import com.liferay.frontend.taglib.clay.servlet.taglib.util.*;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.dao.search.DisplayTerms;
import com.liferay.portal.kernel.dao.search.EmptyOnClickRowChecker;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletRequest;
import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portal.kernel.portlet.PortalPreferences;
import com.liferay.portal.kernel.portlet.PortletPreferencesFactoryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.*;

import javax.portlet.ActionRequest;
import javax.portlet.PortletURL;
import javax.portlet.RenderURL;
import javax.portlet.ResourceURL;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class BlockDisplayContext extends BaseManagementToolbarDisplayContext {
    public BlockDisplayContext(LiferayPortletRequest liferayPortletRequest, LiferayPortletResponse liferayPortletResponse, HttpServletRequest request) {
        super(liferayPortletRequest, liferayPortletResponse, request);
    }

    @Override
    public String getSearchContainerId() {
        return BlockPortletKeys.BLOCK_SEARCH_CONTAINER_ID;
    }

    @Override
    public String getInfoPanelId() {
        return BlockPortletKeys.BLOCK_INFO_PANEL_ID;
    }

    @Override
    public String getComponentId() {
        return BlockPortletKeys.BLOCK_MANAGEMENT_TOOLBAR_ID;
    }

    @Override
    public String getSearchFormName() {
        return BlockPortletKeys.BLOCK_SEARCH_FORM_NAME;
    }


    @Override
    public CreationMenu getCreationMenu() {
        CreationMenu creationMenu = new CreationMenu();
        PortletURL addBlock = liferayPortletResponse.createActionURL();
        addBlock.setParameter("javax.portlet.action", BlockPortletKeys.URL_ADD_BLOCK);

        creationMenu.addPrimaryDropdownItem(dropdownItem -> {
            dropdownItem.setLabel(LanguageUtil.get(request, "sample.caption"));
            dropdownItem.setIcon("add");
            dropdownItem.setHref(addBlock);
        });

        return creationMenu;
    }

    @Override
    public List<DropdownItem> getActionDropdownItems() {
        DropdownItemList dropdownItems = new DropdownItemList();
        DropdownItem deleteSelected = new DropdownItem();
        deleteSelected.putData("action", "deleteSelectedEntries");
        deleteSelected.setIcon("times-circle");
        deleteSelected.setLabel(LanguageUtil.get(request, "delete"));
        deleteSelected.setQuickAction(true);

        DropdownItem tokenizeSelected = new DropdownItem();
        tokenizeSelected.putData("action", "tokenizeSelectedEntries");
        tokenizeSelected.setIcon("link");
        tokenizeSelected.setLabel(LanguageUtil.get(request, "tokenize"));
        tokenizeSelected.setQuickAction(true);

        dropdownItems.add(deleteSelected);
        dropdownItems.add(tokenizeSelected);
        return dropdownItems;
    }

    @Override
    public String getSearchActionURL() {
        PortletURL searchActionURL = getPortletURL();
        //searchActionURL.setParameter("javax.portlet.action", BlockPortletKeys.URL_SEARCH_BLOCK);
        return searchActionURL.toString();
    }

    @Override
    public String getClearResultsURL() {
        PortletURL clearResultsURL = getPortletURL();
        clearResultsURL.setParameter(DisplayTerms.KEYWORDS, StringPool.BLANK);
        return clearResultsURL.toString();
    }

    @Override
    public Boolean isDisabled() {
        if (getItemsTotal() > 0) {
            return false;
        }
        return !Validator.isNotNull(getKeywords());
    }

    @Override
    public Boolean isShowAdvancedSearch() {
        return true;
    }

    @Override
    public List<DropdownItem> getFilterDropdownItems() {
        return super.getFilterDropdownItems();
    }

    @Override
    public int getItemsTotal() {
        return getBlockSearch().getTotal();
    }


    @Override
    public String getSortingOrder() {
        if (Validator.isNotNull(_orderByType)) {
            return _orderByType;
        }
        _orderByType = ParamUtil.getString(request, SearchContainer.DEFAULT_ORDER_BY_TYPE_PARAM, "asc");
        return _orderByType;
    }

    @Override
    public String getSortingURL() {
        PortletURL sortingURL = getPortletURL();
        sortingURL.setParameter(
                SearchContainer.DEFAULT_ORDER_BY_TYPE_PARAM,
                Objects.equals(getOrderByType(), "asc") ? "desc" : "asc");
        sortingURL.setParameter(
                SearchContainer.DEFAULT_ORDER_BY_COL_PARAM, getOrderByCol());
        return sortingURL.toString();
    }

    @Override
    public List<ViewTypeItem> getViewTypeItems() {
        PortletURL portletURL = liferayPortletResponse.createActionURL();

        portletURL.setParameter(
                ActionRequest.ACTION_NAME, "changeDisplayStyle");
        portletURL.setParameter("redirect", PortalUtil.getCurrentURL(request));

        return new ViewTypeItemList(portletURL, getDisplayStyle()) {
            {
                addCardViewTypeItem();
                addListViewTypeItem();
                addTableViewTypeItem();
            }
        };
    }

    @Override
    public Boolean isShowInfoButton() {
        return false;
    }

    public BlockSearch getBlockSearch() {
        if (_blockSearch != null) {
            return _blockSearch;
        }

        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(
                WebKeys.THEME_DISPLAY);

        BlockSearch entrySearch = new BlockSearch(liferayPortletRequest, getPortletURL());
        entrySearch.setOrderByCol(getOrderByCol());

        if (entrySearch.getOrderByComparator() == null) {//nunca entra, los valores por defecto del portlet o el valor por defecto de searcher no permiten la ausencia de valor
            entrySearch.setOrderByComparator(OrderByComparatorFactoryUtil.create("Block", "modifiedDate", false));
        }

        entrySearch.setOrderByType(getOrderByType());

        EmptyOnClickRowChecker emptyOnClickRowChecker =
                new EmptyOnClickRowChecker(liferayPortletResponse);

        //emptyOnClickRowChecker.setRememberCheckBoxStateURLRegex(
        //      "^(?!.*" + liferayPortletResponse.getNamespace() +
        //            "redirect).*^(?!.*/block/)");

        entrySearch.setRowChecker(emptyOnClickRowChecker);

        BlockSearchTerms searchTerms =
                (BlockSearchTerms) entrySearch.getSearchTerms();

        List<Block> blocks = new ArrayList<>();

        if (Validator.isNotNull(searchTerms.getKeywords())) {
            blocks = BlockLocalServiceUtil.findByFileName(themeDisplay.getCompanyId(),
                    themeDisplay.getScopeGroupId(), searchTerms.getKeywords(),
                    entrySearch.getStart(), entrySearch.getEnd(), entrySearch.getOrderByComparator());
            if (blocks.size() == 0) {
                entrySearch.setEmptyResultsMessage(
                        LanguageUtil.format(
                                request,
                                "no-entries-were-found-that-matched-the-keywords-x",
                                "<strong>" + HtmlUtil.escape(searchTerms.getKeywords()) +
                                        "</strong>",
                                false));
            }
            entrySearch.setTotal(BlockLocalServiceUtil.findByFileNameCount(themeDisplay.getCompanyId(),
                    themeDisplay.getScopeGroupId(), searchTerms.getKeywords()));
        } else {
            if (Objects.equals(getNavigation(), "all")) {
                blocks = BlockLocalServiceUtil.getBlocks(entrySearch.getStart(), entrySearch.getEnd(), entrySearch.getOrderByComparator());
                entrySearch.setTotal(BlockLocalServiceUtil.getBlocksCount());
            } else if (Objects.equals(getNavigation(), "noDescription")) {
                blocks = BlockLocalServiceUtil.findWithoutDescription(themeDisplay.getCompanyId(), themeDisplay.getScopeGroupId(), entrySearch.getStart(), entrySearch.getEnd(), null);
                entrySearch.setTotal(BlockLocalServiceUtil.getWithoutDescriptionCount(themeDisplay.getCompanyId(), themeDisplay.getScopeGroupId()));
            } else if (Objects.equals(getNavigation(), "lessModifiedDate")) {
                Date before = new Date(new Date().getTime() - 3600000);

                blocks = BlockLocalServiceUtil.findModiedDateLessThan(themeDisplay.getCompanyId(), themeDisplay.getScopeGroupId(), before, entrySearch.getStart(), entrySearch.getEnd(), null);
                entrySearch.setTotal(BlockLocalServiceUtil.getModiedDateLessThanCount(
                        themeDisplay.getCompanyId(), themeDisplay.getScopeGroupId(), before));
            }

        }

        entrySearch.setResults(blocks);
        entrySearch.setDeltaConfigurable(true);

        _blockSearch = entrySearch;

        return _blockSearch;
    }

    public String getOrderByType() {
        if (Validator.isNotNull(_orderByType)) {
            return _orderByType;
        }

        _orderByType = ParamUtil.getString(request, SearchContainer.DEFAULT_ORDER_BY_TYPE_PARAM, "asc");

        return _orderByType;
    }

    private String getKeywords() {
        if (_keywords != null) {
            return _keywords;
        }
        _keywords = ParamUtil.getString(request, DisplayTerms.KEYWORDS);
        return _keywords;
    }

    @Override
    protected List<DropdownItem> getFilterNavigationDropdownItems() {
        return new DropdownItemList() {
            {
                addGroup(
                        dropdownGroupItem -> {
                            dropdownGroupItem.setDropdownItems(
                                    _getFilterNavigationDropdownItems());
                        });

                addGroup(
                        dropdownGroupItem -> {
                            dropdownGroupItem.setDropdownItems(
                                    _getOrderByDropdownItems());
                            dropdownGroupItem.setLabel(
                                    LanguageUtil.get(request, "order-by"));
                        });
            }
        };
    }

    private List<DropdownItem> _getFilterNavigationDropdownItems() {
        return new DropdownItemList() {
            {
                add(
                        dropdownItem -> {
                            dropdownItem.setActive(
                                    Objects.equals(getNavigation(), "all"));
                            dropdownItem.setHref(
                                    getPortletURL(), "navigation", "all");
                            dropdownItem.setLabel(
                                    LanguageUtil.get(request, "all"));
                        });
                add(
                        dropdownItem -> {
                            dropdownItem.setActive(
                                    Objects.equals(getNavigation(), "noDescription"));
                            dropdownItem.setHref(
                                    getPortletURL(), "navigation", "noDescription");
                            dropdownItem.setLabel(
                                    LanguageUtil.get(request, "block.no.description"));
                        });
                add(
                        dropdownItem -> {
                            dropdownItem.setActive(
                                    Objects.equals(getNavigation(), "lessModifiedDate"));
                            dropdownItem.setHref(
                                    getPortletURL(), "navigation", "lessModifiedDate");
                            dropdownItem.setLabel(
                                    LanguageUtil.get(request, "block.lessModifiedDate"));
                        });
            }
        };
    }

    public String getNavigation() {
        if (_navigation != null) {
            return _navigation;
        }
        _navigation = ParamUtil.getString(request, "navigation", "all");
        return _navigation;
    }

    /**
     * Order block, see more {@link BlockSearch}
     *
     * @return
     */
    private List<DropdownItem> _getOrderByDropdownItems() {
        return new DropdownItemList() {
            {
                add(
                        dropdownItem -> {
                            dropdownItem.setActive(
                                    Objects.equals(getOrderByCol(), "modified-date"));
                            dropdownItem.setHref(
                                    getPortletURL(), SearchContainer.DEFAULT_ORDER_BY_COL_PARAM, "modified-date",
                                    SearchContainer.DEFAULT_ORDER_BY_TYPE_PARAM, getOrderByType());
                            dropdownItem.setLabel(
                                    LanguageUtil.get(request, "block.modified.date"));
                        });
                add(
                        dropdownItem -> {
                            dropdownItem.setActive(
                                    Objects.equals(getOrderByCol(), "file-name"));
                            dropdownItem.setHref(
                                    getPortletURL(), SearchContainer.DEFAULT_ORDER_BY_COL_PARAM, "file-name",
                                    SearchContainer.DEFAULT_ORDER_BY_TYPE_PARAM, getOrderByType());
                            dropdownItem.setLabel(
                                    LanguageUtil.get(request, "block.name"));
                        });
            }
        };
    }

    @Override
    protected String getOrderByCol() {
        if (Validator.isNotNull(_orderByCol)) {
            return _orderByCol;
        }
        _orderByCol = ParamUtil.getString(request, SearchContainer.DEFAULT_ORDER_BY_COL_PARAM, "modified-date");
        return _orderByCol;
    }

    public PortletURL getPortletURL() {
        PortletURL portletURL = liferayPortletResponse.createRenderURL();

        long trashEntryId = getBlockId();

        if (trashEntryId > 0) {
            portletURL.setParameter("mvcPath", "/jsp/registry/view.jsp");
            portletURL.setParameter(
                    "blockId", String.valueOf(trashEntryId));
        }

        String displayStyle = getDisplayStyle();

        if (Validator.isNotNull(displayStyle)) {
            portletURL.setParameter("displayStyle", displayStyle);
        }

        String keywords = ParamUtil.getString(request, DisplayTerms.KEYWORDS);

        if (Validator.isNotNull(keywords)) {
            portletURL.setParameter(DisplayTerms.KEYWORDS, keywords);
        }

        return portletURL;
    }

    public long getBlockId() {
        Block trashEntry = getBlock();
        if (trashEntry != null) {
            return trashEntry.getBlockId();
        }
        return 0;
    }

    public Block getBlock() {
        if (_block != null) {
            return _block;
        }
        long blockId = ParamUtil.getLong(request, "blockId");
        if (blockId > 0) {
            _block = BlockLocalServiceUtil.fetchBlock(blockId);
        }

        return _block;
    }

    public String getDisplayStyle() {
        if (Validator.isNotNull(_displayStyle)) {
            return _displayStyle;
        }

        PortalPreferences portalPreferences =
                PortletPreferencesFactoryUtil.getPortalPreferences(request);

        _displayStyle = portalPreferences.getValue(
                BlockPortletKeys.BLOCK_PANEL, "display-style", "table");

        return _displayStyle;
    }

    private Block _block;
    private String _displayStyle;
    private String _keywords;
    private String _orderByType;
    private String _orderByCol;
    private String _navigation;
    private BlockSearch _blockSearch;

    public String getXMLUrl() {
        ResourceURL resourceURL = liferayPortletResponse.createResourceURL(BlockPortletKeys.BLOCK_PANEL);
        resourceURL.setResourceID("/download/file");
        resourceURL.setParameter("patata", "muerePatata");
        return resourceURL.toString();
    }

    public String getExcelUrl() {
        ResourceURL resourceURL = liferayPortletResponse.createResourceURL(BlockPortletKeys.BLOCK_PANEL);
        resourceURL.setResourceID("/download/file/excel");
        resourceURL.setParameter("patata", "muerePatata");
        return resourceURL.toString();
    }
}
