package com.gmail.rmedina.panel.block.portlet.actions;

import com.gmail.rmedina.panel.block.constants.BlockPortletKeys;
import com.gmail.ronnyecu.database.model.Block;
import com.gmail.ronnyecu.database.service.BlockLocalServiceUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import org.osgi.service.component.annotations.Component;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import java.util.Date;


@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + BlockPortletKeys.BLOCK_PANEL,
                "mvc.command.name=" + BlockPortletKeys.URL_ADD_BLOCK
        },
        service = MVCActionCommand.class
)
public class AddBlockMVCA extends BaseMVCActionCommand {
    @Override
    protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
        ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
        long blockId = new Date().getTime();
        Block block = BlockLocalServiceUtil.createBlock(blockId);

        block.setCompanyId(themeDisplay.getCompanyId());
        block.setGroupId(themeDisplay.getScopeGroupId());

        block.setBlockCode(String.valueOf(blockId));
        block.setFileName("Block");

        BlockLocalServiceUtil.addBlock(block);
    }
}
