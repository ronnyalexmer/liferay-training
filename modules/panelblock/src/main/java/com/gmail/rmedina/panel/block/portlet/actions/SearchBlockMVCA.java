package com.gmail.rmedina.panel.block.portlet.actions;

import com.gmail.rmedina.panel.block.constants.BlockPortletKeys;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import org.osgi.service.component.annotations.Component;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + BlockPortletKeys.BLOCK_PANEL,
                "mvc.command.name=" + BlockPortletKeys.URL_SEARCH_BLOCK
        },
        service = MVCActionCommand.class
)
public class SearchBlockMVCA extends BaseMVCActionCommand {


    @Override
    protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
      /*  String keywords = ParamUtil.getString(actionRequest, "keywords");
        if (!Validator.isBlank(keywords)) {
            System.out.println(keywords);
            actionRequest.setAttribute(Field.KEYWORD_SEARCH, StringPool.PERCENT + keywords + StringPool.PERCENT);
        }*/
    }
}
