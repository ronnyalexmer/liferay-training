package com.gmail.rmedina.panel.block.portlet.search;

import com.gmail.rmedina.panel.block.constants.BlockPortletKeys;
import com.gmail.ronnyecu.database.model.Block;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.PortalPreferences;
import com.liferay.portal.kernel.portlet.PortletPreferencesFactoryUtil;
import com.liferay.portal.kernel.portlet.PortletProvider;
import com.liferay.portal.kernel.portlet.PortletProviderUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;

import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BlockSearch extends SearchContainer<Block> {
    public static final String EMPTY_RESULTS_MESSAGE = "the-recycle-bin-is-empty";
    public static List<String> headerNames = new ArrayList<String>() {
        {
            add("block-name");
            add("modified-date");
        }
    };

    public static Map<String, String> orderableHeaders =
            new HashMap<String, String>() {
                {
                    put("block-name", "block-name");
                    put("modified-date", "modified-date");
                }
            };


    public BlockSearch(PortletRequest portletRequest, PortletURL iteratorURL) {
        super(portletRequest, new BlockDisplayTerms(portletRequest),
                new BlockSearchTerms(portletRequest), DEFAULT_CUR_PARAM,
                BlockPortletKeys.BLOCK_DELTA, iteratorURL, headerNames, EMPTY_RESULTS_MESSAGE);

        try {
            PortalPreferences preferences =
                    PortletPreferencesFactoryUtil.getPortalPreferences(
                            portletRequest);

            String portletId = PortletProviderUtil.getPortletId(
                    User.class.getName(), PortletProvider.Action.VIEW);

            String orderByCol = ParamUtil.getString(
                    portletRequest, DEFAULT_ORDER_BY_COL_PARAM);
            String orderByType = ParamUtil.getString(
                    portletRequest, DEFAULT_ORDER_BY_TYPE_PARAM);

            if (Validator.isNotNull(orderByCol) &&
                    Validator.isNotNull(orderByType)) {

                preferences.setValue(
                        portletId, "entries-order-by-col", orderByCol);
                preferences.setValue(
                        portletId, "entries-order-by-type", orderByType);
            } else {
                orderByCol = preferences.getValue(
                        portletId, "entries-order-by-col", "modified-date");
                orderByType = preferences.getValue(
                        portletId, "entries-order-by-type", "asc");
            }

            OrderByComparator<Block> orderByComparator =
                    _getEntryOrderByComparator(orderByCol, orderByType);

            setOrderableHeaders(orderableHeaders);
            setOrderByCol(orderByCol);
            setOrderByType(orderByType);
            setOrderByComparator(orderByComparator);
        } catch (Exception e) {
            _log.error("Unable to initialize entry search", e);
        }
    }

    /**
     * @param orderByCol
     * @param orderByType
     * @return
     * @default modifiedDate Comparator
     */
    private OrderByComparator<Block> _getEntryOrderByComparator(String orderByCol, String orderByType) {
        boolean orderByAsc = false;

        if (orderByType.equals("asc")) {
            orderByAsc = true;
        }

        OrderByComparator<Block> orderByComparator = null;

        if (orderByCol.equals("file-name")) {
            orderByComparator = OrderByComparatorFactoryUtil.create("Block", "fileName", orderByAsc);
        } else {
            orderByComparator = OrderByComparatorFactoryUtil.create("Block", "modifiedDate", orderByAsc);
        }

        return orderByComparator;
    }

    private static final Log _log = LogFactoryUtil.getLog(BlockSearch.class);

}