package com.gmail.rmedina.panel.block.portlet.search;

import com.liferay.portal.kernel.dao.search.DisplayTerms;
import com.liferay.portal.kernel.util.ParamUtil;

import javax.portlet.PortletRequest;

public class BlockDisplayTerms extends DisplayTerms {

    public static final String BLOCK_NAME = "blockName";
    public static final String MODIFIED_DATE = "modifiedDate";
    protected String blockName;
    protected String modifiedDate;


    public BlockDisplayTerms(PortletRequest portletRequest) {
        super(portletRequest);
        blockName = ParamUtil.getString(portletRequest, BLOCK_NAME);
        modifiedDate = ParamUtil.getString(portletRequest, MODIFIED_DATE);
    }

    public String getBlockName() {
        return blockName;
    }

    public void setBlockName(String blockName) {
        this.blockName = blockName;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }
}
