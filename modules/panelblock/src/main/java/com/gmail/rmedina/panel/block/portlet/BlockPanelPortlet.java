package com.gmail.rmedina.panel.block.portlet;

import com.gmail.rmedina.panel.block.constants.BlockPortletKeys;

import com.gmail.ronnyecu.database.model.Block;
import com.gmail.ronnyecu.database.service.BlockLocalServiceUtil;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.dao.search.EmptyOnClickRowChecker;
import com.liferay.portal.kernel.dao.search.RowChecker;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.portlet.PortalPreferences;
import com.liferay.portal.kernel.portlet.PortletPreferencesFactoryUtil;
import com.liferay.portal.kernel.portlet.PortletURLUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import javax.portlet.*;

import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import org.osgi.service.component.annotations.Component;

import java.io.IOException;

/**
 * @author Administrador
 */
@Component(
        immediate = true,
        property = {
                "com.liferay.portlet.add-default-resource=true",
                "com.liferay.portlet.display-category=category.hidden",
                "com.liferay.portlet.header-portlet-css=/css/main.css",
                "com.liferay.portlet.layout-cacheable=true",
                "com.liferay.portlet.private-request-attributes=false",
                "com.liferay.portlet.private-session-attributes=false",
                "com.liferay.portlet.render-weight=50",
                "com.liferay.portlet.use-default-template=true",
                "javax.portlet.display-name=Panel Block",
                "javax.portlet.expiration-cache=0",
                "javax.portlet.init-param.template-path=/META-INF/resources/",
                "javax.portlet.init-param.view-template=/view.jsp",
                "javax.portlet.name=" + BlockPortletKeys.BLOCK_PANEL,
                "javax.portlet.resource-bundle=content.Language",
                "javax.portlet.security-role-ref=power-user,user",

        },
        service = Portlet.class
)
public class BlockPanelPortlet extends MVCPortlet {

    /* @Override
     public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
         String keywords = ParamUtil.getString(renderRequest, Field.KEYWORD_SEARCH);
         if (!Validator.isBlank(keywords)) {
             renderRequest.setAttribute(Field.KEYWORD_SEARCH, keywords);
         } else {
           /*  SearchContainer<Block> searchContainer = (SearchContainer<Block>) renderRequest.getAttribute("searchContainter");
             if (Validator.isNull(searchContainer)) {
                 PortletURL currentURL = PortletURLUtil.getCurrent(
                         renderRequest, renderResponse);

                 searchContainer = new SearchContainer<>(
                         renderRequest, currentURL, null, null);

                 searchContainer.setEmptyResultsMessage("no-entities-remain-of-this-type");
                 searchContainer.setId("BlockEntities");
                 searchContainer.setTotal(BlockLocalServiceUtil.getBlocksCount());
                 RowChecker rowChecker = new EmptyOnClickRowChecker(renderResponse);
                 searchContainer.setRowChecker(rowChecker);

             }
             searchContainer.setResults(BlockLocalServiceUtil.getBlocks(searchContainer.getStart(), searchContainer.getEnd()));
             renderRequest.setAttribute("searchContainerBlock", searchContainer);*/
     /*   }
        super.render(renderRequest, renderResponse);
    }*/
    public void changeDisplayStyle(
            ActionRequest actionRequest, ActionResponse actionResponse) {

        hideDefaultSuccessMessage(actionRequest);

        String displayStyle = ParamUtil.getString(
                actionRequest, "displayStyle");

        PortalPreferences portalPreferences =
                PortletPreferencesFactoryUtil.getPortalPreferences(actionRequest);

        portalPreferences.setValue(
                BlockPortletKeys.BLOCK_PANEL, "display-style", displayStyle);
    }
}