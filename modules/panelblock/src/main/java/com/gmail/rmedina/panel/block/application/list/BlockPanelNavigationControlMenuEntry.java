package com.gmail.rmedina.panel.block.application.list;

import com.gmail.rmedina.panel.block.constants.BlockPortletKeys;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.product.navigation.control.menu.BaseProductNavigationControlMenuEntry;
import com.liferay.product.navigation.control.menu.ProductNavigationControlMenuEntry;
import com.liferay.product.navigation.control.menu.constants.ProductNavigationControlMenuCategoryKeys;
import org.osgi.service.component.annotations.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

@Component(
        immediate = true,
        property = {
                "product.navigation.control.menu.category.key=" + ProductNavigationControlMenuCategoryKeys.USER,
                "product.navigation.control.menu.category.order:Integer=500"
        },
        service = ProductNavigationControlMenuEntry.class
)
public class BlockPanelNavigationControlMenuEntry extends BaseProductNavigationControlMenuEntry {
    @Override
    public String getLabel(Locale locale) {
        return "Option";
    }

    @Override
    public String getURL(HttpServletRequest request) {
        return "https://google.es";
    }

    @Override
    public String getIcon(HttpServletRequest request) {
        return "ellipsis-v";
    }

    @Override
    public boolean isShow(HttpServletRequest request) throws PortalException {
        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
        if (Validator.isNotNull(themeDisplay)) {
            return themeDisplay.getPortletDisplay().getPortletName().equalsIgnoreCase(BlockPortletKeys.BLOCK_PANEL);
        }
        return false;
    }
}
