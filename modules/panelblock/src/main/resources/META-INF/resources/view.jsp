<%@ page import="com.gmail.rmedina.panel.block.constants.BlockPortletKeys" %>
<%@ include file="/init.jsp" %>
<portlet:actionURL name="/block/add" var="addBlock"/>
<portlet:actionURL name="/block/add" var="deleteEntriesURL"/>

<clay:management-toolbar componentId="<%=BlockPortletKeys.BLOCK_MANAGEMENT_TOOLBAR_ID%>" displayContext="<%= blockDisplayContext %>"/>
<div class="closed container-fluid container-fluid-max-xl"
     id="<portlet:namespace />infoPanelId">
    <div class="sidenav-content">
        <aui:form action="<%= deleteEntriesURL %>" name="fm">

            <liferay-ui:search-container id="<%=BlockPortletKeys.BLOCK_SEARCH_CONTAINER_ID%>"
                                         searchContainer="<%=blockDisplayContext.getBlockSearch()%>">

                <liferay-ui:search-container-row rowVar="row"
                                                 className="com.gmail.ronnyecu.database.model.Block"
                                                 keyProperty="blockCode" modelVar="curBlock">
                    <liferay-ui:search-container-column-text property="fileName"
                                                             name="block.name"
                                                             cssClass="table-cell-expand table-cell-minw-50 table-title"/>
                    <liferay-ui:search-container-column-text property="description"
                                                             name="block.description"
                                                             cssClass="table-cell-expand table-cell-minw-50 table-title"/>
                    <liferay-ui:search-container-column-date property="modifiedDate" name="modified-date"
                                                             cssClass="table-cell-expand"/>
                    <liferay-ui:search-container-column-jsp path="/jsp/block/actions.jsp"/>
                </liferay-ui:search-container-row>

                <liferay-ui:search-iterator displayStyle="<%=blockDisplayContext.getDisplayStyle()%>"
                                            markupView="lexicon"/>
            </liferay-ui:search-container>
        </aui:form>
    </div>
</div>

<aui:script>
    var deleteSelectedEntries = function() {
        if (confirm('<liferay-ui:message key="are-you-sure-you-want-to-delete-this"/>')) {
            var form = document.getElementById('<portlet:namespace/>fm');
            if (form) {
                submitForm(form);
            }
        }
    }

    var ACTIONS = {
    'deleteSelectedEntries': deleteSelectedEntries
    };

    Liferay.componentReady('<%=BlockPortletKeys.BLOCK_MANAGEMENT_TOOLBAR_ID%>').then(function(managementToolbar) {
        managementToolbar.on('actionItemClicked', function(event) {
            var itemData = event.data.item.data;
            console.log(itemData);
                if (itemData && itemData.action && ACTIONS[itemData.action]) {
                    ACTIONS[itemData.action]();
                }
            });
    });
    $("form .dropdown-toggle.btn.btn-unstyled").click(function() {
        setTimeout(function() {
            $("#clay_dropdown_portal .dropdown-menu:nth-child(2) ul").html(
            '<li tabindex="-1"><a class=" dropdown-item" href="Javascript:;">Buscar por fecha</a></li>'+
            '<li tabindex="-1"><a class=" dropdown-item" href="Javascript:;">Buscar por precio</a></li>'
            );
    }, 50);
    });

</aui:script>