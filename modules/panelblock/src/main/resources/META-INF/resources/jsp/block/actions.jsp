<%@ page import="com.liferay.petra.string.StringPool" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/init.jsp" %>
<liferay-ui:icon-menu
        direction="left-side"
        icon="<%= StringPool.BLANK %>"
        markupView="lexicon"
        message="<%= StringPool.BLANK %>"
        showWhenSingleIcon="<%= true %>"
>
    <liferay-ui:icon
            message="Download"
            target="_blank"
            url="<%= blockDisplayContext.getXMLUrl() %>"
    />

    <liferay-ui:icon
            message="Excel"
            target="_blank"
            url="<%= blockDisplayContext.getExcelUrl() %>"
    />
</liferay-ui:icon-menu>