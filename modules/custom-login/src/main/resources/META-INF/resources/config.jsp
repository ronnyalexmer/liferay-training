<%@ include file="/init.jsp" %>

<liferay-portlet:actionURL portletConfiguration="<%=true%>" var="configUrl"/>

<div class="portlet-configuration-setup">
    <aui:form action="<%=configUrl%>">
        <div class="lfr-form-content">
            <div class="sheet sheet-lg">
                <aui:input name="webContentId" type="text" required="true" value="${requestScope.webContentId}"/>
            </div>
        </div>
        <div class="sheet-footer dialog-footer">
            <aui:button type="cancel"/>
            <aui:button type="submit" value="save"/>
        </div>
    </aui:form>
</div>