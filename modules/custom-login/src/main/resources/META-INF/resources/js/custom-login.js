var CUSTOM_LOGIN = function () {
    var ini = function () {
        events();
    };

    var events = function () {
        $(".forgot-password").click(function () {
            $("form.form-login").hide();
            $("form.form-recover").show();
        });

        $("button.backButton").click(function () {
            $("form.form-recover").hide();
            $("form.form-login").show();
        });

        $(".form-signin").submit(function () {
            $.each($(this).find("input[type = 'checkbox']"), function () {
                this.value = this.checked;
            });
        });
    };

    return {
        ini: ini
    }

}();

$(document).ready(function () {
    CUSTOM_LOGIN.ini();
});