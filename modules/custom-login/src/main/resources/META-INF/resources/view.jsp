<%@ page import="com.gmail.ronnyecu.custom.login.constants.CustomLoginPortletKeys" %>
<%@ page import="com.liferay.portal.kernel.captcha.CaptchaConfigurationException" %>
<%@ page import="com.liferay.portal.kernel.captcha.CaptchaException" %>
<%@ page import="com.liferay.portal.kernel.captcha.CaptchaTextException" %>
<%@ include file="/init.jsp" %>

<script type="text/javascript" src="<%=request.getContextPath()%>/js/custom-login.js"></script>

<portlet:actionURL name="<%=CustomLoginPortletKeys.URL_CUSTOM_LOGIN%>" var="login"/>

<portlet:actionURL name="<%=CustomLoginPortletKeys.URL_PASSWORD_RECOVERY%>" var="passwordRecovery"/>

<div class="custom-login">
    <div class="container">
        <div class="card card-container">

            <aui:form cssClass="form-signin form-login ${requestScope.config.view == 1 ? '' : 'form-hidden'}"
                      method="post" action="<%=login%>">
                <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png"
                     alt=""/>
                <aui:input name="email" id="inputEmail" type="email" required="true" autoFocus="true"
                           placeholder="Email"/>
                <aui:input name="password" id="inputPassword" type="password" required="true"
                           placeholder="Password"/>
                <c:if test="${not empty requestScope.msg}">
                    <liferay-ui:message key="${requestScope.msg}"/>
                </c:if>
                <div id="remember" class="checkbox">
                    <aui:input name="rememberMe" type="checkbox" value="false" label="Remember me"/>
                </div>
                <aui:button cssClass="btn-lg btn-primary btn-block btn-signin" type="submit" value="Sign in"/>
                <a href="Javascript:" class="forgot-password">
                    <liferay-ui:message key="login.forget.password"/>
                </a>
            </aui:form>
            <aui:form cssClass="form-signin form-recover ${requestScope.config.view == 2 ? '' : 'form-hidden'}"
                      method="post" action="<%=passwordRecovery%>">
                <aui:input name="email" id="inputEmail" type="email" required="true" autoFocus="true"
                           placeholder="Email"/>

                <c:if test="${not empty requestScope.config.sendPasswordCaptchaEnabled and requestScope.config.sendPasswordCaptchaEnabled}">
                    <liferay-ui:error exception="<%= CaptchaConfigurationException.class %>"
                                      message="a-captcha-error-occurred-please-contact-an-administrator"/>
                    <liferay-ui:error exception="<%= CaptchaException.class %>" message="captcha-verification-failed"/>
                    <liferay-ui:error exception="<%= CaptchaTextException.class %>" message="text-verification-failed"/>
                    <liferay-portlet:resourceURL copyCurrentRenderParameters="<%= false %>"
                                                 id="<%=CustomLoginPortletKeys.URL_PASSWORD_RECOVERY_CAPTCHA%>"
                                                 var="captchaURL"/>
                    <liferay-captcha:captcha url="<%= captchaURL %>"/>
                </c:if>


                <aui:button cssClass="btn-lg btn-primary btn-block btn-signin" type="submit"
                            value="Recover Password"/>
                <aui:button type="button" value="back" cssClass="backButton"/>
            </aui:form>
        </div>
    </div>
</div>