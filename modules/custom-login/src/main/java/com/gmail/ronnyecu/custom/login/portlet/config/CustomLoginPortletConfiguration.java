package com.gmail.ronnyecu.custom.login.portlet.config;

import aQute.bnd.annotation.metatype.Meta;

@Meta.OCD(
        id = "com.gmail.ronnyecu.custom.login.portlet.config.CustomLoginPortletConfiguration"
)
public interface CustomLoginPortletConfiguration {

    @Meta.AD
    public long webContentId();
}
