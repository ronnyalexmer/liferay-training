package com.gmail.ronnyecu.custom.login.portlet.actions;

import com.gmail.ronnyecu.custom.login.constants.CustomLoginPortletKeys;
import com.liferay.captcha.util.CaptchaUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import org.osgi.service.component.annotations.Component;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import java.io.IOException;

@Component(
        property = {
                "javax.portlet.name=" + CustomLoginPortletKeys.CUSTOMLOGIN,
                "mvc.command.name=" + CustomLoginPortletKeys.URL_PASSWORD_RECOVERY_CAPTCHA
        },
        service = MVCResourceCommand.class
)
public class CaptchaMVCResourceCommand implements MVCResourceCommand {

    @Override
    public boolean serveResource(
            ResourceRequest resourceRequest, ResourceResponse resourceResponse)
            throws PortletException {

        try {
            CaptchaUtil.serveImage(resourceRequest, resourceResponse);

            return false;
        } catch (IOException ioe) {
            throw new PortletException(ioe);
        }
    }
}
