package com.gmail.ronnyecu.custom.login.portlet.config.action;

import com.gmail.ronnyecu.custom.login.constants.CustomLoginPortletKeys;
import com.liferay.portal.kernel.model.PortletPreferencesWrapper;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;

import javax.portlet.*;
import javax.servlet.http.HttpServletRequest;

@Component(
        configurationPid = "com.gmail.ronnyecu.custom.login.portlet.config.CustomLoginPortletConfiguration",
        configurationPolicy = ConfigurationPolicy.OPTIONAL, immediate = true,
        property = {
                "javax.portlet.name=" + CustomLoginPortletKeys.CUSTOMLOGIN
        },
        service = ConfigurationAction.class
)
public class CustomLoginPortletConfigurationAction extends DefaultConfigurationAction {

    @Override
    public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
        String webContentId = ParamUtil.getString(actionRequest, CustomLoginPortletKeys.CONFIG_PARAM_WEB_CONTENT_ID);
        actionRequest.getPreferences().setValue(CustomLoginPortletKeys.CONFIG_PARAM_WEB_CONTENT_ID, webContentId);
        actionRequest.getPreferences().store();
        super.processAction(portletConfig, actionRequest, actionResponse);
    }

    @Override
    public String getJspPath(HttpServletRequest request) {
        PortletPreferences pref = getPreferences(request);
        String webContentId = pref.getValue(CustomLoginPortletKeys.CONFIG_PARAM_WEB_CONTENT_ID, "");
        request.setAttribute(CustomLoginPortletKeys.CONFIG_PARAM_WEB_CONTENT_ID, webContentId);

        return super.getJspPath(request);
    }

    public PortletPreferences getPreferences(HttpServletRequest request) {
        PortletRequest pr = (PortletRequest) request.getAttribute("javax.portlet.request");
        return pr.getPreferences();
    }
}
