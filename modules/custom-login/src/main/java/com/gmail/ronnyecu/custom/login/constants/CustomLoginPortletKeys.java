package com.gmail.ronnyecu.custom.login.constants;

/**
 * @author Administrador
 */
public class CustomLoginPortletKeys {

    public static final String CUSTOMLOGIN =
            "com_gmail_ronnyecu_custom_login_CustomLoginPortlet";

    public static final String URL_CUSTOM_LOGIN = "/brandCenter/login";
    public static final String URL_PASSWORD_RECOVERY = "/brandCenter/password";
    public static final String URL_PASSWORD_RECOVERY_CAPTCHA = "/brandCenter/password/captcha";

    //config
    public static final String CONFIG_PARAM_WEB_CONTENT_ID = "webContentId";
    public static final String MSG_ERROR_LOGIN = "msg";

    //structure names
    public static String STRUCTURE_URL_REDIRECT = "urlRedirect";

    //Portlet form params
    public static String PORTLET_FOM_EMAIL = "email";
    public static String PORTLET_FOM_PASSWORD = "password";
    public static String PORTLET_FOM_REMEMBER = "rememberMe";

    //Beans keys
    public static String BEAN_KEY_CONFIG_FORM = "config";
    public static String BEAN_KEY_CONFIG_VIEW = "view";
    public static int BEAN_CONFIG_FORM_VIEW_LOGIN = 1;
    public static int BEAN_CONFIG_FORM_VIEW_RECOvER_PASSWORD = 2;

}