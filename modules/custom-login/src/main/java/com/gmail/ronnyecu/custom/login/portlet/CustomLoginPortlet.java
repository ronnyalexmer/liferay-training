package com.gmail.ronnyecu.custom.login.portlet;

import com.gmail.ronnyecu.custom.login.constants.CustomLoginPortletKeys;
import com.gmail.ronnyecu.custom.login.portlet.models.FormBean;
import com.liferay.captcha.configuration.CaptchaConfiguration;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.*;
import java.io.IOException;

/**
 * @author Administrador
 */
@Component(
        immediate = true,
        property = {
                "com.liferay.portlet.display-category=category.sample",
                "com.liferay.portlet.header-portlet-css=/css/main.css",
                "com.liferay.portlet.instanceable=true",
                "javax.portlet.display-name=CustomLogin",
                "javax.portlet.init-param.template-path=/",
                "javax.portlet.init-param.config-template=/config.jsp",
                "javax.portlet.init-param.view-template=/view.jsp",
                "javax.portlet.name=" + CustomLoginPortletKeys.CUSTOMLOGIN,
                "javax.portlet.resource-bundle=content.Language",
                "javax.portlet.security-role-ref=power-user,user",
                "javax.portlet.init-param.add-process-action-success-action=false"
        },
        service = Portlet.class
)
public class CustomLoginPortlet extends MVCPortlet {
    private static final Log _log = LogFactoryUtil.getLog(CustomLoginPortlet.class);

    @Override
    public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
        setConfig(renderRequest);
        super.doView(renderRequest, renderResponse);
    }

    private void setConfig(RenderRequest renderRequest) {
        int view = GetterUtil.getInteger(renderRequest.getAttribute(CustomLoginPortletKeys.BEAN_KEY_CONFIG_VIEW), CustomLoginPortletKeys.BEAN_CONFIG_FORM_VIEW_LOGIN);
        FormBean formBean = new FormBean(view);
        formBean.setSendPasswordCaptchaEnabled(isCaptchaEnable());
        renderRequest.setAttribute(CustomLoginPortletKeys.BEAN_KEY_CONFIG_FORM, formBean);
    }

    private boolean isCaptchaEnable() {
        boolean isCaptchaEnable;
        try {
            CaptchaConfiguration captchaConfiguration = _configurationProvider.getSystemConfiguration(
                    CaptchaConfiguration.class);
            isCaptchaEnable = captchaConfiguration.sendPasswordCaptchaEnabled();
        } catch (Exception e) {
            isCaptchaEnable = false;
        }
        return isCaptchaEnable;
    }

    @Reference
    private ConfigurationProvider _configurationProvider;
}