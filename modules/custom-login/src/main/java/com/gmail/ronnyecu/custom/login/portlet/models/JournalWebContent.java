package com.gmail.ronnyecu.custom.login.portlet.models;

import com.liferay.journal.model.JournalArticle;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.DocumentException;
import com.liferay.portal.kernel.xml.Node;
import com.liferay.portal.kernel.xml.SAXReaderUtil;

import java.util.Locale;

public class JournalWebContent {
    private JournalArticle journalArticle;
    private Locale locale;

    public JournalWebContent(JournalArticle journalArticle, Locale locale) {
        this.journalArticle = journalArticle;
        this.locale = locale;
    }

    public String getValue(String field) {
        try {
            Document document = SAXReaderUtil.read(journalArticle.getContentByLocale(locale.toString()));
            Node fieldNode = document.selectSingleNode("/root/dynamic-element[@name='" + field + "']/dynamic-content");
            return fieldNode.getStringValue();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        return StringPool.BLANK;
    }


}
