package com.gmail.ronnyecu.custom.login.portlet.actions;

import com.gmail.ronnyecu.custom.login.constants.CustomLoginPortletKeys;
import com.gmail.ronnyecu.custom.login.portlet.models.JournalWebContent;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.CompanyConstants;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.security.auth.session.AuthenticatedSessionManagerUtil;
import com.liferay.portal.kernel.service.LayoutLocalServiceUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.*;
import org.osgi.service.component.annotations.Component;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

@Component(
        property = {
                "javax.portlet.name=" + CustomLoginPortletKeys.CUSTOMLOGIN,
                "mvc.command.name=" + CustomLoginPortletKeys.URL_CUSTOM_LOGIN
        },
        service = MVCActionCommand.class
)
public class CustomLoginMVCActionCommand extends BaseMVCActionCommand {
    private static final Log _log = LogFactoryUtil.getLog(CustomLoginMVCActionCommand.class);

    @Override
    protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) {
        actionRequest.setAttribute(CustomLoginPortletKeys.BEAN_KEY_CONFIG_VIEW, CustomLoginPortletKeys.BEAN_CONFIG_FORM_VIEW_LOGIN);
        ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(
                WebKeys.THEME_DISPLAY);

        HttpServletRequest request = PortalUtil.getOriginalServletRequest(
                PortalUtil.getHttpServletRequest(actionRequest));

        HttpServletResponse response = PortalUtil.getHttpServletResponse(
                actionResponse);
        String redirectUrl = themeDisplay.getLayout().getFriendlyURL(themeDisplay.getLocale());
        String email = ParamUtil.getString(actionRequest, CustomLoginPortletKeys.PORTLET_FOM_EMAIL);
        String password = actionRequest.getParameter(CustomLoginPortletKeys.PORTLET_FOM_PASSWORD);
        boolean rememberMe = ParamUtil.getBoolean(actionRequest, CustomLoginPortletKeys.PORTLET_FOM_REMEMBER);
        String authType = CompanyConstants.AUTH_TYPE_EA;
        boolean loginOk = true;
        try {
            AuthenticatedSessionManagerUtil.login(
                    request, response, email, password, rememberMe, authType);
        } catch (Exception ex) {
            loginOk = false;
        }
        if (loginOk) {
            PortletRequest pr = (PortletRequest) request.getAttribute("javax.portlet.request");
            String webContentId = pr.getPreferences().getValue(CustomLoginPortletKeys.CONFIG_PARAM_WEB_CONTENT_ID, StringPool.BLANK);
            if (!Validator.isBlank(webContentId)) {
                try {
                    JournalWebContent config = new JournalWebContent(JournalArticleLocalServiceUtil.getLatestArticle(themeDisplay.getScopeGroupId(), webContentId), themeDisplay.getLocale());
                    String layoutContent = config.getValue(CustomLoginPortletKeys.STRUCTURE_URL_REDIRECT);
                    if (!Validator.isBlank(layoutContent)) {
                        String[] layoutIds = layoutContent.split(StringPool.AT);
                        if (layoutIds.length == 3) {
                            long linkPageId = GetterUtil.getLong(layoutIds[0]);
                            boolean isPublic = Objects.equals(layoutIds[1], "public");
                            long linkGroupId = GetterUtil.getLong(layoutIds[2]);
                            Layout layout = LayoutLocalServiceUtil.getLayout(linkGroupId, !isPublic, linkPageId);
                            if (Validator.isNotNull(layout) && !Validator.isBlank(layout.getFriendlyURL(themeDisplay.getLocale()))) {
                                redirectUrl = layout.getFriendlyURL(themeDisplay.getLocale());
                            }
                        }
                    }
                    actionResponse.sendRedirect(redirectUrl);
                } catch (PortalException portalEx) {
                    _log.error("Settings doesn't set up");
                } catch (IOException e) {
                    _log.error("Unable to redirect");
                }
            }
        } else {
            SessionErrors.add(request, CustomLoginPortletKeys.MSG_ERROR_LOGIN);
            request.setAttribute(CustomLoginPortletKeys.MSG_ERROR_LOGIN, "msg.error.login");
        }
    }
}
