package com.gmail.ronnyecu.custom.login.portlet.actions;

import com.gmail.ronnyecu.custom.login.constants.CustomLoginPortletKeys;
import com.liferay.captcha.configuration.CaptchaConfiguration;
import com.liferay.captcha.util.CaptchaUtil;
import com.liferay.portal.kernel.captcha.CaptchaConfigurationException;
import com.liferay.portal.kernel.captcha.CaptchaException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.security.auth.PrincipalException;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import java.io.IOException;

@Component(
        property = {
                "javax.portlet.name=" + CustomLoginPortletKeys.CUSTOMLOGIN,
                "mvc.command.name=" + CustomLoginPortletKeys.URL_PASSWORD_RECOVERY
        },
        service = MVCActionCommand.class
)
public class PasswordRecoveryMVCActionCommand extends BaseMVCActionCommand {
    private static final Log _log = LogFactoryUtil.getLog(CustomLoginMVCActionCommand.class);

    @Override
    protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws PrincipalException.MustBeEnabled, CaptchaException {
        actionRequest.setAttribute(CustomLoginPortletKeys.BEAN_KEY_CONFIG_VIEW, CustomLoginPortletKeys.BEAN_CONFIG_FORM_VIEW_RECOvER_PASSWORD);
        String email = ParamUtil.getString(actionRequest, CustomLoginPortletKeys.PORTLET_FOM_EMAIL);
        if (!Validator.isBlank(email) && Validator.isEmailAddress(email)) {
            ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(
                    WebKeys.THEME_DISPLAY);

            ableToSendResetPassword(themeDisplay);
            checkCaptcha(actionRequest);

            try {
                UserLocalServiceUtil.sendPasswordByEmailAddress(themeDisplay.getCompanyId(), email);
            } catch (PortalException e) {
                _log.error("Unable to send email", e);
            }

            SessionMessages.add(actionRequest, "forgotPasswordSent");

            try {
                actionRequest.setAttribute(CustomLoginPortletKeys.BEAN_KEY_CONFIG_VIEW, CustomLoginPortletKeys.BEAN_CONFIG_FORM_VIEW_LOGIN);
                sendRedirect(actionRequest, actionResponse, null);
            } catch (IOException e) {
                _log.error("Unable to redirect", e);
            }
        }
    }

    private void ableToSendResetPassword(ThemeDisplay themeDisplay) throws PrincipalException.MustBeEnabled {
        Company company = themeDisplay.getCompany();

        if (!company.isSendPassword() && !company.isSendPasswordResetLink()) {
            throw new PrincipalException.MustBeEnabled(
                    company.getCompanyId(),
                    PropsKeys.COMPANY_SECURITY_SEND_PASSWORD,
                    PropsKeys.COMPANY_SECURITY_SEND_PASSWORD_RESET_LINK);
        }
    }

    private void checkCaptcha(ActionRequest actionRequest)
            throws CaptchaException {
        try {
            CaptchaConfiguration captchaConfiguration = _configurationProvider.getSystemConfiguration(
                    CaptchaConfiguration.class);
            if (captchaConfiguration.sendPasswordCaptchaEnabled()) {
                CaptchaUtil.check(actionRequest);
            }
        } catch (Exception e) {
            throw new CaptchaConfigurationException(e);
        }
    }

    @Reference
    private ConfigurationProvider _configurationProvider;
}
