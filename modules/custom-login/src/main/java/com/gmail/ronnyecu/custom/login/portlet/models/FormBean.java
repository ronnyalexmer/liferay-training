package com.gmail.ronnyecu.custom.login.portlet.models;

public class FormBean {
    private boolean sendPasswordCaptchaEnabled;
    private int view;

    public FormBean(int view) {
        this.view = view;
    }

    public int getView() {
        return view;
    }

    public void setView(int view) {
        this.view = view;
    }

    public boolean isSendPasswordCaptchaEnabled() {
        return sendPasswordCaptchaEnabled;
    }

    public void setSendPasswordCaptchaEnabled(boolean sendPasswordCaptchaEnabled) {
        this.sendPasswordCaptchaEnabled = sendPasswordCaptchaEnabled;
    }
}
