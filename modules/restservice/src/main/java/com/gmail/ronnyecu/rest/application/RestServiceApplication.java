package com.gmail.ronnyecu.rest.application;

import java.util.Collections;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;

/**
 * @author Administrador
 */
@Component(
        property = {
                JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE + "=/rest-service",
                JaxrsWhiteboardConstants.JAX_RS_NAME + "=Mine.Rest"
        },
        service = Application.class
)
public class RestServiceApplication extends Application {

    public Set<Object> getSingletons() {
        return Collections.<Object>singleton(this);
    }

    @GET
    public void working(@Context UriInfo uriInfo) {
        MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();
        queryParams.forEach((s, strings) -> System.out.println(s + ": " + strings));
        System.out.println(queryParams.get("patata"));
        System.out.println(queryParams.getFirst("patata"));
        System.out.println(uriInfo.getRequestUri().getQuery());
    }

    @GET
    @Path("/morning")
    @Produces("text/plain")
    public String hello() {
        return "Good morning!";
    }

    @GET
    @Path("/morning/{name}")
    @Produces("text/plain")
    public String morning(
            @PathParam("name") String name,
            @QueryParam("drink") String drink) {

        String greeting = "Good Morning " + name;

        if (drink != null) {
            greeting += ". Would you like some " + drink + "?";
        }

        return greeting;
    }

}